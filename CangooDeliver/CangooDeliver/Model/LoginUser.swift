//
//  LoginUser.swift
//  CanTaxi-User
//
//  Created by Abubakar on 14/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginUser: NSObject, NSCoding {
    var pID:String
    var resellerID:String
    var originalPicture:String
    var firstName:String
    var lastName:String
    var access_Token:String
    var rating:String
    var numberDriverFavourites:String
    var spendings:String
    var noOfTrips:String
    var phoneNumber:String
    var selectedPaymentMethod:String
    var email:String
    var isCedirCardAdded:String
    var resellerAuthorizeArea:String
    
    init(pID:String,resellerID:String,originalPicture:String,firstName:String,lastName:String,access_Token:String, rating:String,numberDriverFavourites:String, spendings:String, noOfTrips:String,phoneNumber:String, selectedPaymentMethod:String,email:String, isCedirCardAdded:String, resellerAuthorizeArea:String)
    {
        self.pID =  pID
        self.resellerID = resellerID
        self.originalPicture = originalPicture
        self.firstName = firstName
        self.lastName = lastName
        self.access_Token = access_Token
        self.rating = rating
        self.numberDriverFavourites = numberDriverFavourites
        self.spendings = spendings
        self.noOfTrips = noOfTrips
        self.phoneNumber = phoneNumber
        self.selectedPaymentMethod = selectedPaymentMethod
        self.email = email
        self.isCedirCardAdded = isCedirCardAdded
        self.resellerAuthorizeArea = resellerAuthorizeArea
    }
    
    required  init? (coder aDecoder: NSCoder) {
        self.pID = aDecoder.decodeObject(forKey: "pID") as! String
        self.resellerID = aDecoder.decodeObject(forKey: "resellerID") as! String
        self.originalPicture = aDecoder.decodeObject(forKey: "originalPicture") as! String
        self.firstName = aDecoder.decodeObject(forKey: "firstName") as! String
        self.lastName = aDecoder.decodeObject(forKey: "lastName") as! String
        self.access_Token = aDecoder.decodeObject(forKey: "access_Token") as! String
        self.rating = aDecoder.decodeObject(forKey: "rating") as! String
        self.numberDriverFavourites = aDecoder.decodeObject(forKey: "numberDriverFavourites") as! String
        self.spendings = aDecoder.decodeObject(forKey: "spendings") as! String
        self.noOfTrips = aDecoder.decodeObject(forKey: "noOfTrips") as! String
        self.phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as! String
        self.selectedPaymentMethod = aDecoder.decodeObject(forKey: "selectedPaymentMethod") as! String
        self.email = aDecoder.decodeObject(forKey: "email") as! String
        self.isCedirCardAdded = aDecoder.decodeObject(forKey: "isCedirCardAdded") as! String
        self.resellerAuthorizeArea = aDecoder.decodeObject(forKey: "resellerAuthorizeArea") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(pID, forKey: "pID")
        aCoder.encode(resellerID, forKey: "resellerID" )
        aCoder.encode(originalPicture, forKey: "originalPicture")
        aCoder.encode(firstName, forKey: "firstName" )
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(access_Token, forKey: "access_Token" )
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(numberDriverFavourites, forKey: "numberDriverFavourites" )
        aCoder.encode(spendings, forKey: "spendings")
        aCoder.encode(noOfTrips, forKey: "noOfTrips" )
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(selectedPaymentMethod, forKey: "selectedPaymentMethod" )
        aCoder.encode(email, forKey: "email")
        aCoder.encode(isCedirCardAdded, forKey: "isCedirCardAdded")
        aCoder.encode(resellerAuthorizeArea, forKey: "resellerAuthorizeArea")
    }
    
    static func fromJSON(_ json:[String: Any]) -> LoginUser {
        let json = JSON(json)
        
        let pID = json["pID"].stringValue
        //let resellerID:String
        let originalPicture = json["originalPicture"].stringValue
        let firstName = json["firstName"].stringValue
        let lastName = json["lastName"].stringValue
        let access_Token = json["access_Token"].stringValue
        let rating = json["rating"].stringValue
        let numberDriverFavourites = json["numberDriverFavourites"].stringValue
        let spendings = json["spendings"].stringValue
        let noOfTrips = json["noOfTrips"].stringValue
        let phoneNumber = json["phoneNumber"].stringValue
        let selectedPaymentMethod = json["selectedPaymentMethod"].stringValue
        let email = json["email"].stringValue
        //let isCedirCardAdded = json["isCedirCardAdded"].stringValue
        
        return LoginUser(pID: pID, resellerID: "", originalPicture: originalPicture, firstName: firstName, lastName: lastName, access_Token: access_Token, rating: rating, numberDriverFavourites: numberDriverFavourites, spendings: spendings, noOfTrips: noOfTrips, phoneNumber: phoneNumber, selectedPaymentMethod: selectedPaymentMethod, email: email, isCedirCardAdded: "", resellerAuthorizeArea:"")
    }
    
}
