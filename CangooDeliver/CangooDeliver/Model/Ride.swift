//
//  AcceptRide.swift
//  CanTaxi-User
//
//  Created by Abubakar on 20/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class Ride: NSObject {
    
    var tripID:String
    var pickUplatitude:String
    var pickUplongitude:String
    var dropOfflatitude:String
    var dropOfflongitude:String
    var driverID:String
    var driverName:String
    var driverPicture:String
    var driverRating:Double
    var driverContactNumber:String
    var vehicleNumber:String
    var model:String
    var make:String
    var vehicleRating:Double
    var isWeb:Bool
    var isLaterBooking:Bool
    var isDispatchedRide:Bool
    var laterBookingPickUpDateTime:String
    var cancelReason:[CancelReasons]
    
    init(tripID:String,pickUplatitude:String,pickUplongitude:String,dropOfflatitude:String, dropOfflongitude:String, driverID:String, driverName:String, driverPicture:String, driverRating:Double, driverContactNumber:String, vehicleNumber:String, model:String, make:String, vehicleRating:Double, isWeb:Bool, isLaterBooking:Bool, isDispatchedRide:Bool, laterBookingPickUpDateTime:String,cancelReason:[CancelReasons])
    {
        self.tripID = tripID
        self.pickUplatitude = pickUplatitude
        self.pickUplongitude = pickUplongitude
        self.dropOfflatitude = dropOfflatitude
        self.dropOfflongitude = dropOfflongitude
        self.driverID = driverID
        self.driverName = driverName
        self.driverPicture = driverPicture
        self.driverRating = driverRating
        self.driverContactNumber = driverContactNumber
        self.vehicleNumber = vehicleNumber
        self.model = model
        self.make = make
        self.vehicleRating = vehicleRating
        self.isWeb = isWeb
        self.isLaterBooking = isLaterBooking
        self.isDispatchedRide = isDispatchedRide
        self.laterBookingPickUpDateTime = laterBookingPickUpDateTime
        self.cancelReason = cancelReason
    }
    static func fromJSON(_ json:[String: Any]) -> Ride {
        let json = JSON(json)
    let tripID = json["tripID"].stringValue
    let pickUplatitude = json["pickUplatitude"].stringValue
    let pickUplongitude = json["pickUplongitude"].stringValue
    let dropOfflatitude = json["dropOfflatitude"].stringValue
    let dropOfflongitude = json["dropOfflongitude"].stringValue
    let driverID = json["driverID"].stringValue
    let driverName = json["driverName"].stringValue
    let driverPicture = json["driverPicture"].stringValue
    let driverRating = json["driverRating"].doubleValue
    let driverContactNumber = json["driverContactNumber"].stringValue
    let vehicleNumber = json["vehicleNumber"].stringValue
    let model = json["model"].stringValue
    let make = json["make"].stringValue
    let vehicleRating = json["vehicleRating"].doubleValue
    let isWeb = json["isWeb"].boolValue
    let isLaterBooking = json["isLaterBooking"].boolValue
    let isDispatchedRide = json["isDispatchedRide"].boolValue
    let laterBookingPickUpDateTime = json["laterBookingPickUpDateTime"].stringValue
        var cancelReason:[CancelReasons] = []
        if let arrCancelReasons  = json["lstCancel"].arrayObject as? [[String:Any]]
        {
            for cr in arrCancelReasons
            {
                let bg = CancelReasons.fromJSON(cr)
                cancelReason.append(bg)
            }
        }
        
        return Ride(tripID: tripID, pickUplatitude: pickUplatitude, pickUplongitude: pickUplongitude, dropOfflatitude: dropOfflatitude, dropOfflongitude: dropOfflongitude, driverID: driverID, driverName: driverName, driverPicture: driverPicture, driverRating: driverRating, driverContactNumber: driverContactNumber, vehicleNumber: vehicleNumber, model: model, make: make, vehicleRating: vehicleRating, isWeb: isWeb, isLaterBooking: isLaterBooking, isDispatchedRide: isDispatchedRide, laterBookingPickUpDateTime: laterBookingPickUpDateTime, cancelReason: cancelReason)
    }
}
