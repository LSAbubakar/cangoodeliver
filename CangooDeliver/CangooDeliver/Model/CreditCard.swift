//
//  CreditCard.swift
//  CanTaxi-User
//
//  Created by Abubakar on 22/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class CreditCard: NSObject {
    var cardId:String
    var brand:String
    var expiryMonth:String
    var expiryYear:String
    var last4Digits:String
    var cardHolderName:String
    var cardDescription:String
    
    init(cardId:String,brand:String,expiryMonth:String,expiryYear:String,last4Digits:String, cardHolderName:String, cardDescription:String)
    {
    self.cardId = cardId
        self.brand = brand
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
        self.last4Digits = last4Digits
        self.cardHolderName = cardHolderName
        self.cardDescription = cardDescription
}
    static func fromJSON(_ json:[String: Any]) -> CreditCard {
        let json = JSON(json)
        let cardId = json["cardId"].stringValue
        let brand = json["brand"].stringValue
        let expiryMonth = json["expiryMonth"].stringValue
        let expiryYear = json["expiryYear"].stringValue
        let last4Digits = json["last4Digits"].stringValue
        let cardHolderName = json["cardHolderName"].stringValue
        let cardDescription = json["cardDescription"].stringValue
        
    return CreditCard(cardId: cardId, brand: brand, expiryMonth: expiryMonth, expiryYear: expiryYear, last4Digits: last4Digits, cardHolderName: cardHolderName, cardDescription: cardDescription)
    }
}

