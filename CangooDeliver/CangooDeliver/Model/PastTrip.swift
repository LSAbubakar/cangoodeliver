//
//  PastTrip.swift
//  CanTaxi-User
//
//  Created by Abubakar on 25/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class PastTrip: NSObject {
    var tripID:String
    var PickupLocation:String
    var DropOffLocation:String
    var tripDuration:String
    var totalFare:String
    var BookingDateTime:String
    var PickUpBookingDateTime:String
    var status:String
    var DistanceTraveled:String
    var Tip:String
    var paymentMethod:String
    var requestType:String
    var lstFacility:[Facilities]
    
    init(tripID:String,PickupLocation:String,DropOffLocation:String,tripDuration:String, totalFare:String, BookingDateTime:String, PickUpBookingDateTime:String,status:String,DistanceTraveled:String, Tip:String, paymentMethod:String,requestType:String, lstFacility:[Facilities] )
    {
        self.tripID = tripID
        self.PickupLocation = PickupLocation
        self.DropOffLocation = DropOffLocation
        self.tripDuration = tripDuration
        self.totalFare = totalFare
        self.BookingDateTime = BookingDateTime
        self.PickUpBookingDateTime = PickUpBookingDateTime
        self.status = status
        self.DistanceTraveled = DistanceTraveled
        self.Tip = Tip
        self.paymentMethod = paymentMethod
        self.lstFacility = lstFacility
        self.requestType = requestType
    }
    static func fromJSON(_ json:[String: Any]) -> PastTrip {
        let json = JSON(json)
        let tripID = json["tripID"].stringValue
        let PickupLocation = json["PickupLocation"].stringValue
        let DropOffLocation = json["DropOffLocation"].stringValue
        let tripDuration = json["tripDuration"].stringValue
        let totalFare = json["totalFare"].stringValue
        let BookingDateTime = json["BookingDateTime"].stringValue
        let PickUpBookingDateTime = json["PickUpBookingDateTime"].stringValue
        let status = json["status"].stringValue
        let DistanceTraveled = json["DistanceTraveled"].stringValue
        let Tip = json["Tip"].stringValue
        let paymentMethod = json["paymentMethod"].stringValue
        let requestType = json["requestType"].stringValue
        var lstFaclities:[Facilities] = []
        if let arrfaci  = json["facilities"].arrayObject as? [[String:Any]]
        {
            for cr in arrfaci
            {
                let bg = Facilities.fromJSON(cr)
                lstFaclities.append(bg)
            }
        }
        
        return PastTrip(tripID: tripID, PickupLocation: PickupLocation, DropOffLocation: DropOffLocation, tripDuration: tripDuration, totalFare: totalFare, BookingDateTime: BookingDateTime, PickUpBookingDateTime: PickUpBookingDateTime, status: status, DistanceTraveled: DistanceTraveled, Tip: Tip, paymentMethod: paymentMethod, requestType: requestType, lstFacility: lstFaclities)
    }
}
