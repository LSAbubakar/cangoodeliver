//
//  ParentViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 11/09/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import GoogleMaps
import NotificationBannerSwift

class ParentViewController: UIViewController {
    
let notificationCenter = NotificationCenter.default
    var banner:StatusBarNotificationBanner = StatusBarNotificationBanner(title: NSLocalizedString("Internet connection not available!", comment: ""), style: .danger)
    let vw = UIView()
    var isBannerShow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vw.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        vw.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        self.view.addSubview(vw)
        self.vw.isHidden = true
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.checkLocNotiofication),
                                       name: Notification.Name("checkLocNoti"),
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(self.openShowInternetOffMsg),
                                       name: Notification.Name("internetOffMsg"),
                                       object: nil)
//        notificationCenter.addObserver(self,
//                                       selector: #selector(self.closeInterOffBanner),
//                                       name: Notification.Name("internetOnMsg"),
//                                       object: nil)
        //self.checkLocNotiofication()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        notificationCenter.removeObserver(self, name: NSNotification.Name("checkLocNoti"), object: nil)
    }
    @objc private func checkLocNotiofication()
    {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                self.openLocationPopSettings()
            case .restricted:
                self.openLocationPopSettings()
            case .denied:
                self.openLocationPopSettings()
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            }
        }
        ActionHandler.sharedInstance.checkIsNotificationPermissionGranted(completion: { (success) -> Void in
            if success
            {
                
            }
            else
            {
                self.openShowAlertNotificationPermission()
            }
        })
    }
    
    private func openLocationPopSettings()
    {
        self.present(ActionHandler.sharedInstance.alertForNotificationPermission(message: NSLocalizedString("We need to access your location. Please permit the permission through settings.", comment: "")), animated: true, completion: nil)
    }
    
    private func openShowAlertNotificationPermission()
    {
        self.present(ActionHandler.sharedInstance.alertForNotificationPermission(message: NSLocalizedString("We need to access your notification. Please permit the permission through settings.", comment: "")), animated: true, completion: nil)
    }
    
    @objc private func openShowInternetOffMsg()
    {
        if !Connectivity.isConnectedToInternet() {
            if !self.isBannerShow
            {
                self.isBannerShow = true
                banner.autoDismiss = false
                self.banner.dismiss()
                self.banner.show()
                self.vw.isHidden = false
                self.view.bringSubviewToFront(vw)
            }
        }
        else
        {
            self.vw.isHidden = true
            self.isBannerShow = false
            self.banner.dismiss()
        }
    }
    public func tokenValidation()
    {
        checkToken { (response) in
            guard response.result.isSuccess else {
                return
            }
            if let _JSON = response.result.value
            {
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.navigationController?.viewControllers.removeAll()
                            //Constants.Currentlocation.isOnline = false
                            //ActionHandler.sharedInstance.delAllTripDefaults()
                            UserDefaults.standard.removeObject(forKey: "LoginUserData")
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //appDelegate.logoutOnAuthFaild()
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Your session is expired. Please login again", comment: "")), animated: true, completion: nil)
                            return
                        }
                    }
                }
                catch{}
            }
        }
    }
}
