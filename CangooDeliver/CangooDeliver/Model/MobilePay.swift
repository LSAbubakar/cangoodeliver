//
//  MobilePay.swift
//  CanTaxi-User
//
//  Created by Abubakar on 24/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//
//public string pickUplatitude { get; set; }
//public string pickUplongitude { get; set; }
//public string dropOfflatitude { get; set; }
//public string dropOfflongitude { get; set; }
//public string driverID { get; set; }
//public string ressellerID { get; set; }
//public string estimatedFare { get; set; }
//public string isOverride { get; set; }
//public string distance { get; set; }
//public string vehicleID { get; set; }
//public string paymentMode { get; set; }




import Foundation
import SwiftyJSON

class MobilePay: NSObject {
    
    var estmateFare:String
    var distance:String
    var paymentMode:String
    var paypalAccount:String
    var duration:String
    var isPaymentRequested:Bool
    var tripID:String
    var isOverride:String
    var vehicleID:String
    var voucherUsedAmount:String
    var walletAmountUsed:String
    var promoDiscountAmount:String
    var totalFare:String
    var fleetID:String
    var walletTotalAmount:String
    var collectedAmount:String
    //walk-inn
    var pickUplatitude:String
    var pickUplongitude:String
    var dropOfflatitude:String
    var dropOfflongitude:String
    var driverID:String
    var ressellerID:String
    var estimatedFare:String
    var PaymentMode:String
    //firebas
    
    
     init(estmateFare:String,distance:String,paymentMode:String,paypalAccount:String,duration:String, isPaymentRequested:Bool,tripID:String, isOverride:String, vehicleID:String, pickUplatitude:String, pickUplongitude:String, dropOfflatitude:String, dropOfflongitude:String,driverID:String, ressellerID:String, estimatedFare:String,PaymentMode:String,voucherUsedAmount:String, walletAmountUsed:String,promoDiscountAmount:String,totalFare:String, fleetID:String,walletTotalAmount:String,collectedAmount:String)
     {
        self.estmateFare = estmateFare
        self.distance = distance
        self.paymentMode = paymentMode
        self.paypalAccount = paypalAccount
        self.duration = duration
        self.isPaymentRequested = isPaymentRequested
        self.tripID = tripID
        self.isOverride = isOverride
        self.vehicleID = vehicleID
        //walk-in
        self.pickUplatitude = pickUplatitude
        self.pickUplongitude = pickUplongitude
        self.dropOfflatitude = dropOfflatitude
        self.dropOfflongitude = dropOfflongitude
        self.driverID = driverID
        self.ressellerID = ressellerID
        self.estimatedFare = estimatedFare
        self.PaymentMode = PaymentMode
        self.voucherUsedAmount = voucherUsedAmount
        self.walletAmountUsed = walletAmountUsed
        self.promoDiscountAmount = promoDiscountAmount
        self.totalFare = totalFare
        self.fleetID = fleetID
        self.walletTotalAmount = walletTotalAmount
        self.collectedAmount = collectedAmount
    }
    
    static func fromJSON(_ json:[String: Any]) -> MobilePay {
        let json = JSON(json)
        let estmateFare = json["estmateFare"].stringValue
        let distance = json["distance"].stringValue
        let paymentMode = json["paymentMode"].stringValue
        let paypalAccount = json["paypalAccount"].stringValue
        let duration = json["duration"].stringValue
        let isPaymentRequested = json["isPaymentRequested"].boolValue
        let tripID = json["tripID"].stringValue
        let isOverride = json["isOverride"].stringValue
        let vehicleID = json["vehicleID"].stringValue
        let voucherUsedAmount = json["voucherUsedAmount"].stringValue
        let walletAmountUsed =  json["walletAmountUsed"].stringValue
        let promoDiscountAmount = json["promoDiscountAmount"].stringValue
        let totalFare = json["totalFare"].stringValue
        let walletTotalAmount = json["walletTotalAmount"].stringValue
        let collectedAmount = json["collectedAmount"].stringValue
    //walk-in
        let pickUplatitude = json["pickUplatitude"].stringValue
        let pickUplongitude = json["pickUplongitude"].stringValue
        let dropOfflatitude = json["dropOfflatitude"].stringValue
        let dropOfflongitude = json["dropOfflongitude"].stringValue
        let driverID = json["driverID"].stringValue
        let ressellerID = json["ressellerID"].stringValue
        let estimatedFare = json["estimatedFare"].stringValue
        let PaymentMode = json["PaymentMode"].stringValue
        let fleetID = json["fleetID"].stringValue
        
        return MobilePay(estmateFare: estmateFare, distance: distance, paymentMode: paymentMode, paypalAccount: paypalAccount, duration: duration, isPaymentRequested: isPaymentRequested, tripID: tripID, isOverride: isOverride, vehicleID: vehicleID, pickUplatitude: pickUplatitude, pickUplongitude: pickUplongitude, dropOfflatitude: dropOfflatitude, dropOfflongitude: dropOfflongitude, driverID: driverID, ressellerID: ressellerID, estimatedFare: estimatedFare,PaymentMode:PaymentMode, voucherUsedAmount: voucherUsedAmount, walletAmountUsed: walletAmountUsed, promoDiscountAmount: promoDiscountAmount, totalFare: totalFare, fleetID: fleetID, walletTotalAmount: walletTotalAmount, collectedAmount: collectedAmount)
    }
}
