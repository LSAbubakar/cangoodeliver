//
//  CancelReasons.swift
//  CanTaxi
//
//  Created by Abubakar on 20/02/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class CancelReasons: NSObject {
    var id:Int
    var reason:String
    
     init(id:Int,reason:String) {
        self.id = id
        self.reason = reason
    }
    static func fromJSON(_ json:[String: Any]) -> CancelReasons {
        let json = JSON(json)
        let id = json["id"].intValue
        let reason = json["reason"].stringValue
        
        return CancelReasons(id: id, reason: reason)
    }
}
