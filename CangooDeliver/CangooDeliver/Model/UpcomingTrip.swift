//
//  UpcomingTrip.swift
//  CanTaxi-User
//
//  Created by Abubakar on 24/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class UpcomingTrip: NSObject {
    var tripID:String
    var PickupLocation:String
    var DropOffLocation:String
    var tripDuration:String
    var PickUpBookingDateTime:String
    var status:String
    var DistanceTraveled:String
    var PickupLocationLatitude:String
    var PickupLocationLongitude:String
    var DropOffLocationLatitude:String
    var DropOffLocationLongitude:String
    var lstFaclities:[Facilities]
    
    init(tripID:String,PickupLocation:String,DropOffLocation:String,tripDuration:String, PickUpBookingDateTime:String,status:String, DistanceTraveled:String, PickupLocationLatitude:String, PickupLocationLongitude:String, DropOffLocationLatitude:String,DropOffLocationLongitude:String,lstFaclities:[Facilities] )
    {
        self.tripID = tripID
        self.PickupLocation = PickupLocation
        self.DropOffLocation = DropOffLocation
        self.tripDuration = tripDuration
        self.PickUpBookingDateTime = PickUpBookingDateTime
        self.status = status
        self.DistanceTraveled = DistanceTraveled
        self.PickupLocationLatitude = PickupLocationLatitude
        self.PickupLocationLongitude = PickupLocationLongitude
        self.DropOffLocationLatitude = DropOffLocationLatitude
        self.DropOffLocationLongitude = DropOffLocationLongitude
        self.lstFaclities = lstFaclities
    }
    static func fromJSON(_ json:[String: Any]) -> UpcomingTrip {
        let json = JSON(json)
        let tripID = json["tripID"].stringValue
        let PickupLocation = json["PickupLocation"].stringValue
        let DropOffLocation = json["DropOffLocation"].stringValue
        let tripDuration = json["tripDuration"].stringValue
        let PickUpBookingDateTime = json["PickUpBookingDateTime"].stringValue
        let status = json["status"].stringValue
        let DistanceTraveled = json["DistanceTraveled"].stringValue
        let PickupLocationLatitude = json["PickupLocationLatitude"].stringValue
        let PickupLocationLongitude = json["PickupLocationLongitude"].stringValue
        let DropOffLocationLatitude = json["DropOffLocationLatitude"].stringValue
        let DropOffLocationLongitude = json["DropOffLocationLongitude"].stringValue
        
        var lstFaclities:[Facilities] = []
        if let arrfaci  = json["facilities"].arrayObject as? [[String:Any]]
        {
            for cr in arrfaci
            {
                let bg = Facilities.fromJSON(cr)
                lstFaclities.append(bg)
            }
        }
        
        
        return UpcomingTrip(tripID: tripID, PickupLocation: PickupLocation, DropOffLocation: DropOffLocation, tripDuration: tripDuration, PickUpBookingDateTime: PickUpBookingDateTime, status: status, DistanceTraveled: DistanceTraveled, PickupLocationLatitude: PickupLocationLatitude, PickupLocationLongitude: PickupLocationLongitude, DropOffLocationLatitude: DropOffLocationLatitude, DropOffLocationLongitude: DropOffLocationLongitude, lstFaclities: lstFaclities)
    }
}
