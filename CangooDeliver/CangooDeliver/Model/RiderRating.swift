//
//  RiderRating.swift
//  CanTaxi-User
//
//  Created by Abubakar on 26/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class RiderRating:NSObject, NSCoding {
    var driverName:String
    var driverImage:String
    var date:String
    var time:String
    var isFav:String
    var distance:String
    var tripID:String
    
init(driverName:String,driverImage:String,date:String,time:String,isFav:String,distance:String,tripID:String)
{
    self.driverName = driverName
    self.driverImage = driverImage
    self.date = date
    self.time = time
    self.isFav = isFav
    self.distance = distance
    self.tripID = tripID
    }
    
    required  init? (coder aDecoder: NSCoder) {
        self.driverName = aDecoder.decodeObject(forKey: "driverName") as! String
        self.driverImage = aDecoder.decodeObject(forKey: "driverImage") as! String
        self.date = aDecoder.decodeObject(forKey: "date") as! String
        self.time = aDecoder.decodeObject(forKey: "time") as! String
        self.isFav = aDecoder.decodeObject(forKey: "isFav") as! String
        self.distance = aDecoder.decodeObject(forKey: "distance") as! String
        self.tripID = aDecoder.decodeObject(forKey: "tripID") as! String
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(driverName, forKey: "driverName")
        aCoder.encode(driverImage, forKey: "driverImage" )
        aCoder.encode(date, forKey: "date")
        aCoder.encode(time, forKey: "time" )
        aCoder.encode(isFav, forKey: "isFav")
        aCoder.encode(distance, forKey: "distance" )
        aCoder.encode(tripID, forKey: "tripID")
    }
    
    static func fromJSON(_ json:[String: Any]) -> RiderRating {
        let json = JSON(json)
        let driverName = json["driverName"].stringValue
        let driverImage = json["driverImage"].stringValue
        let date = json["date"].stringValue
        let time = json["time"].stringValue
        let isFav = json["isFav"].stringValue
        let distance = json["distance"].stringValue
        let tripID = json["tripID"].stringValue
        
        return RiderRating(driverName: driverName, driverImage: driverImage, date: date, time: time, isFav: isFav, distance: distance, tripID: tripID)
    }
}
