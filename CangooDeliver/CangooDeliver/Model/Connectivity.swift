//
//  Connectivity.swift
//  CanTaxi-User
//
//  Created by Abubakar on 01/05/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
