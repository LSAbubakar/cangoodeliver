//
//  Facilities.swift
//  CanTaxi-User
//
//  Created by Abubakar on 30/07/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import SwiftyJSON

class Facilities: NSObject {
    var facilityID:String
    var facilityName:String
    var facilityIcon:String
    
    init(facilityID:String,facilityName:String,facilityIcon:String)
    {
        self.facilityID = facilityID
        self.facilityName = facilityName
        self.facilityIcon = facilityIcon
    }
    
    static func fromJSON(_ json:[String: Any]) -> Facilities {
        let json = JSON(json)
        let FacilityID = json["facilityID"].stringValue
        let FacilityName = json["facilityName"].stringValue
        let FacilityIcon = json["facilityIcon"].stringValue
        
        return Facilities(facilityID: FacilityID, facilityName: FacilityName, facilityIcon: FacilityIcon)
    }
}
