//
//  ActionHandler.swift
//  CanTaxi-User
//
//  Created by Abubakar on 20/12/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD
import CoreLocation
import Firebase
import SystemConfiguration
import UserNotifications

class ActionHandler: NSObject,CLLocationManagerDelegate {
    
    var isInertOff = true
    
    let locationManager = CLLocationManager()
    static let sharedInstance : ActionHandler = {
        let instance = ActionHandler()
        return instance
    }()
    func setStatusBarColor(color:UIColor) -> UIView {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        // let statusBarColor = UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)
        statusBarView.backgroundColor = color
        return statusBarView
    }
    func showAlert(message:String)-> UIAlertController
    {        let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        alert.view.tintColor = Constants.Common.appYellowColor
        return alert
    }
    func showAlertPopViewController(message:String, vw:UIViewController)-> UIAlertController
    {        let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (at) in
            vw.navigationController?.popViewController(animated: true)
        }))
        alert.view.tintColor = Constants.Common.appYellowColor
        return alert
    }
    func showAlertDismisViewController(message:String, vw:UIViewController)-> UIAlertController
    {        let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (at) in
            vw.dismiss(animated: true, completion: nil)
        }))
        alert.view.tintColor = Constants.Common.appYellowColor
        return alert
    }
    func showAlertPushViewController(message:String, vwParent:UIViewController, moveVW:UIViewController)-> UIAlertController
    {        let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (at) in
            vwParent.navigationController?.pushViewController(moveVW, animated: true)
        }))
        alert.view.tintColor = Constants.Common.appYellowColor
        return alert
    }
    
    func alertForNotificationPermission(message:String)-> UIAlertController
    {
        // add your own
        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }
        alertController.addAction(settingsAction)
        return alertController
    }
    
    func delUpdateUserDefaultsForUserData(uData:LoginUser){
        UserDefaults.standard.removeObject(forKey: "LoginUserData")
        let dt = try! NSKeyedArchiver.archivedData(withRootObject: uData, requiringSecureCoding: false)
        UserDefaults.standard.set(dt, forKey: "LoginUserData")
    }
    func delUpdateUserDefaultsForRatingData(rData:RiderRating){
        UserDefaults.standard.removeObject(forKey: "RiderRating")
        
        let dt = try! NSKeyedArchiver.archivedData(withRootObject: rData, requiringSecureCoding: false)
        UserDefaults.standard.set(dt, forKey: "RiderRating")
    }
    func addTimeZone(date:String, intervelInSeconds:Int)-> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormat.serverDateformat
        var date = dateFormatter.date(from: date)
        date = date?.addingTimeInterval(TimeInterval(intervelInSeconds))
        return date
    }
    func getDatePart(date:Date) -> (date:String, time:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormat.date
        let datePart = dateFormatter.string(from: date)
        dateFormatter.dateFormat = Constants.dateFormat.time
        let timePart = dateFormatter.string(from: date)
        return (datePart, timePart)
    }
    func getDatePartList(date:Date) -> (date:String, time:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormat.dateList
        let datePart = dateFormatter.string(from: date)
        dateFormatter.dateFormat = Constants.dateFormat.timeList
        let timePart = dateFormatter.string(from: date)
        return (datePart, timePart)
    }
    
    func delAllTripDefaults() -> Void {
        let prefs = UserDefaults.standard
    }
    func locationUpdate()
    {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.activityType = .automotiveNavigation
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //get bearing
        if Constants.Currentlocation.latitude != "" && Constants.Currentlocation.longitude != ""
        {
            let lat1 = degreesToRadians(degrees: Double(Constants.Currentlocation.latitude)!)
            let lon1 = degreesToRadians(degrees: Double(Constants.Currentlocation.longitude)!)
            
            let lat2 = degreesToRadians(degrees: locValue.latitude)
            let lon2 = degreesToRadians(degrees: locValue.longitude)
            
            let dLon = lon2 - lon1
            
            let y = sin(dLon) * cos(lat2)
            let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
            let radiansBearing = atan2(y, x)
            
            Constants.Currentlocation.bearing = "\(radiansToDegrees(radians: radiansBearing))"
        }
        print(manager.location?.horizontalAccuracy ??  0)
        Constants.Currentlocation.latitude = "\(locValue.latitude)"
        Constants.Currentlocation.longitude = "\(locValue.longitude)"
        var isInertnet = 0.0
        if Connectivity.isConnectedToInternet()
        {
            isInertnet = 1.0
        }
        //loc update on firebase
        self.updateLocationOnFB(lat:"\(locValue.latitude)", Long:"\(locValue.longitude)")
        
        let location:[String: Double] = ["Latitude": locValue.latitude, "Logitude": locValue.longitude, "isInertnet":isInertnet]
        
        NotificationCenter.default.post(name: Notification.Name("locationUpdation"), object: nil, userInfo:location)
        NotificationCenter.default.post(name: Notification.Name("internetOffMsg"), object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var dic:[String:Bool] = [:]
        if status == .denied || status == .notDetermined || status == .restricted
        {
            dic = ["isDenied":true]
            
        }
        else if status == .authorizedAlways || status == .authorizedWhenInUse
        {
            dic = ["isDenied":false]
        }
        NotificationCenter.default.post(name: NSNotification.Name("locationPermissionDenied"), object:nil, userInfo: dic)
        NotificationCenter.default.post(name: NSNotification.Name("checkLocNoti"), object:nil, userInfo: dic)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        Constants.Currentlocation.heating = "\(newHeading.trueHeading)"
    }
    private func updateLocationOnFB(lat:String, Long:String)
    {
        if SharedManager.sharedInstance.userData != nil && Constants.Common.isOnline
        {
            let uData = SharedManager.sharedInstance.userData
            let dic = ["latitude":lat, "longitude":Long, "passengerID":uData?.pID, "passengerName":uData!.firstName + " " + uData!.lastName, "passengerPhone":uData?.phoneNumber, "passengerPicture":uData?.originalPicture ]
            let refDB = Database.database().reference()
            refDB.child("OnlineCustomers").child(uData!.pID).setValue(dic)
        }
        
    }
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func isValidEmail(emailText:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailText)
    }
}
extension UIView {
    func dropShadow(scale: Bool = true, radius:CGFloat) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = radius
    }
    
    func dropShadowBookingRide(scale: Bool = true, radius:CGFloat) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = radius
    }

    func headerView()
    {
        clipsToBounds = true
        layer.cornerRadius = 8
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    func addBordWhAppClr()
    {
        layer.borderColor = Constants.Common.appYellowColor.cgColor
        layer.borderWidth = 2
        clipsToBounds = true
    }
    func removeBrdrClr()
    {
        layer.borderColor = UIColor.white.cgColor
        clipsToBounds = true
    }
    func cornerRadius(radius:CGFloat)
    {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    func specificCornerRadius(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
extension UITextField
{
    func textFieldRoundCorner()
    {
        layer.cornerRadius = 8
        clipsToBounds = true
        font = UIFont.init(name: "Work Sans-Medium", size: 15.0)
        tintColor = Constants.Common.appYellowColor
        textAlignment = .center
    }
    func textfieldWithLeftAlignment()
    {
        layer.cornerRadius = 8
        clipsToBounds = true
        font = UIFont.init(name: "Work Sans-Medium", size: 15.0)
        tintColor = Constants.Common.appYellowColor
        textAlignment = .left
    }
}
extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Work-Sans", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }

extension UIImage {
    
    public static func loadFrom(url: URL, completion: @escaping (_ image: UIImage?) -> ()) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    completion(UIImage(data: data))
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
        
        public func maskWithColor(color: UIColor) -> UIImage {
            
            UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
            let context = UIGraphicsGetCurrentContext()!
            
            let rect = CGRect(origin: CGPoint.zero, size: size)
            
            color.setFill()
            self.draw(in: rect)
            
            context.setBlendMode(.sourceIn)
            context.fill(rect)
            
            let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return resultImage
        }
}

public extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}
extension ActionHandler //notification
{
    func checkIsNotificationPermissionGranted( completion: @escaping (Bool) -> ())
    {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                completion(true)
            }
            else if settings.authorizationStatus == .denied {
                completion(false)
            }
        }
    }
}
