//
//  Constants.swift
//  CanTaxi
//
//  Created by Abubakar on 16/01/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import RappleProgressHUD

struct Constants {
    struct Server {
        static let serverURL = "https://api.cangoo.at/api/user/" //"http://apicantaxi.agiletechstudio.net/api/user/"
        //"http://demoapi.agiletechstudio.net/api/user/"
       // static let serverURL = "http://33285a62.ngrok.io/api" //live
        static let profilePic = "https://business.cangoo.at/"
        static let passengerPicUrl = "https://api.cangoo.at/"
        static let googleAPIKey = "AIzaSyDUyBlK-oylKS-eqy5MalCgU78nKdtneH0"
        //"AIzaSyCzry9cfcNwunHBGZ6WTiAth5oWFL8bpy4"
        static let pushySecretKey = "3a26915c48451df8f243d25b07cecb8598e99d6dd8670ff31888901b4c9e6cc4"
        static let paypalSandbox = "AdEZ_p-0m8Wmmf8a8-y39w3dltVt6CHsnebizlZYe1pOEHLO3LEBoVR6oJ2K2Cxuh9qmWlWAoeemfxnN"
        static let paypalPublishKey = "AQeTotokPtvICMDKlT7KrPGrMwRuIp60kwAU09938u9I9eOPCsykv0wJA7Az6Urh69hEx6WaVWZHJ1I8"
        static let stripTestKey = "pk_test_nf76ubG8IElElYHcBR2AcBBE00oD9KeUiS"
        static let stripPublishKey = "pk_live_dYOwqaqZBelbzmEE5kPUsicw00BcdyDf8D"
        static let resellerID = "73BABE98-3CA1-49E0-BE0A-1638B154762D"
        static let applicationID = "CF8AF4B2-371D-46DD-AD66-65B6A26FB889"
        
     //   PAYPAL_CLIENT_ID = "AdEZ_p-0m8Wmmf8a8-y39w3dltVt6CHsnebizlZYe1pOEHLO3LEBoVR6oJ2K2Cxuh9qmWlWAoeemfxnN";
       //         STRIPE_PUBLISHABLE_KEY = "pk_test_nf76ubG8IElElYHcBR2AcBBE00oD9KeUiS";


        //        Live Keys
        //        PAYPAL_CLIENT_ID = "AQeTotokPtvICMDKlT7KrPGrMwRuIp60kwAU09938u9I9eOPCsykv0wJA7Az6Urh69hEx6WaVWZHJ1I8";
        //        STRIPE_PUBLISHABLE_KEY ="pk_live_dYOwqaqZBelbzmEE5kPUsicw00BcdyDf8D";
    }
    struct AuthToken {
        static var authToken = ""
    }
    
    struct dateFormat {
        static let serverDateformat = "MM/dd/yyyy HH:mm:ss"
        static let date = "MM/dd/yyyy"
        static let dateList = "dd MMM yyyy"
        static let time = "HH:mm:ss"
        static let timeList = "HH:mm"
        static let dateWithAlpha = "dd MMM yyyy, HH:mm"
    }
    struct DeviceToken {
        static var deviceToken = ""
    }
    struct rappleAttribute {
        static let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: UIColor(red: 245/255, green: 152/255, blue: 23/255, alpha: 1.0), screenBG: .lightGray, progressBG: .black, progressBarBG: .orange, progreeBarFill: .blue, thickness: 4)
        static let attributesDash = RappleActivityIndicatorView.attribute(style: RappleStyle.apple, tintColor: Common.appYellowColor, screenBG: .gray, progressBG: .black, progressBarBG: .orange, progreeBarFill: .blue, thickness: 4)
    }
    struct Currentlocation {
        static var latitude = "0.0"
        static var longitude = "0.0"
        static var bearing = ""
        static var heating = "0.0"
    }
    struct CurrentUtcDateTime{
        static var utcDateTime = ""
    }
    struct Common {
        static var isWalkIn = false
        static var isLaterCancel = false
        static let appYellowColor = UIColor(red: 245/255, green: 152/255, blue: 23/255, alpha: 1.0)
        static let unSelectedColor = UIColor(red: 70/255, green: 70/255, blue: 70/255, alpha: 1.0)
        static let paypal = "PayPal"
        static let creditCard = "CreditCard"
        static let cash = "Cash"
        static let mobilePay = "MobilePay"
        static var isBookingAccepted = false
        static let maxZoom = 17.0
        static let minZoom = 10.0
        static let normalZoom = 15.0
        static var isOnline = false
    }
    struct tripStatus {
        static var onWay = "OnTheWay"
        static var Arrived = "Arrived"
        static let RideStart = "RideStarted"
    }
    struct CreditCardType {
        static let cardFriend = "cardFriend"
        static let cardCompany = "cardCompany"
        static let cardPersonal = "cardPersonal"
    }
    struct promoType
    {
        static let special = "special"
        static let fixed = "fixed"
        static let percentage = "percentage"
    }
    struct tripType {
        static let grocery = "Grocery"
        static let medicine = "Medicine"
        static let taxi = "Taxi"
    }
}
