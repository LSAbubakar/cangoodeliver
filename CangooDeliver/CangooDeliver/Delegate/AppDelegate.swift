//
//  AppDelegate.swift
//  CangooDeliver
//
//  Created by Abubakar on 19/03/2020.
//  Copyright © 2020 LS. All rights reserved.
//

import UIKit
import IQKeyboardManager
import GoogleMaps
import GooglePlaces
import Firebase
import RappleProgressHUD
import Stripe
import Pushy

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = false
        //IQKeyboardManager.shared().previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        GMSServices.provideAPIKey(Constants.Server.googleAPIKey)
        GMSPlacesClient.provideAPIKey(Constants.Server.googleAPIKey)
        
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: Constants.Server.paypalPublishKey,
                                                                PayPalEnvironmentSandbox: ""])
        STPPaymentConfiguration.shared().publishableKey = Constants.Server.stripPublishKey
        
        // Initialize Pushy SDK
        let pushyObj = Pushy(UIApplication.shared)
        // Register the device for push notifications
        pushyObj.register({ (error, deviceToken) in
            // Handle registration errors
            if error != nil {
                return print ("Registration failed: \(error!)")
            }
            // Print device token to console
            print("Pushy device token: \(deviceToken)")
            
            Constants.DeviceToken.deviceToken = deviceToken
            // Persist the token locally and send it to your backend later
            //UserDefaults.standard.set(deviceToken, forKey: "pushyToken")
        })
        //Constants.DeviceToken.deviceToken = "test123"
        //notification handling
        self.handlePushNotification(pushyObj: pushyObj)
        FirebaseApp.configure()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}
extension AppDelegate
{
    func handlePushNotification(pushyObj:Pushy)
    {
        //Handle push notifications
        pushyObj.setNotificationHandler({ (data, completionHandler) in
            if let key = data["messageKey"] as? String
            {
                if let request = data["data"] as? String
                {
                    let jsonData = request.data(using: String.Encoding.utf8)
                    do{
                        
                        if key == "pas_rideAccepted"
                        {
                            if let jdata = jsonData,
                                let json = try JSONSerialization.jsonObject(with: jdata) as? [String:Any]
                            {
                                if let topVC = UIApplication.getTopMostViewController() {
                                    
                                    self.alertNotification(data: data)
                                    //here open later booking
                                    NotificationCenter.default.post(name: NSNotification.Name("timerInvalidate"), object: nil)
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeliveryOnWayViewController") as! DeliveryOnWayViewController
                                    topVC.navigationController?.pushViewController(vw, animated: true)
                                    
                                }
                            }
                        }
                        else if key == "pas_driverReached"
                        {
                            
                            //                            if let topVC = UIApplication.getTopMostViewController()
                            //                            {
                            //                                self.alertNotification(data: data)
                            //                                //here open later booking
                            ////                                let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RidesViewController") as! RidesViewController
                            ////                                //vw.accepRide = ride
                            ////                                UserDefaults.standard.set(Date(), forKey: "arriveDateTime")
                            ////                                vw.tripStatus = Constants.tripStatus.Arrived
                            ////                                Constants.Common.isBookingAccepted = true
                            ////                                topVC.navigationController?.pushViewController(vw, animated: true)
                            //                                //  topVC.navigationController?.present(vw, animated: true, completion: nil)
                            //                            }
                            
                        }
                        else if key == "pas_rideStarted"
                        {
                            
                            if let topVC = UIApplication.getTopMostViewController()
                            {
                                self.alertNotification(data: data)
                                //here open later booking
                                let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeliveryOnWayViewController") as! DeliveryOnWayViewController
                                //                                vw.tripStatus = Constants.tripStatus.RideStart
                                //                                Constants.Common.isBookingAccepted = true
                                topVC.navigationController?.pushViewController(vw, animated: true)
                            }
                        }
                        else if key == "pas_endRideDetail"
                        {
                            if let jdata = jsonData,
                                let json = try JSONSerialization.jsonObject(with: jdata) as? [String:Any]
                            {
                                //                                if let dN = json["driverName"] as? String
                                //                                {
                                // let rideEnd = RiderRating.fromJSON(json)
                                // ActionHandler.sharedInstance.delUpdateUserDefaultsForRatingData(rData: rideEnd)
                                //Constants.Common.isBookingAccepted = false
                                if let topVC = UIApplication.getTopMostViewController()
                                {
                                    self.alertNotification(data: data)
                                    //here open later booking
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                    vw.isPaymentPending = true
                                    topVC.navigationController?.pushViewController(vw, animated: true)
                                    //}
                                }
                            }
                        }
                        else if key == "pas_paypalPayment"
                        {
                            if let jdata = jsonData,
                                let json = try JSONSerialization.jsonObject(with: jdata) as? [String:Any]
                            {
                                let mp = MobilePay.fromJSON(json)
                                if let topVC = UIApplication.getTopMostViewController()
                                {
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FarePaymentViewController") as! FarePaymentViewController
                                    RappleActivityIndicatorView.stopAnimation()
                                    let data:[String : Any] = ["paymentMethod":Constants.Common.paypal,"mobilePay":mp]
                                    NotificationCenter.default.post(name: NSNotification.Name("fareManagement"), object: nil, userInfo:data)
                                    vw.paymentMethod = Constants.Common.paypal
                                    vw.mobilePay = mp
                                    vw.modalPresentationStyle = .fullScreen
                                    topVC.navigationController?.present(vw, animated: true, completion: nil)
                                }
                            }
                        }
                        else if key == "pas_creditCardPayment"
                        {
                            if let jdata = jsonData,
                                let json = try JSONSerialization.jsonObject(with: jdata) as? [String:Any]
                            {
                                let mp = MobilePay.fromJSON(json)
                                if let topVC = UIApplication.getTopMostViewController()
                                {
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FarePaymentViewController") as! FarePaymentViewController
                                    RappleActivityIndicatorView.stopAnimation()
                                    let data:[String : Any] = ["paymentMethod":Constants.Common.creditCard,"mobilePay":mp]
                                    NotificationCenter.default.post(name: NSNotification.Name("fareManagement"), object: nil, userInfo:data)
                                    vw.mobilePay = mp
                                    vw.paymentMethod = Constants.Common.creditCard
                                    vw.modalPresentationStyle = .fullScreen
                                    topVC.navigationController?.present(vw, animated: true, completion: nil)
                                }
                            }
                        }
                            //                        else if key == "pas_LaterBookingTimeOut"
                            //                        {
                            //                            if let topVC = UIApplication.getTopMostViewController()
                            //                            {
                            ////                                let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
                            ////                                vw.isNotify = true
                            ////                                vw.modalPresentationStyle = .overCurrentContext
                            ////                                vw.message = NSLocalizedString("No captain accepted your later booking. Please book new ride again.", comment: "")
                            ////                                topVC.navigationController?.present(vw, animated: true, completion: nil)
                            //                            }
                            //                        }
                        else if key == "pas_rideCancel"
                        {
                            if let topVC = UIApplication.getTopMostViewController()
                            {
                                let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                vw.isRideCancelledByRider = true
                                //vw.modalPresentationStyle = .overCurrentContext
                                topVC.navigationController?.pushViewController(vw, animated: true)
                            }
                        }
                        else if key == "pas_CashPaymentPaid"
                        {
                            if let jdata = jsonData,
                                let json = try JSONSerialization.jsonObject(with: jdata) as? [String:Any]
                            {
                                let mp = MobilePay.fromJSON(json)
                                if let topVC = UIApplication.getTopMostViewController()
                                {
                                    if topVC is FarePaymentViewController
                                    {
                                        NotificationCenter.default.post(name: NSNotification.Name("isPaymentCash"), object: nil)
                                    }
                                    RappleActivityIndicatorView.stopAnimation()
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CashPaymentConfimationViewController") as! CashPaymentConfimationViewController
                                    vw.trp = mp
                                    vw.modalPresentationStyle = .overCurrentContext
                                    topVC.present(vw, animated: true, completion: nil)
                                }
                            }
                        }
                        
                    }catch{}
                }
            }
            completionHandler(UIBackgroundFetchResult.newData)
        })
    }
    func alertNotification(data:[AnyHashable:Any])
    {
        var message = "\(data)"
        print("notification data")
        print(data)
        
        // Attempt to extract "message" key from APNs payload
        if let aps = data["aps"] as? [AnyHashable : Any] {
            if let payloadMessage = aps["sound"] as? String {
                message = payloadMessage
            }
        }
    }
}
extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}
extension AppDelegate:UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if let key = notification.request.content.userInfo["messageKey"] as? String
        {
            if let request = notification.request.content.userInfo["data"] as? String
            {
                //self.openPushNotification(key: key, request: request, IsBG: false, notifData: ["":""])
            }
            // print(notification.request.content.userInfo)
            completionHandler([.sound])
        }
    }
}
