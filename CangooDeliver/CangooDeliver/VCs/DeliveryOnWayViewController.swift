//
//  DeliveryOnWayViewController.swift
//  CangooDeliver
//
//  Created by Abubakar on 22/03/2020.
//  Copyright © 2020 LS. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import MessageUI
import GoogleMaps
import RappleProgressHUD

class DeliveryOnWayViewController: UIViewController {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblVN: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var vwMap: UIView!
    @IBOutlet weak var vwInfo: UIView!
    @IBOutlet weak var btnMyLocation: UIButton!
    @IBOutlet weak var imgCarIcon: UIImageView!
    
    @IBOutlet weak var btnCancel: UIButton!
      @IBOutlet weak var vwConfHeader: UIView!
      @IBOutlet weak var lblConfirMsg: UILabel!
      @IBOutlet weak var btnNO: UIButton!
      @IBOutlet weak var btnYes: UIButton!
      @IBOutlet weak var vwCancelConfirmation: UIView!
      @IBOutlet weak var lblConfirmationCap: UILabel!
    
    var refFirDB:DatabaseReference!
    var phoneNumber = ""
    var trpID = ""
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var dropLat = ""
    var dropLon = ""
    var mapView:GMSMapView!
    let marker = GMSMarker()
    var camera:GMSCameraPosition!
    var isLocation = false
    var isMyLocationUpdated = true
    var rideStatus = "On The Way"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refFirDB =  Database.database().reference()
        self.getDataFromFireBase()
        self.lblVehicleNumber.text = NSLocalizedString("Vehicle Number", comment: "")
        self.btnMyLocation.addTarget(self, action: #selector(setMyLoc), for: .touchUpInside)
        self.btnCancel.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
        self.btnYes.setTitle(NSLocalizedString("YES", comment: ""), for: .normal)
        self.btnYes.addTarget(self, action: #selector(self.yesConfrm), for: .touchUpInside)
        self.btnNO.setTitle(NSLocalizedString("NO", comment: ""), for: .normal)
        self.btnNO.addTarget(self, action: #selector(self.noConfrm), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(self.cancelRide), for: .touchUpInside)
        self.vwConfHeader.headerView()
        self.lblConfirMsg.text = NSLocalizedString("Are your sure you want to cancel order?", comment: "")
        self.lblConfirmationCap.text = NSLocalizedString("CONFIRMATION", comment: "")
        
    }
    @IBAction func btnCall(_ sender: Any) {
        if self.phoneNumber != ""
        {
            let aURL = NSURL(string: "telprompt://"+self.phoneNumber)
            if UIApplication.shared.canOpenURL(aURL! as URL) {
                UIApplication.shared.open(aURL! as URL, options: [:], completionHandler: nil)
            } else {
                print("error")
            }
        }
    }
    @IBAction func btnSMS(_ sender: Any) {
        if self.phoneNumber != ""
        {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [self.phoneNumber]
                controller.messageComposeDelegate = self
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @objc private func yesConfrm()
       {
           RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
           tripCancel(tripID: self.trpID, cancelID: "7") { (response) in
                                      guard response.result.isSuccess else {
                                          // self.showAlert(message: response.result.error! as! String)
                                          RappleActivityIndicatorView.stopAnimation()
                                          return
                                      }
                                      if let _JSON = response.result.value
                                      {
                                          RappleActivityIndicatorView.stopAnimation()
                                          let jsonData = _JSON.data(using: String.Encoding.utf8)
                                          do{
                                              if let data = jsonData,
                                                  let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                                              {
                                                  if (json["Message"] as? String) != nil
                                                  {
                                                      self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                                      return
                                                  }
                                                  let error = json["error"] as! Bool
                                                  if !error
                                                  {
                                                      let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                      self.navigationController?.pushViewController(vw, animated: true)
                                                  }
                                                  else
                                                  {
                                                      self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                                  }
                                              }
                                          }catch{}
                                      }
                                  }
       }
       @objc private func noConfrm()
       {
           self.vwCancelConfirmation.isHidden = true
       }
       @objc private func cancelRide()
       {
           self.vwCancelConfirmation.isHidden = false
       }
    func getDataFromFireBase()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let userID = SharedManager.sharedInstance.userData.pID
        self.refFirDB.child("CustomerTrips/"+userID).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            //print(snapshot)
            if let postDist = snapshot.value as? [String:Any]
            {
                if let trpID = postDist["tripID"] as? String
                {
                    self.refFirDB.child("Trips/"+trpID+"/TripStatus").observeSingleEvent(of: DataEventType.value) { (response) in
                        
                        if let status = response.value as? String, status == "On The Way"
                        {
                            self.rideStatus = status
                            self.lblStatus.text = NSLocalizedString("Order is being prepared", comment: "")
                            self.btnCancel.isHidden = false
                        }
                        else
                        {
                            self.rideStatus = "Picked"
                            self.lblStatus.text = NSLocalizedString("Coming...", comment: "")
                            self.btnCancel.isHidden = true
                        }
                    }
                    self.refFirDB.child("Trips/"+trpID+"/"+userID).observeSingleEvent(of: DataEventType.value, with: { (tp) in
                        //print(tp)
                        if let tpData = tp.value as? [String:Any]
                        {
                            let rd = Ride.fromJSON(tpData)
                            self.trpID = rd.tripID
                            self.phoneNumber = rd.driverContactNumber
                            self.syncFireBaseDriverPosition(driverID: rd.driverID)
                            self.dropLat = rd.dropOfflatitude
                            self.dropLon = rd.dropOfflongitude
                            self.imgUser.image = UIImage.init(named: "Sample")
                            if Connectivity.isConnectedToInternet()
                            {
                                // RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
                                Alamofire.request(Constants.Server.profilePic + rd.driverPicture.replacingOccurrences(of: "~", with: "")).responseImage { (response) in
                                    //  RappleActivityIndicatorView.stopAnimation()
                                    if let image = response.result.value {
                                        self.imgUser.image = image
                                    }
                                }
                            }
                            self.lblCustomerName.text = rd.driverName
                            self.lblVN.text = rd.vehicleNumber
                            getAddressFromGeocoding(latitude: Double(rd.dropOfflatitude)!, longitude: Double(rd.dropOfflongitude)!) { (err, address) in
                                self.lblAddress.text = address
                            }
                        }
                    })
                }
                
            }
        }
    }
    func syncFireBaseDriverPosition(driverID:String)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        self.refFirDB.child("OnlineDriver/"+driverID).child("location").observe(DataEventType.value) { (snapshot) in
            if let postDict = snapshot.value as? [String:Any]
            {
                var bearing = 0.0
                self.refFirDB.child("OnlineDriver/"+driverID+"/bearing").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                    bearing = snapshot.value as! Double
                    if let cooradinates = postDict["l"] as? [Any]
                    {
                        let lat = cooradinates[0] as! Double
                        let long = cooradinates[1] as! Double
                        self.updateDriverLocation(lat: Double(lat), long: Double(long), bearing: bearing)
                    }
                })
            }
        }
    }
}
extension DeliveryOnWayViewController:MFMessageComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case MessageComposeResult.cancelled:
            controller.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent:
            controller.dismiss(animated: true, completion: nil)
        default:
            controller.dismiss(animated: true, completion: nil)
        }
        //self.dismiss(animated: true, completion: nil)
    }
    
}
extension DeliveryOnWayViewController:CLLocationManagerDelegate, UIGestureRecognizerDelegate
{
    func updateDriverLocation(lat:Double, long:Double, bearing:Double)
    {
        self.camera =  GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: Float(Constants.Common.normalZoom))
        self.currentLatitude = lat
        self.currentLongitude = long
        
        if !isLocation
        {
            isLocation = true
            self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), camera: camera)
            self.mapView.setMinZoom(Float(Constants.Common.minZoom), maxZoom: Float(Constants.Common.maxZoom))
            self.vwMap.addSubview(mapView)
            //show controlles on map view
            self.vwMap.bringSubviewToFront(self.vwInfo)
            self.vwMap.bringSubviewToFront(self.imgUser)
            self.vwMap.bringSubviewToFront(self.imgCarIcon)
            self.vwMap.bringSubviewToFront(self.btnMyLocation)
            self.vwMap.bringSubviewToFront(self.btnCancel)
            self.vwMap.bringSubviewToFront(self.vwCancelConfirmation)
            self.mapGestures()
            //add pick up pin
            // self.addPickUpPin(pickLat: self.pickUpLat, pickLon: self.pickUpLon)
            //add drop pin
            if self.rideStatus != "On The Way"
            {
                self.addDropOffPin(dropLat: self.dropLat, dropLon: self.dropLon)
            }
        }
        else{
            if isMyLocationUpdated
            {
                self.mapView.animate(to: camera)
            }
            else
            {
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            }
            self.mapView.animate(toBearing: bearing)
        }
    }
    //    func addPickUpPin(pickLat:String, pickLon:String) -> Void {
    //
    //             let mark = GMSMarker()
    //             mark.position = CLLocationCoordinate2D(latitude: Double(pickLat)!, longitude: Double(pickUpLon)!)
    //             mark.icon = UIImage.init(named: "pin_man")
    //             mark.map = self.mapView
    //     }
    //
    func addDropOffPin(dropLat:String, dropLon:String) -> Void {
        let mark = GMSMarker()
        mark.position = CLLocationCoordinate2D(latitude: Double(dropLat)!, longitude: Double(dropLon)!)
        mark.icon = UIImage.init(named: "ic_poly_end_cap")
        mark.map = self.mapView
    }
    func mapGestures()
    {
        let tg = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        tg.numberOfTapsRequired = 2
        tg.delegate = self
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
        swipeLeft.direction = .left
        swipeLeft.delegate = self
        self.mapView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
        swipeRight.direction = .right
        swipeRight.delegate = self
        self.mapView.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
        swipeUp.direction = .up
        swipeUp.delegate = self
        self.mapView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeGest(_:)))
        swipeDown.direction = .down
        swipeDown.delegate = self
        self.mapView.addGestureRecognizer(swipeDown)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchGest(_:)))
        pinch.delegate = self
        self.mapView.addGestureRecognizer(pinch)
        
        self.mapView.addGestureRecognizer(tg)
        self.vwMap.isUserInteractionEnabled = true
        self.vwMap.isMultipleTouchEnabled = true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func pinchGest(_ sender: UIPinchGestureRecognizer) {
        let pg = sender
        if pg.state == UIGestureRecognizer.State.ended
        {
            self.btnMyLocation.isHidden = false
            self.isMyLocationUpdated = false
            self.imgCarIcon.isHidden = true
            self.addPinOnSwipe()
        }
    }
    
    @objc func swipeGest(_ sender: UISwipeGestureRecognizer) {
        let sg = sender
        if sg.state == UIGestureRecognizer.State.ended
        {
            self.btnMyLocation.isHidden = false
            self.isMyLocationUpdated = false
            self.imgCarIcon.isHidden = true
            self.addPinOnSwipe()
        }
    }
    
    @objc func tapGesture(_ gesture: UITapGestureRecognizer)
    {
        self.btnMyLocation.isHidden = false
        self.isMyLocationUpdated = false
        self.imgCarIcon.isHidden = true
        self.addPinOnSwipe()
    }
    
    func addPinOnSwipe()
    {
        //self.isMarkerAdd = true
        marker.position = CLLocationCoordinate2D(latitude: currentLatitude, longitude: currentLongitude)
        marker.icon = UIImage.init(named: "caricon")
        marker.map = mapView
    }
    @objc private func setMyLoc()
    {
        self.isMyLocationUpdated = true
        self.mapView.clear()    //add drop pin
        self.addDropOffPin(dropLat: self.dropLat, dropLon: self.dropLon)
        self.imgCarIcon.isHidden = false
        self.btnMyLocation.isHidden = true
        camera = GMSCameraPosition.camera(withLatitude: self.currentLatitude , longitude: self.currentLongitude, zoom: Float(Constants.Common.normalZoom))
        self.mapView.animate(to: camera)
    }
}
