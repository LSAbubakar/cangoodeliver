//
//  RegisterUserViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 14/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import Planet
import RappleProgressHUD

class RegisterUserViewController: UIViewController {

    @IBOutlet weak var lblRegistrationCap: UILabel!
    @IBOutlet weak var lblFirstname: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var btnSignUpOutlet: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
   // @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    let viewController = CountryPickerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        //coutry code delegate
        self.viewController.delegate = self
        self.setCountry()
        
        //localization
        self.lblRegistrationCap.text = NSLocalizedString("REGISTRATION", comment: "")
        self.lblFirstname.text = NSLocalizedString("FIRST NAME", comment: "")
        self.lblLastName.text = NSLocalizedString("LAST NAME", comment: "")
        self.lblMobileNumber.text = NSLocalizedString("MOBILE NUMBER", comment: "")
//        self.lblEmail.text = NSLocalizedString("EMAIL", comment: "")
        self.lblPassword.text = NSLocalizedString("PASSWORD", comment: "")
        self.lblConfirmPassword.text = NSLocalizedString("CONFIRM PASSWORD", comment: "")
        self.btnSignUpOutlet.setTitle(NSLocalizedString("SIGN UP", comment: ""), for: .normal)
        
        //textfield
        self.txtFirstName.textFieldRoundCorner()
        self.txtLastName.textFieldRoundCorner()
        self.txtPhone.textfieldWithLeftAlignment()
       // self.txtEmail.textFieldRoundCorner()
        self.txtPassword.textFieldRoundCorner()
        self.txtConfirmPassword.textFieldRoundCorner()
        
        //status bar color
        self.view.addSubview(ActionHandler.sharedInstance.setStatusBarColor(color: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)))
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func validate() -> Bool {
        var result = true
        if  self.txtFirstName.text == ""{
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter first name", comment: "")),animated: true,completion: nil)
        }
        else if self.txtLastName.text == "" {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter last name", comment: "")),animated: true,completion: nil)
        }
        else if self.txtPhone.text == "" {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter mobile number", comment: "")),animated: true,completion: nil)
        }
//        else if self.txtEmail.text == "" {
//            result = false
//            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter email address", comment: "")),animated: true,completion: nil)
//        }
        else if self.txtPassword.text == "" {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter password", comment: "")),animated: true,completion: nil)
        }
        else if self.txtConfirmPassword.text == "" {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter confirm password", comment: "")),animated: true,completion: nil)
        }
        else if self.txtPassword.text != self.txtConfirmPassword.text {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Password mistached", comment: "")),animated: true,completion: nil)
        }
//        else if self.txtEmail.text != "" {
//            result = ActionHandler.sharedInstance.isValidEmail(emailText: self.txtEmail.text!)
//            if !result
//            {
//             self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter correct email address", comment: "")),animated: true,completion: nil)
//            }
//        }
        return result
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignUP(_ sender: Any) {
        if validate()
        {
            if !Connectivity.isConnectedToInternet() {
                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
                return
            }
            
            let phn = self.lblCountryCode.text! + self.txtPhone.text!
            RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
            registerPassenger(firstName: self.txtFirstName.text!, lastName: self.txtLastName.text!, phoneNumber: phn, email: "", password: self.txtPassword.text!) { (response) in
                guard response.result.isSuccess else {
                    // self.showAlert(message: response.result.error! as! String)
                    RappleActivityIndicatorView.stopAnimation()
                    return
                }
                if let _JSON = response.result.value
                {
                    RappleActivityIndicatorView.stopAnimation()
                    let jsonData = _JSON.data(using: String.Encoding.utf8)
                    do{
                        if let data = jsonData,
                            let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                        {
                            if (json["Message"] as? String) != nil
                            {
                                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                return
                            }
                            let error = json["error"] as! Bool
                            if !error
                            {
                                if let dt = json["data"] as? [String:Any]
                                {
                                    if let code = dt["pID"] as? String
                                    {
                                        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyUserPhoneViewController") as! VerifyUserPhoneViewController
                                            vw.userID = code
                                        vw.phoneNumber = self.lblCountryCode.text! + self.txtPhone.text!
                                        self.present(ActionHandler.sharedInstance.showAlertPushViewController(message: NSLocalizedString("User registered successfully please verify your account using verification code sent on mobile number", comment: ""), vwParent: self, moveVW:vw), animated: true, completion: nil)
                                    }
                                }
                            }
                            else
                            {
                                if let msg = json["message"] as? String
                                {
                                    if msg.lowercased() == "useralreadyregistered"
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("User already registered", comment: "")), animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                    }
                                    
                                }
                            }
                        }
                    }catch{}
                }
            }
        }
    }
    @IBAction func btnSelectCountryCode(_ sender: Any) {
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
    
}
extension RegisterUserViewController:CountryPickerViewControllerDelegate {
    func countryPickerViewControllerDidCancel(_ countryPickerViewController: CountryPickerViewController) {
        print("countryPickerViewControllerDidCancel: \(countryPickerViewController)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func countryPickerViewController(_ countryPickerViewController: CountryPickerViewController, didSelectCountry country: Country) {
        self.imgCountry.image = country.image
        self.lblCountryCode.text = country.callingCode
        //lblCountryNameCode.text = "\(country.isoCode) \(country.callingCode)"
        print("countryPickerViewController: \(countryPickerViewController) didSelectCountry: \(country)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func setCountry()
    {
        if let code = Locale.current.regionCode
        {
            if let country = Country.find(isoCode: code)
            {
                
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
        else {
            if let country = Country.find(isoCode: "AT")
            {
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
    }
    
}
