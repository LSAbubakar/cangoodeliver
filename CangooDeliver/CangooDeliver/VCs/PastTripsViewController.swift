//
//  PastTripsViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 19/12/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD
import FirebaseDatabase

class PastTripsViewController: ParentViewController {
    
    @IBOutlet weak var tblPastTrip: UITableView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    @IBOutlet weak var vwEmptyMessage: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    var selectedCell = -1
    var offset = 1
    var limit = 10
    var arrPastTrp:[PastTrip] = []
    var refreshController:UIRefreshControl!
    var reachedEndOfItems = false
    var cancelTripID = ""
    var cancelReasons:[CancelReasons] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblPastTrip.delegate = self
        self.tblPastTrip.dataSource = self
        self.tblPastTrip.tableFooterView = UIView()
        self.refreshController = UIRefreshControl()
        self.refreshController.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        self.refreshController.tintColor = Constants.Common.appYellowColor
        self.tblPastTrip.addSubview(self.refreshController)
        self.lblEmptyMessage.text = NSLocalizedString("There is no ride yet", comment: "")
        self.lblHeading.text = NSLocalizedString("Your Ordered", comment: "")
        self.loadTripHistory(isReferesher: false)
    }
    @objc func refreshTable()
    {
        self.offset = 1
        self.limit = 10
        self.loadTripHistory(isReferesher: true)
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func loadTripHistory(isReferesher:Bool)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        getTripHistory(pID: SharedManager.sharedInstance.userData.pID, offset: "\(self.offset)", limit: "\(self.limit)") { (response) in
            guard response.result.isSuccess else {
                self.refreshController.endRefreshing()
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                self.refreshController.endRefreshing()
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                let totalBooking = dt["totalRecords"] as! Int
                                if let trps = dt["tripHistory"] as? [[String:Any]]
                                {
                                    if isReferesher
                                    {
                                        self.arrPastTrp = []
                                    }
                                    for tp in trps
                                    {
                                        self.arrPastTrp.append(PastTrip.fromJSON(tp))
                                    }
                                    if self.arrPastTrp.count > 0
                                    {
                                        self.vwEmptyMessage.isHidden = true
                                        self.offset += self.limit - 1
                                        if self.arrPastTrp.count <= totalBooking
                                        {
                                            self.reachedEndOfItems = false
                                        }
                                        self.tblPastTrip.reloadData()
                                    }
                                    else
                                    {
                                        self.vwEmptyMessage.isHidden = false
                                    }
                                }
                            }
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
}
extension PastTripsViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPastTrp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblPastTrip.dequeueReusableCell(withIdentifier: "PastTripTableViewCell", for: indexPath) as! PastTripTableViewCell
       // cell.contentView.layer
        cell.vwMain.layer.cornerRadius = 10
        cell.vwMain.layer.masksToBounds = true
        let tp = self.arrPastTrp[indexPath.row]
        cell.lblPickUpAddress.text = tp.DropOffLocation == "" ? "N/A" : tp.DropOffLocation
        cell.lblRideFare.text = "€ 15"
        //cell.lblDropOffaddress.text = tp.DropOffLocation == "" ? "N/A" : tp.DropOffLocation
        
       // cell.lblEstimatedTime.text = tp.tripDuration + " min"
        cell.lblTip.text = "€ \(tp.Tip)"
//        if tp.DistanceTraveled != ""
//        {
//            let dis = Double(tp.DistanceTraveled)! / 1000
//            cell.lblEstimatedDistance.text = "\(String(format: "%.1f", dis)) Km"
//        }
//        else{
//            cell.lblEstimatedDistance.text = "N/A"
//        }
        let dtZone = ActionHandler.sharedInstance.addTimeZone(date: tp.PickUpBookingDateTime, intervelInSeconds: TimeZone.current.secondsFromGMT())
        let fr = DateFormatter()
        fr.dateFormat = "dd MMM yyyy"
        let date = fr.string(from: dtZone!)
        cell.lblDate.text = date
        fr.dateFormat = "HH:mm"
        let time = fr.string(from: dtZone!)
        cell.lblTime.text = time
        cell.btnStatus.layer.borderWidth = 1
        if tp.status.lowercased() == "completed"
        {
            cell.btnStatus.setTitle(NSLocalizedString("COMPLETED", comment: ""), for: .normal)
            cell.btnStatus.titleLabel?.minimumScaleFactor = 0.7
            cell.btnStatus.titleLabel?.numberOfLines = 1
            let clr = UIColor.init(red: 83/255, green: 138/255, blue: 46/255, alpha: 1.0)
            cell.btnStatus.layer.borderColor = clr.cgColor
            cell.btnStatus.setTitleColor(clr, for: .normal)
            cell.lblGoodsCost.text = "€ \(String(format: "%.2f",Double(tp.totalFare)! - 15.0))"
        }
        else
        {
            cell.btnStatus.setTitle(NSLocalizedString("CANCELLED", comment: ""), for: .normal)
            cell.btnStatus.layer.borderColor = UIColor.red.cgColor
            cell.btnStatus.setTitleColor(UIColor.red, for: .normal)
            cell.lblGoodsCost.text = "€0"
        }
        if tp.paymentMethod != ""
        {
            if tp.paymentMethod == Constants.Common.cash
            {
                
                cell.imgBGP.image = UIImage.init(named: "cash_pay_bg")
                cell.imgPayment.image = UIImage.init(named: "cash")
            }
            else
            {
                
                cell.imgBGP.image = UIImage.init(named: "mobile_pay_bg")
                cell.imgPayment.image = UIImage.init(named: "mobile")
            }
        }
        if tp.requestType == Constants.tripType.grocery
        {
            cell.imgType.image = UIImage.init(named: "grocery_unselect")
            cell.imgType.image = cell.imgType.image?.withRenderingMode(.alwaysTemplate)
            cell.imgType.tintColor = Constants.Common.appYellowColor
        }
        else
        {
            cell.imgType.image = UIImage.init(named: "medicine_unselect")
            cell.imgType.image = cell.imgType.image?.withRenderingMode(.alwaysTemplate)
            cell.imgType.tintColor = Constants.Common.appYellowColor
        }
        if indexPath.row == self.arrPastTrp.count - 1 {
            self.loadMore()
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCell = indexPath.row
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.selectedCell
        {
           return 240
        }
        return 110
    }
    func loadMore() {
        guard !self.reachedEndOfItems else {
            return
        }
        // get data from api
        self.loadTripHistory(isReferesher: false)
    }
}
