//
//  CashPaymentConfimationViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 21/05/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit

class CashPaymentConfimationViewController: ParentViewController {

    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblWalletAdj: UILabel!
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblDisCountCap: UILabel!
    @IBOutlet weak var lblWalletCap: UILabel!
    @IBOutlet weak var lblTotalCap: UILabel!
    @IBOutlet weak var vwHeader: UIView!
    
    var trp:MobilePay!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwHeader.headerView()
        
        self.lblTotalFare.text = "€ " + trp.collectedAmount.replacingOccurrences(of: ".", with: ",")
        //self.lblWalletAdj.text = "€ " + trp.walletAmountUsed.replacingOccurrences(of: ".", with: ",")
       // self.lblDiscount.text = "€ " + trp.promoDiscountAmount.replacingOccurrences(of: ".", with: ",")
        self.lblWalletAdj.text = "€ \(Double(trp.collectedAmount)! - 15.0)".replacingOccurrences(of: ".", with: ",")
       // self.lblPayableAmount.text = "€ " + trp.totalFare.replacingOccurrences(of: ".", with: ",")
        //localization
        self.lblTotalCap.text = NSLocalizedString("Total Fare", comment: "")
        self.lblWalletCap.text = NSLocalizedString("Goods Cost", comment: "")
        self.lblDisCountCap.text = NSLocalizedString("Ride Fare", comment: "")
        
    }
    @IBAction func btnOk(_ sender: Any) {
       // NotificationCenter.default.post(name: NSNotification.Name("ratingObserver"), object: nil)
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name("isPaymentCash"), object: nil)
    }
}
