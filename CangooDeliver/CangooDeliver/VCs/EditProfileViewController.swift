//
//  EditProfileViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 02/05/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import RappleProgressHUD

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblFIrstName: UILabel!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var btnUpdateOL: UIButton!
    
    let  imagePicker =  UIImagePickerController()
    var alamoFireManager : SessionManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwHeader.headerView()
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.masksToBounds = false
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        
        self.imgUser.image = UIImage.init(named: "Sample")
        if SharedManager.sharedInstance.userData.originalPicture != ""
        {
            let  url = URL(string:Constants.Server.passengerPicUrl+"\(SharedManager.sharedInstance.userData.originalPicture.replacingOccurrences(of: "~", with: ""))")!
            UIImage.loadFrom(url: url) { image in
                if let img = image
                {
                    self.imgUser.image = img
                }
                else
                {
                    self.imgUser.image = UIImage.init(named: "Sample")
                }
            }
        }
        self.imgUser.clipsToBounds = true
        self.imgUser.contentMode = .scaleAspectFill
        self.lblFIrstName.text = NSLocalizedString("First Name", comment: "")
        self.lblLastName.text = NSLocalizedString("Last Name", comment: "")
        self.btnUpdateOL.setTitle(NSLocalizedString("Update", comment: ""), for: .normal)
        
        txtFirstName.textFieldRoundCorner()
        txtLastName.textFieldRoundCorner()
        
        let ud = SharedManager.sharedInstance.userData
        self.txtFirstName.text = ud?.firstName
        self.txtLastName.text = ud?.lastName
    }
    @IBAction func btnClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnUpdate(_ sender: Any) {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        if txtFirstName.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter first name", comment: "")), animated: true, completion: nil)
            return
        }
        else if txtLastName.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter last name", comment: "")), animated: true, completion: nil)
            return
        }
        updateName(pID: SharedManager.sharedInstance.userData.pID, firstName: self.txtFirstName.text!, lastName: self.txtLastName.text!) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            SharedManager.sharedInstance.userData.firstName = self.txtFirstName.text!
                            SharedManager.sharedInstance.userData.lastName = self.txtLastName.text!
                            ActionHandler.sharedInstance.delUpdateUserDefaultsForUserData(uData: SharedManager.sharedInstance.userData)
                            NotificationCenter.default.post(name: NSNotification.Name("profileUpdate"), object: nil)
                            self.present(ActionHandler.sharedInstance.showAlertDismisViewController(message: NSLocalizedString("Successfully updated", comment: ""), vw: self), animated: true, completion: nil)
                            
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    @IBAction func btnEditProfileImage(_ sender: Any) {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: NSLocalizedString("Please select", comment: ""), message:nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: NSLocalizedString("Abort", comment: ""), style: .cancel) { _ in
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: NSLocalizedString("Take From Camera", comment: ""), style: .default)
        { _ in
            //var imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: NSLocalizedString("Pick From Gellary", comment: ""), style: .default)
        { _ in
            //var imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
}
extension EditProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//
//        self.imgUser.contentMode = .scaleToFill //3
//
//        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage //2
//        {
//            self.uploadProfileImage(imgProfile: chosenImage)
//
//        }
//        dismiss(animated:true, completion: nil) //5
//    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.imgUser.contentMode = .scaleToFill //3
        
        if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage //2
        {
            self.uploadProfileImage(imgProfile: chosenImage)
            
        }
        dismiss(animated:true, completion: nil) //5
    }
    func uploadProfileImage(imgProfile:UIImage)
    {
        if !Connectivity.isConnectedToInternet() {
            //showAlert(message: "Please check internet connection.")
            return
        }
         RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let parameter = ["pID":SharedManager.sharedInstance.userData.pID]
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 100
        configuration.timeoutIntervalForResource = 100
        
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager!.upload(multipartFormData: { multipartFormData in
            
            let mugImgData = imgProfile.jpegData(compressionQuality: 0.2)!
            multipartFormData.append(mugImgData, withName: "profileImag.jpg",fileName: "profileImag.jpg", mimeType: "image/jpg")
            
            for (key, value) in parameter {
                multipartFormData.append(((value).data(using: String.Encoding.utf8)!), withName: key)
            }
        },
                                 usingThreshold: UInt64.init(), to: Constants.Server.serverURL+"userProfileImage", method: .post, headers: ["Authorization":"Bearer "+Constants.AuthToken.authToken])
        { (result) in
            print(result)
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //  RappleActivityIndicatorView.stopAnimation()
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseString { response in
                    RappleActivityIndicatorView.stopAnimation()
                    do {
                        print(response)
                        if response.data != nil
                        {
                            let json = try JSONSerialization.jsonObject(with: response.data!) as! [String: Any]
                            let error = json["error"] as! Bool
                            if !error {
                                self.imgUser.image = imgProfile //4
                                NotificationCenter.default.post(name: NSNotification.Name("profileUpdate"), object: nil)
                            }
                        }
                    }catch{}
                }
                break
            case .failure(let encodingError):
                RappleActivityIndicatorView.stopAnimation()
                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                print(encodingError)
            }
        }
    }
}
extension UIImageView {
    public func imageFromURL(urlString: String) {
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}
