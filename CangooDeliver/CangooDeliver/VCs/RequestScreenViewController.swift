//
//  RequestScreenViewController.swift
//  CanTaxi
//
//  Created by Abubakar on 11/09/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//
import GoogleMaps
import UIKit
import ShimmerSwift
import RappleProgressHUD
import Alamofire
import MBCircularProgressBar
import FirebaseDatabase
import AMDots

class RequestScreenViewController: UIViewController {
    
    @IBOutlet weak var vwShimmer: ShimmeringView!
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var btnRejectOutlet: UIButton!
    @IBOutlet weak var lblDistance: UILabel!
    //@IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var vwProgress: MBCircularProgressBarView!
    @IBOutlet weak var vwDots: UIView!
    @IBOutlet weak var lblSearchCaptain: UILabel!
    @IBOutlet weak var lblWait: UILabel!
    @IBOutlet weak var lblWaitMsg: UILabel!
    
    var currentLat = ""
    var currentLong = ""
    var refFireDB:DatabaseReference!
    var isAccepted = false
    var dotsView: AMDots!
    var waiTingTime = ""
    var tripID = ""
    var tmr:Timer!
    var ns:NotificationCenter!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        refFireDB = Database.database().reference()
        self.vwShimmer.contentView = self.imgMap
        self.vwShimmer.shimmerSpeed = 400
        self.vwShimmer.shimmerPauseDuration = 0.2
        self.imgMap.layer.cornerRadius = self.imgMap.frame.size.width / 2
        self.imgMap.clipsToBounds = true
        self.imgMap.layer.borderColor = UIColor.black.cgColor
        self.imgMap.layer.borderWidth = 1
        //localization
        self.btnRejectOutlet.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
        self.lblSearchCaptain.text = NSLocalizedString("SEARCHING RIDER", comment: "")
        self.lblWait.text = NSLocalizedString("PLEASE WAIT", comment: "")
        self.lblWaitMsg.text = NSLocalizedString("Please wait finding your best match", comment: "")
        ns = NotificationCenter.default
        self.ns.addObserver(self,
                            selector: #selector(self.timerInvalidate),
                            name: Notification.Name("timerInvalidate"),
                            object: nil)
        self.btnRejectOutlet.setTitleColor(Constants.Common.appYellowColor, for: .normal)
        self.btnRejectOutlet.layer.borderColor = Constants.Common.appYellowColor.cgColor
        self.btnRejectOutlet.layer.borderWidth = 2
        self.vwShimmer.isShimmering = true
        //dot loader
        let fst = Constants.Common.appYellowColor
        dotsView = AMDots(frame: CGRect(x: 35, y: 10, width: 100, height: 40), colors: [fst,fst,fst])
        self.vwDots.addSubview(dotsView)
        dotsView.animationType = .scale
        self.vwProgress.countdown = true
        self.vwProgress.progressCapType = 1
        
        //load image from static google map
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap?markers=icon:http://demoapi.agiletechstudio.net/Images/icman.png|\(self.currentLat),\(self.currentLong)&zoom=15&size=240x240&maptype=roadmap&key=\(Constants.Server.googleAPIKey)"
        let url = NSURL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        //get image from google
        if  let data = try? Data(contentsOf: url! as URL)
        {
           //Thread.sleep(forTimeInterval: 2)
            self.vwShimmer.isShimmering = false
             self.imgMap.image = UIImage(data: data)
            let interval = Double(self.waiTingTime)
//            DispatchQueue.main.asyncAfter(deadline: .now() + interval!) {
//                if !Constants.Common.isBookingAccepted
//                {
//                self.timoutDriverSearch(isCancel: false)
//                }
//            }
            tmr = Timer.scheduledTimer(withTimeInterval: interval!, repeats: false, block: { (tm) in
                if !Constants.Common.isBookingAccepted
                                {
                                self.timoutDriverSearch(isCancel: false)
                                }
            })
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let interval = Double(self.waiTingTime)
        UIView.animate(withDuration: interval!, animations: {
            self.vwProgress.value = 100.0
        })
    }
    @objc private func timerInvalidate()
    {
        self.tmr.invalidate()
    }
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func btnReject(_ sender: Any) {
        self.tmr.invalidate()
        self.timoutDriverSearch(isCancel: true)
    }
    func timoutDriverSearch(isCancel:Bool)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        rideRequestTimeOut(tripID: self.tripID, pID: SharedManager.sharedInstance.userData.pID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if isCancel
                            {
                              self.navigationController?.popViewController(animated: true)
                            }
                            else
                            {
                                self.present(ActionHandler.sharedInstance.showAlertPopViewController(message: NSLocalizedString("Captain not found, Please try again later", comment: ""), vw: self), animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            if let msg = json["message"] as? String
                            {
                                if msg == "tripAlreadyBooked"
                                {
                                    
                                }
                                else
                                {
                                    
                                }
                            }
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
}
