//
//  PurchasedViewController.swift
//  CangooDeliver
//
//  Created by Abubakar on 21/03/2020.
//  Copyright © 2020 LS. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import RappleProgressHUD
import MessageUI
import Firebase


class PurchasedViewController: UIViewController {

    @IBOutlet weak var imgCart: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblVN: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var vwConfHeader: UIView!
    @IBOutlet weak var lblConfirMsg: UILabel!
    @IBOutlet weak var btnNO: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var vwCancelConfirmation: UIView!
    @IBOutlet weak var lblConfirmationCap: UILabel!
    
    var refFirDB:DatabaseReference!
    var phoneNumber = ""
    var trpID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refFirDB =  Database.database().reference()
        self.getDataFromFireBase()
        self.vwConfHeader.headerView()
        self.lblConfirMsg.text = NSLocalizedString("Are your sure you want to cancel order?", comment: "")
        self.lblConfirmationCap.text = NSLocalizedString("CONFIRMATION", comment: "")
        self.lblVehicleNumber.text = NSLocalizedString("Vehicle Number", comment: "")
        self.lblStatus.text = NSLocalizedString("Order is being prepared", comment: "")
        self.btnCancel.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
        self.btnYes.setTitle(NSLocalizedString("YES", comment: ""), for: .normal)
        self.btnYes.addTarget(self, action: #selector(self.yesConfrm), for: .touchUpInside)
        self.btnNO.setTitle(NSLocalizedString("NO", comment: ""), for: .normal)
        self.btnNO.addTarget(self, action: #selector(self.noConfrm), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(self.cancelRide), for: .touchUpInside)
        self.imgCart.image = UIImage.gifImageWithName("cart")
    }
    @IBAction func btnCall(_ sender: Any) {
        if self.phoneNumber != ""
        {
            let aURL = NSURL(string: "telprompt://"+self.phoneNumber)
            if UIApplication.shared.canOpenURL(aURL! as URL) {
                UIApplication.shared.open(aURL! as URL, options: [:], completionHandler: nil)
            } else {
                print("error")
            }
        }
    }
    @IBAction func btnSMS(_ sender: Any) {
        if self.phoneNumber != ""
        {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [self.phoneNumber]
                controller.messageComposeDelegate = self
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func yesConfrm()
    {
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        tripCancel(tripID: self.trpID, cancelID: "7") { (response) in
                                   guard response.result.isSuccess else {
                                       // self.showAlert(message: response.result.error! as! String)
                                       RappleActivityIndicatorView.stopAnimation()
                                       return
                                   }
                                   if let _JSON = response.result.value
                                   {
                                       RappleActivityIndicatorView.stopAnimation()
                                       let jsonData = _JSON.data(using: String.Encoding.utf8)
                                       do{
                                           if let data = jsonData,
                                               let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                                           {
                                               if (json["Message"] as? String) != nil
                                               {
                                                   self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                                   return
                                               }
                                               let error = json["error"] as! Bool
                                               if !error
                                               {
                                                   let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                   self.navigationController?.pushViewController(vw, animated: true)
                                               }
                                               else
                                               {
                                                   self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                               }
                                           }
                                       }catch{}
                                   }
                               }
    }
    @objc private func noConfrm()
    {
        self.vwCancelConfirmation.isHidden = true
    }
    @objc private func cancelRide()
    {
        self.vwCancelConfirmation.isHidden = false
    }
}
extension PurchasedViewController:MFMessageComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case MessageComposeResult.cancelled:
            controller.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent:
            controller.dismiss(animated: true, completion: nil)
        default:
            controller.dismiss(animated: true, completion: nil)
        }
        //self.dismiss(animated: true, completion: nil)
    }
   func getDataFromFireBase()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let userID = SharedManager.sharedInstance.userData.pID
        self.refFirDB.child("CustomerTrips/"+userID).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            //print(snapshot)
            if let postDist = snapshot.value as? [String:Any]
            {
                if let trpID = postDist["tripID"] as? String
                {
                    self.refFirDB.child("Trips/"+trpID+"/"+userID).observeSingleEvent(of: DataEventType.value, with: { (tp) in
                        //print(tp)
                        if let tpData = tp.value as? [String:Any]
                        {
                            let rd = Ride.fromJSON(tpData)
                            self.phoneNumber = rd.driverContactNumber
                            self.trpID = rd.tripID
                            self.imgUser.image = UIImage.init(named: "Sample")
                            if Connectivity.isConnectedToInternet()
                            {
                                // RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
                                Alamofire.request(Constants.Server.profilePic + rd.driverPicture.replacingOccurrences(of: "~", with: "")).responseImage { (response) in
                                    //  RappleActivityIndicatorView.stopAnimation()
                                    if let image = response.result.value {
                                        self.imgUser.image = image
                                    }
                                }
                            }
                            self.lblCustomerName.text = rd.driverName
                            self.lblVN.text = rd.vehicleNumber
                            getAddressFromGeocoding(latitude: Double(rd.dropOfflatitude)!, longitude: Double(rd.dropOfflongitude)!) { (err, address) in
                                self.lblAddress.text = address
                            }
                        }
                    })
                }
                
            }
        }
    }
}
