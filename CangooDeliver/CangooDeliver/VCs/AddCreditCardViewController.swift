//
//  AddCreditCardViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 21/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import LSCreditCardForm
import Stripe
import RappleProgressHUD

protocol AddCreditCardDelegate {
    func CcAddedSucc(stripToken:String, customerID:String)
}

class AddCreditCardViewController: ParentViewController,LSCreditCardFormDelegate {

    @IBOutlet weak var lblCreditCardCap: UILabel!
    @IBOutlet weak var vwCrd: LSCreditCardFormView!
    
    var delegateCardToke:AddCreditCardDelegate!
    var customerID = ""
    var cardType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblCreditCardCap.text = NSLocalizedString("Add Credit Card", comment: "")
        self.vwCrd.delegate = self
       LSCreditCardFormConfig.CreditCard.backgroundColor = UIColor.lightText
        //vwCrd.txt
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
   @objc func keyboardWillShow(notification: Notification) {
    if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func didCompleteForm(creditCard: LSCreditCard) {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let cardParams = STPCardParams()
        cardParams.number = creditCard.number
        let dt = creditCard.expiration.split(separator: "/")
        var year = ""
        var month = ""
        if dt.count == 2
        {
            month = String(dt[0])
            year = String(dt[1])
        }
        cardParams.expMonth = UInt(month)!
        cardParams.expYear = UInt(year)!
        cardParams.cvc = creditCard.cvv
        //cardParams.name = creditCard.cardHolderName
        cardParams.currency = "eur"
        let address = STPAddress()
        address.line1 = self.cardType
        cardParams.address = address
        cardParams.name = creditCard.cardHolderName
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                RappleActivityIndicatorView.stopAnimation()
                                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Invalid card details. Please re-enter correct details", comment: "")), animated: true, completion: nil)
                return
            }
            RappleActivityIndicatorView.stopAnimation()
            self.delegateCardToke.CcAddedSucc(stripToken: String(token.tokenId), customerID: self.customerID)
            self.dismiss(animated: true, completion: nil)
        }
    }
    func getCustomCardType(for number: String) -> LSCreditCardType? {
        return number.hasPrefix("") ? .custom("") : nil
    }

}
