//
//  MenuViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 22/12/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Alamofire
import Firebase

class MenuViewController: ParentViewController {

    @IBOutlet weak var btnTripCap: UIButton!
    @IBOutlet weak var btnWalletCap: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnLogOutCap: UIButton!
    @IBOutlet weak var vwLogOutBorder: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var isFromDashBoard = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.masksToBounds = false
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
       // self.imgUser.clipsToBounds = true
        
        if isFromDashBoard
        {
            self.btnLogOutCap.isHidden = false
            self.vwLogOutBorder.isHidden = false
        }
        else
        {
            self.btnLogOutCap.isHidden = true
            self.vwLogOutBorder.isHidden = true
        }
        //localization
        self.btnTripCap.setTitle(NSLocalizedString("Your Trips", comment: ""), for: .normal)
        self.btnWalletCap.setTitle(NSLocalizedString("Payment methods", comment: ""), for: .normal)
        self.btnChangePassword.setTitle(NSLocalizedString("Change Password", comment: ""), for: .normal)
        self.btnLogOutCap.setTitle(NSLocalizedString("Logout", comment: ""), for: .normal)
        self.loadData()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.loadData),
                                       name: Notification.Name("profileUpdate"),
                                       object: nil)
    }
    
    @objc func loadData()
    {
        self.lblUserName.text = SharedManager.sharedInstance.userData.firstName + " " + SharedManager.sharedInstance.userData.lastName
        self.imgUser.image = UIImage.init(named: "Sample")
        if SharedManager.sharedInstance.userData.originalPicture != ""
        {
            let  url = URL(string:Constants.Server.passengerPicUrl+"\(SharedManager.sharedInstance.userData.originalPicture.replacingOccurrences(of: "~", with: ""))")!
            UIImage.loadFrom(url: url) { image in
                if let img = image
                {
                    self.imgUser.image = img
                }
                else
                {
                    self.imgUser.image = UIImage.init(named: "Sample")
                }
            }
            self.imgUser.clipsToBounds = true
            self.imgUser.contentMode = .scaleAspectFill
        }
    }
    @IBAction func btnBack(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTrips(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PastTripsViewController") as! PastTripsViewController
        vw.modalPresentationStyle = .fullScreen
        self.present(vw, animated: true, completion: nil)
    }
    
    @IBAction func btnWallet(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        vw.modalPresentationStyle = .fullScreen
        self.present(vw, animated: true, completion: nil)
    }
    @IBAction func btnChangePassword(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        vw.modalPresentationStyle = .fullScreen
        self.present(vw, animated: true, completion: nil)
    }
    @IBAction func btnLogOut(_ sender: Any) {
        Constants.Common.isOnline = false
        UserDefaults.standard.removeObject(forKey: "LoginUserData")
        self.removeNodeFB()
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vw, animated: true)
    }
    @IBAction func btnProfile(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vw.modalPresentationStyle = .fullScreen
        self.present(vw, animated: true, completion: nil)
    }
    private func removeNodeFB()
    {
        let uData = SharedManager.sharedInstance.userData
        let refDB = Database.database().reference()
        refDB.child("OnlineCustomers").child(uData!.pID).removeValue()
    }
}
