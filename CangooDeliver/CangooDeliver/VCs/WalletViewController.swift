//
//  WalletViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 20/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import FirebaseDatabase
import RappleProgressHUD

class WalletViewController: UIViewController {
    
    @IBOutlet weak var btnWalletAdjustOutlet: UIButton!
    @IBOutlet weak var lblWalletBalanceCap: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblAdjustWallet: UILabel!
    @IBOutlet weak var lblSelectPaymentCap: UILabel!
    @IBOutlet weak var lblPaypalCap: UILabel!
    @IBOutlet weak var lblCashCap: UILabel!
    @IBOutlet weak var btnPaypalOutlet: UIButton!
    @IBOutlet weak var btnCashOutlet: UIButton!
    @IBOutlet weak var btnCreditCardOutlet: UIButton!
    @IBOutlet weak var lblCreditCard: UILabel!
    
    @IBOutlet weak var btnUpdatePaymentMethod: UIButton!
    @IBOutlet weak var vwScroll: UIScrollView!
    @IBOutlet weak var lblCreditCardNo: UILabel!
    
    
    var unSelectPaymentColor = UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1.0)
    var selectPaymentColor = UIColor.black
    var selectedPaymentMethod = ""
    var isWalletPreffer = false
    var defaultCard = ""
    var customerID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblBalance.text = "€0,0"
        self.vwScroll.contentSize = CGSize(width: self.view.frame.width, height: 600)
        self.getDetail()
        
        //localization
        self.lblWalletBalanceCap.text = NSLocalizedString("Wallet Balance", comment: "")
        self.lblAdjustWallet.text = NSLocalizedString("Adjust wallet balance on priority payment.", comment: "")
        self.lblSelectPaymentCap.text = NSLocalizedString("SELECT PAYMENT MODE", comment: "")
        self.lblPaypalCap.text = NSLocalizedString("PayPal", comment: "")
        self.lblCashCap.text = NSLocalizedString("Cash", comment: "")
        self.lblCreditCard.text = NSLocalizedString("Credit Card", comment: "")
        self.lblCreditCardNo.text = NSLocalizedString("****", comment: "")
        self.btnUpdatePaymentMethod.setTitle(NSLocalizedString("UPDATE PAYMENT METHOD", comment: ""), for: .normal)
        self.btnWalletAdjustOutlet.addTarget(self, action: #selector(self.adjustWallet), for: .touchUpInside)
    }
    @IBAction func btnPaypal(_ sender: Any) {
        self.selectPaymentMethod(paymentMethod: "PayPal")
    }
    @IBAction func btnCash(_ sender: Any) {
        self.selectPaymentMethod(paymentMethod: "Cash")
    }
    @IBAction func btnSetCreditCardPaymentMethod(_ sender: Any) {
        if self.defaultCard == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please add credit card.", comment: "")), animated: true, completion: nil)
            return
        }
        self.selectPaymentMethod(paymentMethod: "CreditCard")
    }
    
    @IBAction func btnCreditCard(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreditCardListViewController") as! CreditCardListViewController
        vw.delegate = self
        self.present(vw, animated: true, completion: nil)
        self.selectPaymentMethod(paymentMethod: "Credit Card")
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnInvition(_ sender: Any) {
//        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
//        vw.modalPresentationStyle = .fullScreen
//        self.present(vw, animated: true, completion: nil)
    }
    
    @IBAction func btnAddPromoCode(_ sender: Any) {
//        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddPromoCodeViewController") as! AddPromoCodeViewController
//        vw.promoDelegate = self
//        vw.modalPresentationStyle = .overCurrentContext
//        self.present(vw, animated: true, completion: nil)
    }
    @objc func adjustWallet()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
         RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        walletPreffer(pID: SharedManager.sharedInstance.userData.pID, isWallet: !self.isWalletPreffer) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                           if self.isWalletPreffer
                           {
                            self.isWalletPreffer = false
                            self.btnWalletAdjustOutlet.setBackgroundImage(UIImage.init(named: "check-box-empty"), for: .normal)
                            }
                            else
                           {
                            self.isWalletPreffer = true
                            self.btnWalletAdjustOutlet.setBackgroundImage(UIImage.init(named: "check-box"), for: .normal)
                            }
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    @IBAction func btnUpdatePayment(_ sender: Any) {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        updatePaymentMethod(pID: SharedManager.sharedInstance.userData.pID, selectedPaymentMethod:self.selectedPaymentMethod) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            SharedManager.sharedInstance.userData.selectedPaymentMethod = self.selectedPaymentMethod
                            ActionHandler.sharedInstance.delUpdateUserDefaultsForUserData(uData: SharedManager.sharedInstance.userData)
                             self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Payment method updated successfully.", comment: "")), animated: true, completion: nil)
                        }
                        else
                        {
                             self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    @IBAction func btnDelPromoCode(_ sender: Any) {
        //self.delPromoCode()
    }
    func selectPaymentMethod(paymentMethod:String)
    {
        if paymentMethod.lowercased() == "cash"
        {
            self.selectedPaymentMethod = "Cash"
            self.btnCashOutlet.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.lblCashCap.textColor = self.selectPaymentColor
            self.btnPaypalOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblPaypalCap.textColor = self.unSelectPaymentColor
            self.btnCreditCardOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblCreditCard.textColor = self.unSelectPaymentColor
            self.lblCreditCardNo.textColor = self.unSelectPaymentColor
        }
        else if paymentMethod.lowercased() == "paypal"
        {
            self.selectedPaymentMethod = "PayPal"
            self.btnCashOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblCashCap.textColor = self.unSelectPaymentColor
            self.btnPaypalOutlet.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.lblPaypalCap.textColor = self.selectPaymentColor
            self.btnCreditCardOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblCreditCard.textColor = self.unSelectPaymentColor
            self.lblCreditCardNo.textColor = self.unSelectPaymentColor
        }
        else if paymentMethod.lowercased() == "creditcard"
        {
            self.selectedPaymentMethod = "CreditCard"
            self.btnCashOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblCashCap.textColor = self.unSelectPaymentColor
            self.btnCreditCardOutlet.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.lblCreditCard.textColor = self.selectPaymentColor
            self.lblCreditCardNo.textColor = self.selectPaymentColor
            self.btnPaypalOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.lblPaypalCap.textColor = self.unSelectPaymentColor
        }
    }
    func getDetail()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let uID = SharedManager.sharedInstance.userData.pID
        getWallet(pID: uID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                if let wlt = dt["Wallet"] as? [String:Any]
                                {
                                    let isWalletPreferred = wlt["isWalletPreferred"] as! Bool
                                    if isWalletPreferred
                                    {
                                        self.btnWalletAdjustOutlet.setBackgroundImage(UIImage.init(named: "check-box"), for: .normal)
                                        self.isWalletPreffer = true
                                    }
                                    else
                                    {
                                        
                                        self.btnWalletAdjustOutlet.setBackgroundImage(UIImage.init(named: "check-box-empty"), for: .normal)
                                        self.isWalletPreffer = false
                                    }
                                    if let preferredPaymentMethod = wlt["preferredPaymentMethod"] as? String
                                    {
                                        self.selectPaymentMethod(paymentMethod: preferredPaymentMethod)
                                    }
                                    let walletBalance = wlt["walletBalance"] as! Double
                                    self.lblBalance.text = "€\(walletBalance)".replacingOccurrences(of: ".", with: ",")
                                    
                                    if let crd = wlt["creditCardDetails"] as? [String:Any]
                                    {
                                        if let cusID = crd["customerId"] as? String
                                            {
                                                self.customerID = cusID
                                        }
                                        if let defauldCard = crd["defaultSourceId"] as? String
                                        {
                                            
                                            self.defaultCard = defauldCard
                                            if let crdLst = crd["cardsList"] as? [[String:Any]]
                                            {
                                                for card in crdLst
                                                {
                                                    let cr = CreditCard.fromJSON(card)
                                                    if self.defaultCard == cr.cardId
                                                    {
                                                        self.lblCreditCard.text = cr.cardHolderName
                                                        self.lblCreditCardNo.text = "************"+cr.last4Digits
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if self.selectedPaymentMethod == "CreditCard"
                                            {
                                               self.lblCreditCard.text = NSLocalizedString("Credit Card", comment: "")
                                                self.lblCreditCardNo.text = NSLocalizedString("****", comment: "")
                                               // self.selectPaymentMethod(paymentMethod: "Cash")
                                             self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("No credit card added in your wallet, add credit card OR change payment method", comment: "")), animated: true, completion: nil)
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
}

extension WalletViewController:CreditCardListDelegate
{
    func backFrmCC() {
        self.getDetail()
    }
    func defaultCard(crd: CreditCard) {
        self.defaultCard = crd.cardId
        self.lblCreditCard.text = crd.cardHolderName
        self.lblCreditCardNo.text = "************"+crd.last4Digits
    }
}

