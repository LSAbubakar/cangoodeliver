//
//  ShowImageViewController.swift
//  CangooDeliver
//
//  Created by Abubakar on 28/03/2020.
//  Copyright © 2020 LS. All rights reserved.
//

import UIKit

protocol AttachImageDelete {
    func delImage()
}

class ShowImageViewController: UIViewController {

    @IBOutlet weak var imgAttachment: UIImageView!
    
    var attachemt:UIImage!
    var delegateImg:AttachImageDelete!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if attachemt != nil
        {
            self.imgAttachment.image = attachemt
        }
    }
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDelete(_ sender: Any) {
        self.delegateImg.delImage()
         self.dismiss(animated: true, completion: nil)
    }
    


}
