//
//  ViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 17/09/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import UIKit
import Planet
import RappleProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var lblSingInCap: UILabel!
    @IBOutlet weak var lblMobileNumberCap: UILabel!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblPasswordCap: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnForgetPasswordOutlet: UIButton!
    @IBOutlet weak var btnRegisterOutlet: UIButton!
    @IBOutlet weak var btnSignInOutlet: UIButton!
    @IBOutlet weak var btnRememberOL: UIButton!
    @IBOutlet weak var lblRememberPassword: UILabel!
    
    
    let viewController = CountryPickerViewController()
    var isRemember = false
    var tmr:Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //coutry code delegate
        self.viewController.delegate = self
        self.setCountry()
        
        //set localization
        self.lblMobileNumberCap.text = NSLocalizedString("MOBILE NUMBER", comment: "")
        self.lblPasswordCap.text = NSLocalizedString("PASSWORD", comment: "")
        self.lblSingInCap.text = NSLocalizedString("SIGN IN", comment: "")
        self.btnSignInOutlet.setTitle(NSLocalizedString("SIGN IN", comment: ""), for: .normal)
        self.btnForgetPasswordOutlet.setTitle(NSLocalizedString("Forgot Password", comment: ""), for: .normal)
        self.btnRegisterOutlet.setTitle(NSLocalizedString("Register New User?", comment: ""), for: .normal)
        self.lblRememberPassword.text = NSLocalizedString("Remember Password", comment: "")
        
        //set textfield
        self.txtPhone.textfieldWithLeftAlignment()
        self.txtPassword.textFieldRoundCorner()
        
        //status bar color
        self.view.addSubview(ActionHandler.sharedInstance.setStatusBarColor(color: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)))
        
        
        self.btnRememberOL.addTarget(self, action: #selector(btnIsRemember), for: .touchUpInside)
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        self.tmr = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkToken), userInfo: nil, repeats: true)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        if UserDefaults.standard.bool(forKey: "isRemember")
        {
            self.isRemember = true
            self.btnRememberOL.setImage(UIImage.init(named: "check-box"), for: .normal)
            if let phone = UserDefaults.standard.object(forKey: "userPhone") as? String
            {
                self.txtPhone.text = phone
            }
            if let password = UserDefaults.standard.object(forKey: "userPassword") as? String
            {
                self.txtPassword.text = password
            }
        }
        else
        {
            self.isRemember = false
            self.btnRememberOL.setImage(UIImage.init(named: "check-box-empty"), for: .normal)
        }
    }
    
    @objc func btnIsRemember()
    {
        if UserDefaults.standard.bool(forKey: "isRemember")
        {
            self.isRemember = false
            UserDefaults.standard.set(false, forKey: "isRemember")
            self.btnRememberOL.setImage(UIImage.init(named: "check-box-empty"), for: .normal)
        }
        else
        {
            self.isRemember = true
            UserDefaults.standard.set(true, forKey: "isRemember")
            self.btnRememberOL.setImage(UIImage.init(named: "check-box"), for: .normal)
        }
    }
        @objc private func checkToken()
        {
            //Constants.DeviceToken.deviceToken = ""
            if Constants.DeviceToken.deviceToken != ""
            {
                RappleActivityIndicatorView.stopAnimation()
                self.tmr.invalidate()
            }
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnForgetPassword(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as!
        ForgotPasswordViewController
        self.navigationController?.pushViewController(vw, animated: true)
    }
    @IBAction func btnRegisterUser(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterUserViewController") as!
        RegisterUserViewController
        self.navigationController?.pushViewController(vw, animated: true)
    }
    @IBAction func btnSigIn(_ sender: Any) {
        if self.txtPhone.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter phone number", comment: "")), animated: true, completion: nil)
            return
        }
        if self.txtPassword.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter password", comment: "")), animated: true, completion: nil)
            
            return
        }
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Check your internet connection", comment: "")), animated: true, completion: nil)
            return
        }
        let phone = self.lblCountryCode.text! + txtPhone.text!
        
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        loginPassenger(phoneNumber: phone, password: self.txtPassword.text!, deviceToken: Constants.DeviceToken.deviceToken) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let uData = json["data"] as? [String:Any]
                            {
                                if UserDefaults.standard.bool(forKey: "isRemember")
                                {
                                    UserDefaults.standard.set(self.txtPhone.text, forKey: "userPhone")
                                    UserDefaults.standard.set(self.txtPassword.text, forKey: "userPassword")
                                }
                                
                                let isVerified = uData["isVerified"] as! Bool
                                if !isVerified {
                                    if let pid = uData["pid"] as? String
                                    {
                                        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyUserPhoneViewController") as! VerifyUserPhoneViewController
                                        vw.userID = pid
                                        vw.phoneNumber = self.lblCountryCode.text! + self.txtPhone.text!
                                        self.navigationController?.pushViewController(vw, animated: true)
                                        return
                                    }
                                    
                                }
                                var resID = ""
                                var resArea = ""
                                var isCreditCardAdded = ""
                                if let rID = uData["resellerID"] as? String
                                {
                                    resID = rID
                                    resArea = uData["resellerAuthorizeArea"] as! String
                                }
                                if let iscredit = uData["isCreditCardAdded"] as? Bool
                                {
                                    isCreditCardAdded = iscredit ? "true" : "false"
                                }
                                if resID != ""
                                {
                                    let psgr = LoginUser.fromJSON(uData["Passenger"] as! [String : Any])
                                    psgr.resellerID = resID
                                    psgr.resellerAuthorizeArea = resArea
                                    Constants.Common.isOnline = true
                                    Constants.AuthToken.authToken = psgr.access_Token
                                    psgr.isCedirCardAdded = isCreditCardAdded
                                    SharedManager.sharedInstance.userData = psgr
                                    ActionHandler.sharedInstance.delUpdateUserDefaultsForUserData(uData: psgr)
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as!
                                    DashboardViewController
                                    self.navigationController?.pushViewController(vw, animated: true)
                                }
                            }
                        }
                        else
                        {
                            if let msg = json["message"] as? String
                            {
                                
                                 if msg.lowercased() == "userblocked"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Account temporarily blocked", comment: "")), animated: true, completion: nil)
                                }
                                else if msg.lowercased() == "usernotfound"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to fetch user details. Please try again later", comment: "")), animated: true, completion: nil)
                                }
                                    else if msg.lowercased() == "authenticationfailed"
                                 {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Invalid phone number or password", comment: "")), animated: true, completion: nil)
                                 }
                                else
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }catch{}
            }
        }
    }
    @IBAction func btnSelectCountryCode(_ sender: Any) {
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
    
}
extension ViewController:CountryPickerViewControllerDelegate {
    func countryPickerViewControllerDidCancel(_ countryPickerViewController: CountryPickerViewController) {
        print("countryPickerViewControllerDidCancel: \(countryPickerViewController)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func countryPickerViewController(_ countryPickerViewController: CountryPickerViewController, didSelectCountry country: Country) {
        self.imgCountry.image = country.image
        self.lblCountryCode.text = country.callingCode
        //lblCountryNameCode.text = "\(country.isoCode) \(country.callingCode)"
        print("countryPickerViewController: \(countryPickerViewController) didSelectCountry: \(country)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func setCountry()
    {
        if let code = Locale.current.regionCode
        {
            if let country = Country.find(isoCode: code)
            {
                
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
        else {
            if let country = Country.find(isoCode: "AT")
            {
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
    }
    
}
