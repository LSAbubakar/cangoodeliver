//
//  VerifyUserPhoneViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 14/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD


class VerifyUserPhoneViewController: UIViewController {

    @IBOutlet weak var lblMobileVerificationCap: UILabel!
    @IBOutlet weak var lblMobileCodeCap: UILabel!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var btnVerifyOutlet: UIButton!
    
    
    var userID = ""
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
      //localization
        self.lblMobileVerificationCap.text = NSLocalizedString("MOBILE VERIFICATION", comment: "")
        self.lblMobileCodeCap.text = NSLocalizedString("MOBILE VERIFICATION CODE", comment: "")
        self.btnVerifyOutlet.setTitle("VERIFY", for: .normal)
        
        //TEXT FILED
        self.txtCode.textFieldRoundCorner()
        
        //status bar color
        self.view.addSubview(ActionHandler.sharedInstance.setStatusBarColor(color: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)))
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResendCode(_ sender: Any) {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        resendCode(pID: self.userID, phoneNumber: self.phoneNumber) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Verification code sent successfully to you registered phone number", comment: "")), animated: true, completion: nil)
                        }
                        else
                        {
                         self.present(ActionHandler.sharedInstance.showAlert(message: "Something wrong! Please try again later"), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    
    @IBAction func btnVerify(_ sender: Any) {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        if self.txtCode.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: "Please enter verification code"), animated: true, completion: nil)
            return
        }
        
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        phoneVerification(verificationCode: self.txtCode.text!, pID: self.userID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            
                            let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(ActionHandler.sharedInstance.showAlertPushViewController(message: NSLocalizedString("Successfully verified", comment: ""), vwParent: self, moveVW: vw), animated: true, completion: nil)
                            self.navigationController?.pushViewController(vw, animated: true)
                        }
                        else {
                            if let msg = json["message"] as? String
                            {
                                if msg.lowercased() == "useralreadyverified"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: "Your mobile number already verified"), animated: true, completion: nil)
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    self.navigationController?.pushViewController(vw, animated: true)
                                    
                                }
                                else if msg.lowercased() == "usernotverified"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: "Incorrect verification code"), animated: true, completion: nil)
                                }
                                else {
                                 self.present(ActionHandler.sharedInstance.showAlert(message: "Something wrong! Please try again later"), animated: true, completion: nil)
                                }
                            }
                            
                        }
                    }
                }catch{}
            }
        }
    }
}
