//
//  MobilePayConfirmationViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 21/05/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit

protocol MobilePayDelegate {
    func payment(payable:String)
}

class MobilePayConfirmationViewController: UIViewController {

    @IBOutlet weak var vwHeader: UIView!
    
    @IBOutlet weak var lblGoodsCostCap: UILabel!
    @IBOutlet weak var lblGoodsCost: UILabel!
    @IBOutlet weak var lblRideFareCap: UILabel!
    @IBOutlet weak var lblRideFare: UILabel!
    @IBOutlet weak var lblTipCap: UILabel!
    @IBOutlet weak var lblTip: UILabel!
    @IBOutlet weak var lblTotalCap: UILabel!
    
    @IBOutlet weak var lblTotalFare: UILabel!
    
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    var delegate:MobilePayDelegate!
    //var mp:MobilePay!
   var goodsCost = ""
    var rideFare = ""
    var totalFare = ""
    var tip = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwHeader.headerView()
        //self.mp.totalFare = //self.isWalkIn ? self.mp.estimatedFare : self.mp.totalFare
        self.lblTotalFare.text = "€ \(self.totalFare)".replacingOccurrences(of: ".", with: ",")
        //var fareWithoutTip = 0.0
        
        self.lblTip.text = "€ \(self.tip)".replacingOccurrences(of: ".", with: ",")
        self.lblGoodsCost.text = "€ \(self.goodsCost)".replacingOccurrences(of: ".", with: ",")
        //localization
        self.lblTotalCap.text = NSLocalizedString("Total Amount", comment: "")
        self.lblGoodsCostCap.text = NSLocalizedString("Goods Cost", comment: "")
        self.lblRideFareCap.text = NSLocalizedString("Ride Fare", comment: "")
        self.lblTipCap.text = NSLocalizedString("Tip", comment: "")
        self.btnPay.setTitle(NSLocalizedString("PAY", comment: ""), for: .normal)
        self.btnCancel.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
        self.btnPay.addTarget(self, action: #selector(self.pay), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(self.cancel), for: .touchUpInside)
    }
    
    @objc func pay()
    {
        self.dismiss(animated: true, completion: nil)
        self.delegate.payment(payable: self.totalFare)
        
    }
    @objc func cancel()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
