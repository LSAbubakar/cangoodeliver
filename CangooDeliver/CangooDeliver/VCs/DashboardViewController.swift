//
//  DashboardViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 13/10/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FloatingPanel
import RappleProgressHUD
import Alamofire
import AMDots

class DashboardViewController: UIViewController,CLLocationManagerDelegate {
    //    struct Item: Decodable {
    //        // let copyright, date, explanation: String
    //        // let hdurl: String
    //        // let mediaType, serviceVersion, title: String
    //        let url: URL
    //    }
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnUserImageOutlet: UIButton!
    @IBOutlet weak var vwMap: UIView!
    @IBOutlet weak var vwRoot: UIView!
    @IBOutlet weak var vwButtons: UIView!
    @IBOutlet weak var btnMyLocation: UIButton!
    @IBOutlet weak var btnBookNowOL: UIButton!
    @IBOutlet weak var btnBookLaterOL: UIButton!
    //@IBOutlet weak var btnSDOL: UIButton!
    @IBOutlet weak var imgDot: UIImageView!
    @IBOutlet weak var constrainImgDot: NSLayoutConstraint!
    @IBOutlet weak var imgLn: UIImageView!
    @IBOutlet weak var vwPickupAddress: UIView!
    @IBOutlet weak var lblAddPicCap: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var vwDotLoader: UIView!
    @IBOutlet weak var btnSkipDestination: UIButton!
    // @IBOutlet weak var vwDestination: UIView!
    
    //variable
    var refFirDB:DatabaseReference!
    let locationManager = CLLocationManager()
    var mapView:GMSMapView!
    let marker = GMSMarker()
    var distinationLat = 0.0
    var distinationLong = 0.0
    var selectedCurrentLat = 0.0
    var selectedCurrentLong = 0.0
    var camera:GMSCameraPosition!
    var isLocation = false
    var isMyLocationUpdated = false
    var distance = ""
    var isSetDestination = false
    var points = ""
    var duration = ""
    var isRideCancelledByRider = false
    var fpc: FloatingPanelController!
    var isPaymentPending = false
    var dicOnlineCap = [String:String]()
    var timr = Timer()
    var dotsView: AMDots!
    var isMapMove = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        //let crntWdth = self.view.frame.width
        //let crntHght = self.view.frame.height
        
        var diff = 0
        if UIDevice.modelName == "iPhone X" || UIDevice.modelName == "iPhone X" || UIDevice.modelName == "iPhone XS" || UIDevice.modelName == "iPhone XS Max" || UIDevice.modelName == "iPhone XR"
        {
            diff = 5
        }
        else
        {
            diff = 10
        }
        self.constrainImgDot.constant = self.constrainImgDot.constant + CGFloat(diff)
        // self.vwDestination.dropShadow(radius: 2)
        //loader
        self.vwPickupAddress.layer.borderColor = Constants.Common.appYellowColor.cgColor
        self.vwPickupAddress.layer.borderWidth = 1
        let fst = Constants.Common.appYellowColor
        dotsView = AMDots(frame: CGRect(x: 65, y: 20, width: 50, height: 20), colors: [fst,fst,fst])
        self.vwDotLoader.addSubview(dotsView)
        self.isMapMove = true
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        //localization
        //        self.txtCurrentLoc.text = NSLocalizedString("Current Location", comment: "")
        //        self.txtDistinationLoc.placeholder = NSLocalizedString("Select Destination", comment: "")
        //        self.txtNoOfPassenger.placeholder = NSLocalizedString("No of Passengers", comment: "")
        self.btnBookNowOL.setTitle(NSLocalizedString("BOOK NOW", comment: ""), for: .normal)
        self.btnBookLaterOL.setTitle(NSLocalizedString("BOOK LATER", comment: ""), for: .normal)
        //self.btnSDOL.setTitle(NSLocalizedString("Select Destination", comment: ""), for: .normal)
        self.lblAddPicCap.text = NSLocalizedString("Pickup Address", comment: "")
        self.btnSkipDestination.setTitle(NSLocalizedString("Place an order", comment: ""), for: .normal)
        //user profile image
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.masksToBounds = false
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        self.imgUser.clipsToBounds = true
        self.uploadProfile()
        
        //location update
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        ActionHandler.sharedInstance.locationUpdate()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.locationUpdationOnMap(_:)),
                                       name: Notification.Name("locationUpdation"),
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(self.uploadProfile),
                                       name: Notification.Name("profileUpdate"),
                                       object: nil)
        
        self.refFirDB = Database.database().reference()
        //ride status
        self.checkRideStatus()
        
        self.camera =  GMSCameraPosition.camera(withLatitude: Double(Constants.Currentlocation.latitude)!, longitude: Double(Constants.Currentlocation.longitude)!, zoom: 15.0)
        self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), camera: camera)
        self.mapView.delegate = self
        self.vwMap.addSubview(mapView)
        self.vwMap.bringSubviewToFront(self.btnUserImageOutlet)
        self.vwMap.bringSubviewToFront(self.imgUser)
        self.vwMap.bringSubviewToFront(self.vwRoot)
        self.vwMap.bringSubviewToFront(self.vwButtons)
        self.vwMap.bringSubviewToFront(self.btnMyLocation)
        self.vwMap.bringSubviewToFront(self.imgDot)
        self.vwMap.bringSubviewToFront(self.imgLn)
        self.vwMap.bringSubviewToFront(self.vwPickupAddress)
        self.vwMap.bringSubviewToFront(self.vwDotLoader)
        self.vwMap.bringSubviewToFront(self.btnSkipDestination)
        self.mapView.isMyLocationEnabled = true
        //check token auth
        //self.tokenValidation()
        
    }
    @objc func uploadProfile()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Check your internet connection", comment: "")), animated: true, completion: nil)
            return
        }
        //self.imgUser.af_setImage(withURL: URL(string:"\(Constants.Server.passengerPicUrl)Images/User/\(SharedManager.sharedInstance.userData.originalPicture)")!)
        self.imgUser.image = UIImage.init(named: "Sample")
        if SharedManager.sharedInstance.userData.originalPicture != ""
        {
            let url = URL(string: Constants.Server.passengerPicUrl+"\(SharedManager.sharedInstance.userData.originalPicture.replacingOccurrences(of: "~", with: ""))")!
            UIImage.loadFrom(url: url) { image in
                if let img = image
                {
                    self.imgUser.image = img
                }
                else
                {
                    self.imgUser.image = UIImage.init(named: "Sample")
                }
            }
        }
        self.imgUser.contentMode = .scaleAspectFill
        self.imgUser.clipsToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        if !CLLocationManager.locationServicesEnabled()
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Location service is disable. Please enable to access your current location", comment: "")), animated: true, completion: nil)
            return
        }
        if isRideCancelledByRider
        {
            self.isRideCancelledByRider = false
            let  vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
            vw.isNotify = true
            vw.isRating = false
            vw.modalPresentationStyle = .overCurrentContext
            vw.message = NSLocalizedString("Captain canceled the ride", comment: "")
            self.present(vw, animated: true, completion: nil)
        }
        if self.isPaymentPending
        {
            self.isPaymentPending = false
            RappleActivityIndicatorView.startAnimatingWithLabel(NSLocalizedString("Please wait calculating fare...", comment: ""), attributes: Constants.rappleAttribute.attributesDash)
        }
    }
    @IBAction func btnDestinationLoc(_ sender: Any) {
        self.openAddressPicker(isCurrent: false)
    }
    @IBAction func btnSkipDestLoc(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
        vw.deliveryAddress = self.lblAddress.text!
        vw.deliveryLatitude = "\(self.selectedCurrentLat)"
        vw.deliveryLogitude = "\(self.selectedCurrentLong)"
        self.navigationController?.pushViewController(vw, animated: false)
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navMenu")
        vw.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vw, animated: true, completion: nil)
    }
    @IBAction func btnCurrentAddress(_ sender: Any) {
        self.openAddressPicker(isCurrent: true)
    }
    
    func openAddressPicker(isCurrent:Bool)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressPickerFromMapViewController") as! AddressPickerFromMapViewController
        //vw.delegateAddress = self
        // vw.modalPresentationStyle = .fullScreen
        vw.isCurrent = isCurrent
        
        vw.currentLatitude = self.selectedCurrentLat == 0.0 ? Constants.Currentlocation.latitude : "\(self.selectedCurrentLat)"
        vw.currentLogitude = self.selectedCurrentLong == 0.0 ? Constants.Currentlocation.longitude : "\(self.selectedCurrentLong)"
        vw.isFromDashboard = true
        self.navigationController?.pushViewController(vw, animated: true)
    }
    
    @IBAction func btnBookNow(_ sender: Any) {
        if fpc != nil
        {
            fpc.removePanelFromParent(animated: true)
        }
        self.openBookView(isLaterBooking: false)
        
    }
    @IBAction func btnBookLater(_ sender: Any) {
        if fpc != nil
        {
            fpc.removePanelFromParent(animated: true)
        }
        self.openBookView(isLaterBooking: true)
    }
    func openBookView(isLaterBooking:Bool)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        
        //UserDefaults.standard.set(Int(self.txtNoOfPassenger.text!), forKey: "NOOFPassengers")
        //        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookNowViewController") as! BookNowViewController
        //        var currentLat = Constants.Currentlocation.latitude
        //        var currentLon = Constants.Currentlocation.longitude
        //        if self.selectedCurrentLat != 0.0 && self.selectedCurrentLong != 0.0
        //        {
        //            currentLat = "\(self.selectedCurrentLat)"
        //            currentLon = "\(self.selectedCurrentLong)"
        //        }
        //        vw.currentLat =  currentLat
        //        vw.currentLon = currentLon
        //        vw.destinationLat = "\(self.distinationLat)"
        //        vw.destinationLon = "\(self.distinationLong)"
        //        vw.estimatedDistance = "\(self.distance)"
        //        vw.estimatedTime = self.duration
        //        //vw.seatingRequired = self.txtNoOfPassenger.text!
        //        vw.isLaterBooking = isLaterBooking
        //        vw.delegate = self
        //        self.navigationController?.pushViewController(vw, animated: true)
    }
}
extension DashboardViewController:GMSMapViewDelegate
{
    @IBAction func btnMyLocation(_ sender: Any) {
        self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(Constants.Currentlocation.latitude)!, longitude: Double(Constants.Currentlocation.longitude)!))
        self.isMyLocationUpdated = true
        self.btnMyLocation.isHidden = true
        self.isMapMove = true
    }
    @objc func locationUpdationOnMap(_ notification: NSNotification)  {
        if let loc = notification.userInfo as NSDictionary? {
            let lat = loc["Latitude"] as! Double
            let long = loc["Logitude"] as! Double
            
            Constants.Currentlocation.latitude = "\(lat)"
            Constants.Currentlocation.longitude = "\(long)"
            
            self.camera =  GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
            //
            if !isLocation
            {
                isLocation = true
                self.mapView.animate(to: self.camera)
                //swipe function
                let tg = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
                tg.numberOfTapsRequired = 2
                tg.delegate = self
                let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
                swipeLeft.direction = .left
                swipeLeft.delegate = self
                self.mapView.addGestureRecognizer(swipeLeft)
                
                let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
                swipeRight.direction = .right
                swipeRight.delegate = self
                self.mapView.addGestureRecognizer(swipeRight)
                
                let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGest(_:)))
                swipeUp.direction = .up
                swipeUp.delegate = self
                self.mapView.addGestureRecognizer(swipeUp)
                
                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeGest(_:)))
                swipeDown.direction = .down
                swipeDown.delegate = self
                self.mapView.addGestureRecognizer(swipeDown)
                
                self.mapView.addGestureRecognizer(tg)
                self.vwMap.isUserInteractionEnabled = true
                self.vwMap.isMultipleTouchEnabled = true
                //current address
                //getAddressFromGeocoding(latitude: lat, longitude: long) { (err, address) in
                //     self.txtCurrentLoc.text = address
                //}
                // Creates a marker in the center of the map.
                //                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                //                marker.icon = UIImage.init(named: "location_pin_2")
                //                marker.map = self.mapView
            }
            //            else{
            //                if self.isMyLocationUpdated
            //                {
            //                    //update camera
            //                    self.mapView.animate(to: camera)
            //                }
            //                else
            //                {
            //                    //update marker position only
            //
            //                    //marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //                }
            //            }
        }
        
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
extension DashboardViewController:UIGestureRecognizerDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let cooradint = mapView.camera.target
        if self.isMapMove
        {
            self.vwPickupAddress.isHidden = false
            self.imgLn.isHidden = false
            self.lblAddress.text = ""
            self.vwDotLoader.isHidden = false
            self.timr = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(stopLoading), userInfo: cooradint, repeats: false)
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    @IBAction func pinchGest(_ sender: Any) {
        let pg = sender as! UIPinchGestureRecognizer
        if pg.state == UIGestureRecognizer.State.ended
        {
            self.btnMyLocation.isHidden = false
            self.isMyLocationUpdated = false
            self.vwPickupAddress.isHidden = true
            self.imgLn.isHidden = true
            self.isMapMove = true
        }
    }
    @objc func swipeGest(_ sender: UISwipeGestureRecognizer) {
        let sg = sender
        if sg.state == UIGestureRecognizer.State.ended
        {
            self.btnMyLocation.isHidden = false
            self.isMyLocationUpdated = false
            self.vwPickupAddress.isHidden = true
            self.imgLn.isHidden = true
            self.isMapMove = true
        }
    }
    @objc func tapGesture(_ gesture: UITapGestureRecognizer)
    {
        self.btnMyLocation.isHidden = false
        self.isMyLocationUpdated = false
        self.vwPickupAddress.isHidden = true
        self.imgLn.isHidden = true
        self.isMapMove = true
    }
    
    @objc func stopLoading(timer:Timer)
    {
        let uf = timer.userInfo as! CLLocationCoordinate2D
        self.selectedCurrentLat = uf.latitude
        self.selectedCurrentLong = uf.longitude
        getLocationFromCooredinates(latitude: uf.latitude, longitude: uf.longitude)
    }
    
    func getLocationFromCooredinates(latitude:Double,longitude:Double)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        getAddressFromGeocoding(latitude: latitude, longitude: longitude) { (err, address) in
            self.isMapMove = false
            self.lblAddress.text = address
            self.vwDotLoader.isHidden = true
        }
    }
}
extension DashboardViewController:AddressPickerFromMapDelegate
{
    func getAddress(address: String, latitude: String, longitude: String, iscurrent: Bool) {
        
        if iscurrent
        {
            self.selectedCurrentLat = Double(latitude)!
            self.selectedCurrentLong = Double(longitude)!
            self.camera =  GMSCameraPosition.camera(withLatitude: self.selectedCurrentLat, longitude: self.selectedCurrentLong, zoom: 15.0)
            print("address selected by picker")
            self.mapView.animate(to: self.camera)
            self.btnMyLocation.isHidden = false
            getLocationFromCooredinates(latitude: self.selectedCurrentLat, longitude: self.selectedCurrentLong)
        }
    }
    
    func cancelSelection(oldLat: String, OldLon: String, iscurrent: Bool) {
        if iscurrent
        {
            
        }
        else {
            //self.distinationLat = Double(oldLat)!
            //self.distinationLong = Double(OldLon)!
            //            if self.distinationLat != 0.0 && self.distinationLong != 0.0
            //            {
            //                getAddressFromGeocoding(latitude: self.distinationLat, longitude: self.distinationLong) { (err, address) in
            //                    if !err
            //                    {
            //                        self.txtDistinationLoc.text = address
            //                    }
            //                }
            //            }
        }
    }
}
extension DashboardViewController //manage ride status
{
    func checkRideStatus()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        //self.refFirDB =  Database.database().reference()
        self.refFirDB.child("CustomerTrips/"+SharedManager.sharedInstance.userData.pID).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            // print(snapshot.value)
            if let postDict = snapshot.value as? [String:String]
            {
                self.refFirDB.child("Trips/"+postDict["tripID"]!+"/TripStatus").observeSingleEvent(of: DataEventType.value, with: { (tpStatus) in
                    if let status  = tpStatus.value as? String
                    {
                        if status == "Request Sent"
                        {
                            self.refFirDB.child("Trips/"+postDict["tripID"]!).observeSingleEvent(of: DataEventType.value, with: { (trpDetail) in
                                if let tp = trpDetail.value as? [String:Any]
                                {
                                    //if tp[""]
                                    //let mobilePay = MobilePay.fromJSON(tp)
                                    if let tpDetail = tp[SharedManager.sharedInstance.userData.pID] as? [String:Any]
                                    {
                                        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestScreenViewController") as! RequestScreenViewController
                                        vw.modalPresentationStyle = .fullScreen
                                        if let waitingTime = tpDetail["requestTimeOut"] as? Int
                                        {
                                            vw.waiTingTime = "\(waitingTime)"
                                        }
                                        if let lat = tpDetail["pickUplatitude"] as? String
                                        {
                                            vw.currentLat = lat
                                        }
                                        if let lon = tpDetail["pickUplongitude"] as? String
                                        {
                                            vw.currentLong = lon
                                        }
                                        if let tpID = postDict["tripID"]
                                        {
                                            vw.tripID = tpID
                                        }
                                        //vw.isLaterBooking = isLaterBooking
                                        self.present(vw, animated: true, completion: nil)
                                    }
                                }
                            })
                        }
                            
                        else if status == "On The Way" || status == "Waiting" || status == "Picked" //after purched
                        {
                            self.OnRide(status: status)
                        }
                            
                        else if status == "Payment Pending"
                        {
                            RappleActivityIndicatorView.startAnimatingWithLabel(NSLocalizedString("Please wait calculating fare...", comment: ""), attributes: Constants.rappleAttribute.attributesDash)
                            //                            self.refFirDB.child("Trips/"+postDict["tripID"]!+"/\(SharedManager.sharedInstance.userData.pID)").observeSingleEvent(of: DataEventType.value, with: { (trpDetail) in
                            //                                if let tp = trpDetail.value as? [String:Any]
                            //                                {
                            //                                    let mobilPay = MobilePay.fromJSON(tp)
                            //                                    if mobilPay.PaymentMode != "Cash"
                            //                                    {
                            //                                        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FarePaymentViewController") as! FarePaymentViewController
                            //                                        vw.isWalkIn = false
                            //                                        vw.mobilePay = mobilPay
                            //                                        vw.paymentMethod = mobilPay.paymentMode
                            //                                        self.present(vw, animated: true, completion: nil)
                            //                                    }
                            //
                            //                                }
                            //                            })
                        }
                    }
                })
            }
        }
    }
    func OnRide(status:String)
    {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeliveryOnWayViewController") as! DeliveryOnWayViewController
        self.navigationController?.pushViewController(vw, animated: true)
    }
}
extension DashboardViewController:FloatingPanelControllerDelegate
{
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return layoutOfFloatingPointOnDashboard()
    }
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        print("test")
        
    }
    func floatingPanelDidEndDraggingToRemove(_ vc: FloatingPanelController, withVelocity velocity: CGPoint) {
        print("did move")
    }
    func floatingPanelDidEndDragging(_ vc: FloatingPanelController, withVelocity velocity: CGPoint, targetPosition: FloatingPanelPosition) {
        if targetPosition != .full {
            self.fpc.removePanelFromParent(animated: true)
        }
    }
    
}
class layoutOfFloatingPointOnDashboard: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .full
    }
    
    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .full: return 200// A top inset from safe area
        case .half: return 15.0 // A bottom inset from the safe area
        case .tip: return 5.0 // A bottom inset from the safe area
        default: return nil // Or `case .hidden: return nil`
        }
    }
}
