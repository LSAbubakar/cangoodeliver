//
//  ProfileViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 02/05/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Alamofire

class ProfileViewController: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhoneCap: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.imgUser.layer.cornerRadius = self.imgUser.frame.width / 2
        //self.imgUser.clipsToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.masksToBounds = false
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
       // self.imgUser.clipsToBounds = true
       self.lblPhoneCap.text = NSLocalizedString("Phone", comment: "")
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.uploadProfile),
                                       name: Notification.Name("profileUpdate"),
                                       object: nil)
    }
    @objc func uploadProfile()
    {
        self.imgUser.image = UIImage.init(named: "Sample")
        if SharedManager.sharedInstance.userData.originalPicture != ""
        {
            let  url = URL(string:Constants.Server.passengerPicUrl+"\(SharedManager.sharedInstance.userData.originalPicture.replacingOccurrences(of: "~", with: ""))")!
            UIImage.loadFrom(url: url) { image in
                if let img = image
                {
                    self.imgUser.image = img
                }
                else
                {
                    self.imgUser.image = UIImage.init(named: "Sample")
                }
            }
        }
        self.imgUser.clipsToBounds = true
        self.imgUser.contentMode = .scaleAspectFill
        self.lblUserName.text = SharedManager.sharedInstance.userData.firstName + " " + SharedManager.sharedInstance.userData.lastName
    }
    override func viewWillAppear(_ animated: Bool) {
       self.uploadProfile()
        let ud = SharedManager.sharedInstance.userData
        self.lblPhone.text = ud?.phoneNumber
    }
    @IBAction func btnEditProfile(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        vw.modalPresentationStyle = .overCurrentContext
        self.present(vw, animated: true, completion: nil)
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
