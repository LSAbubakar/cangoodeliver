//
//  AddressPickerViewController.swift
//  CanTaxi
//
//  Created by Abubakar on 18/03/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import AMDots

protocol AddressPickerFromMapDelegate {
    func getAddress(address:String, latitude:String, longitude:String,iscurrent:Bool)
    func cancelSelection(oldLat:String,OldLon:String, iscurrent:Bool)
}

class AddressPickerFromMapViewController: ParentViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var vwAddress: UIView!
    @IBOutlet weak var vwMap: UIView!
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var vwDotLoader: UIView!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    
    let locationManager = CLLocationManager()
    var mapView:GMSMapView!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var currentLatitude = ""
    var currentLogitude = ""
    var selectedLatitude = ""
    var selectedLongitude = ""
    var timr = Timer()
    var dotsView: AMDots!
    let geocoder = CLGeocoder()
    var delegateAddress:AddressPickerFromMapDelegate?
    var isCurrent = false
    var isFromDashboard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let fst = Constants.Common.appYellowColor
        dotsView = AMDots(frame: CGRect(x: 50, y: 20, width: 150, height: 40), colors: [fst,fst,fst,fst])
        self.vwDotLoader.addSubview(dotsView)
        //dotsView.backgroundColor = UIColor.white
        dotsView.animationType = .scale
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 40, width: 350.0, height: 40))
        
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        self.vwMap.bringSubviewToFront(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        //map
        //self.currentLatitude = Constants.Currentlocation.latitude
        //self.currentLogitude = Constants.Currentlocation.longitude
        let lat = Double(Constants.Currentlocation.latitude)
        let long = Double(Constants.Currentlocation.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: long!, zoom: 15.0)
        self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), camera: camera)
        self.vwMap.addSubview(mapView)
        self.vwMap.bringSubviewToFront(self.vwAddress)
        self.vwMap.bringSubviewToFront(self.imgPin)
        self.vwMap.bringSubviewToFront(self.btnCancelOutlet)
        self.vwMap.bringSubviewToFront(self.vwDotLoader)
        self.mapView.animate(toZoom: 15.0)
        self.mapView.delegate = self
        self.btnCancelOutlet.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
    }
    @IBAction func btnSelectAddress(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
        vw.deliveryLogitude = self.selectedLongitude
        vw.deliveryLatitude = self.selectedLatitude
        vw.deliveryAddress = self.lblAddress.text!
        self.navigationController?.pushViewController(vw, animated: true)
    }
    @IBAction func btnCancel(_ sender: Any) {
        if self.isFromDashboard
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.delegateAddress?.cancelSelection(oldLat: self.currentLatitude, OldLon: self.currentLogitude, iscurrent: self.isCurrent)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
extension AddressPickerFromMapViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        self.mapView.animate(to: GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0))
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension AddressPickerFromMapViewController:GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idle")
        print("position:\(mapView.camera.target)")
        let cooradint = mapView.camera.target
        
        self.timr = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(stopLoading), userInfo: cooradint, repeats: false)
        
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.timr.invalidate()
        self.lblAddress.text = ""
        self.vwDotLoader.isHidden = false
        self.dotsView.start()
    }
    @objc func stopLoading(timer:Timer)
    {
        let uf = timer.userInfo as! CLLocationCoordinate2D
        getLocationFromCooredinates(latitude: uf.latitude, longitude: uf.longitude)
    }
    func getLocationFromCooredinates(latitude:Double,longitude:Double)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        getAddressFromGeocoding(latitude: latitude, longitude: longitude) { (err, address) in
            self.lblAddress.text = address
            self.selectedLatitude = "\(latitude)"
            self.selectedLongitude = "\(longitude)"
            self.dotsView.stop()
            self.vwDotLoader.isHidden = true
        }
    }
}
