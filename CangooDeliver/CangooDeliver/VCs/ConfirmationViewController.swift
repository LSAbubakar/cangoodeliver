

import UIKit
protocol ConfirmDelegate {
    func yes(delContentID:String)
    //func no()
}
class ConfirmationViewController: UIViewController {

    @IBOutlet weak var lblConfirmation: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnYesOutlet: UIButton!
    @IBOutlet weak var btnNoOutlet: UIButton!
    @IBOutlet weak var vwOption: UIView!
    @IBOutlet weak var vwNotify: UIView!
    @IBOutlet weak var vwNotifyHeader: UIView!
    @IBOutlet weak var vwOptionHeader: UIView!
    @IBOutlet weak var lblNotifyMsg: UILabel!
    @IBOutlet weak var btnOPOutlet: UIButton!
    @IBOutlet weak var lblConfirmationNotify: UILabel!
    
    var delegateConfirm:ConfirmDelegate!
    var isNotify = false
    var message = ""
    var delContentID = ""
    var isRating = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwOptionHeader.headerView()
        self.vwNotifyHeader.headerView()
        if isNotify
        {
            vwNotify.isHidden = false
            vwOption.isHidden = true
            self.lblNotifyMsg.text = NSLocalizedString(self.message, comment: "")
        }
        else
        {
            vwNotify.isHidden = true
            vwOption.isHidden = false
            self.lblMessage.text = NSLocalizedString(self.message, comment: "")
        }
        
        //nslocalization
        self.lblConfirmation.text = NSLocalizedString("CONFIRMATION", comment: "")
        self.lblConfirmationNotify.text = NSLocalizedString("CONFIRMATION", comment: "")
        self.btnYesOutlet.setTitle(NSLocalizedString("YES", comment: ""), for: .normal)
        self.btnNoOutlet.setTitle(NSLocalizedString("NO", comment: ""), for: .normal)
        self.btnOPOutlet.setTitle(NSLocalizedString("OK", comment: ""), for: .normal)
    }
    @IBAction func btnYes(_ sender: Any) {
        self.delegateConfirm.yes(delContentID: self.delContentID)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNo(_ sender: Any) {
        //self.delegateConfirm.no()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnOk(_ sender: Any) {
        if isRating
        {
            NotificationCenter.default.post(name: NSNotification.Name("ratingObserver"), object: nil)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
   
}
