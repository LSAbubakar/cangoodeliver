//
//  ForgotPasswordViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 14/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import Planet
import RappleProgressHUD


class ForgotPasswordViewController: ParentViewController {

    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnForgotOutlet: UIButton!
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblMobileNumberCap: UILabel!
    
    let viewController = CountryPickerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        //localization
        self.lblForgotPassword.text = NSLocalizedString("FORGOT PASSWORD", comment: "")
        self.lblMobileNumberCap.text = NSLocalizedString("MOBILE NUMBER", comment: "")
        self.btnForgotOutlet.setTitle(NSLocalizedString("FORGOT", comment: ""), for: .normal)
        
        //coutry code delegate
        self.viewController.delegate = self
        self.setCountry()
        
        //status bar color
        self.view.addSubview(ActionHandler.sharedInstance.setStatusBarColor(color: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)))
        
        //textfield
        self.txtPhone.textfieldWithLeftAlignment()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnSelectCountryCode(_ sender: Any) {
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnForgotPassword(_ sender: Any) {
        if txtPhone.text == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter mobile number", comment: "")), animated: true, completion: nil)
            return
        }
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let phn = self.lblCountryCode.text! + self.txtPhone.text!
        forgetPassword(phoneNumber: phn) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            self.present(ActionHandler.sharedInstance.showAlertPopViewController(message: NSLocalizedString("Your new password sent to your register mobile number", comment: ""), vw: self), animated: true, completion: nil)
                        }
                        else {
                            if let msg = json["message"] as? String
                            {
                                if msg.lowercased() == "usernotfound" || msg.lowercased() == "authenticationfailed"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Enter valid mobile number", comment: "")), animated: true, completion: nil)
                                }
                                else {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }catch{}
            }
        }
    }
}
extension ForgotPasswordViewController:CountryPickerViewControllerDelegate {
    func countryPickerViewControllerDidCancel(_ countryPickerViewController: CountryPickerViewController) {
        print("countryPickerViewControllerDidCancel: \(countryPickerViewController)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func countryPickerViewController(_ countryPickerViewController: CountryPickerViewController, didSelectCountry country: Country) {
        self.imgCountry.image = country.image
        self.lblCountryCode.text = country.callingCode
        //lblCountryNameCode.text = "\(country.isoCode) \(country.callingCode)"
        print("countryPickerViewController: \(countryPickerViewController) didSelectCountry: \(country)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func setCountry()
    {
        if let code = Locale.current.regionCode
        {
            if let country = Country.find(isoCode: code)
            {
                
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
        else {
            if let country = Country.find(isoCode: "AT")
            {
                self.imgCountry.image = country.image
                self.lblCountryCode.text = country.callingCode
            }
        }
    }
    
}
