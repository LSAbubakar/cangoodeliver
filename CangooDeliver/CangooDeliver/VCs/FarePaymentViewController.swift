//
//  FarePaymentViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 22/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD

protocol CCSelectionDelegate {
    func CCSelected(arrCC:[CreditCard], defaultCard:String)
}

var environment:String = PayPalEnvironmentProduction {
    willSet(newEnvironment) {
        if (newEnvironment != environment) {
            PayPalMobile.preconnect(withEnvironment: newEnvironment)
        }
    }
}
class FarePaymentViewController: UIViewController {
    
    @IBOutlet weak var lblFareCap: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet weak var lblTimeCap: UILabel!
    @IBOutlet weak var lblDistanceCap: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTipCap: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var btnPayOutlet: UIButton!
    //vw percentage
    @IBOutlet weak var btn5PrOutlet: UIButton!
    @IBOutlet weak var btn10PrOT: UIButton!
    @IBOutlet weak var btn15PrOT: UIButton!
    @IBOutlet weak var btn20prOT: UIButton!
    //vw fixAmout
    @IBOutlet weak var lblFixAmountCap: UILabel!
    @IBOutlet weak var btn1EU: UIButton!
    @IBOutlet weak var btn2EU: UIButton!
    @IBOutlet weak var btn3EU: UIButton!
    @IBOutlet weak var btn4EU: UIButton!
    //credit card
    @IBOutlet weak var vwCreditCard: UIView!
    @IBOutlet weak var vwCardHolderName: UILabel!
    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var lblCardMessage: UILabel!
    
//    @IBOutlet weak var vwHeader: UIView!
//    @IBOutlet weak var vwConfirmation: UIView!
//    @IBOutlet weak var lblMessage: UILabel!
//    @IBOutlet weak var lblConfirmation: UILabel!
    @IBOutlet weak var btnPayTopConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var vwPaypalSelection: UIView!
    @IBOutlet weak var vwCreditCardSelection: UIView!
    
    
    var arrCreditCard:[CreditCard] = []
    var customerID = ""
    var defaultCardID = ""
    var paymentMethod = ""
    var totalAmount = 0.0
    var tip = "0"
    var isWalkIn = false
    var mobilePay:MobilePay!
    var promoDiscount = 0.0
    var walletAmountUsed = 0.0
    var payable = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //back swipe viewcontroller block
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        //
        self.loadData()
        self.selectionPaymentView()
        //
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.setPayment(notification:)),
                                       name: Notification.Name("fareManagement"),
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(self.paymentIsCash),
                                       name: Notification.Name("isPaymentCash"),
                                       object: nil)
        
        //fix
        self.btn1EU.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn2EU.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn3EU.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn4EU.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        //percentage
        self.btn5PrOutlet.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn10PrOT.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn15PrOT.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        self.btn20prOT.addTarget(self, action: #selector(self.addTipe(btn:)), for: .touchUpInside)
        
        self.vwPaypalSelection.cornerRadius(radius: 5)
        self.vwCreditCardSelection.cornerRadius(radius: 5)
        
        //localization
        self.lblFareCap.text = NSLocalizedString("Fare", comment: "")
        self.lblTipCap.text = NSLocalizedString("TIP", comment: "")
        self.lblPercentage.text = NSLocalizedString("Percentage", comment: "")
        self.lblFixAmountCap.text = NSLocalizedString("Fix Amount", comment: "")
        self.lblTimeCap.text = NSLocalizedString("Time:", comment: "")
        self.lblDistanceCap.text = NSLocalizedString("Distance:", comment: "")
        self.lblCardMessage.text = NSLocalizedString("Click here to change card", comment: "")
        self.btnPayOutlet.setTitle(NSLocalizedString("PAY", comment: ""), for: .normal)
    }
    @objc func paymentIsCash()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func addTipe(btn:UIButton)
    {
        switch btn.tag {
        case 1:
            self.btn1EU.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 1, isPercent: false)
        case 2:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 2, isPercent: false)
        case 3:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 3, isPercent: false)
        case 4:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 4, isPercent: false)
        case 5:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 5, isPercent: true)
        case 10:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 10, isPercent: true)
        case 15:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.setTotalPayment(tip: 15, isPercent: true)
        case 20:
            self.btn1EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn2EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn3EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn4EU.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn5PrOutlet.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn10PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn15PrOT.setImage(UIImage.init(named: "radio empty"), for: .normal)
            self.btn20prOT.setImage(UIImage.init(named: "radio tick"), for: .normal)
            self.setTotalPayment(tip: 20, isPercent: true)
        default:
            print(btn.tag)
        }
    }
    func setTotalPayment(tip:Int, isPercent:Bool)
    {
        
        let amount = self.totalAmount - Double(self.tip)!
        if isPercent
        {
            let per = (Double(self.mobilePay.totalFare)! * Double(tip)) / 100
            self.tip = "\(per)"
            self.totalAmount = amount + per
            self.lblFare.text = "€"+"\(String(format: "%.2f",totalAmount))".replacingOccurrences(of: ".", with: ",")
        }
        else
        {
            self.tip = "\(tip)"
            self.totalAmount = amount + Double(tip)
            self.lblFare.text = "€"+"\(String(format: "%.2f",totalAmount))".replacingOccurrences(of: ".", with: ",")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    @objc func setPayment(notification:NSNotification)
    {
        if let loc = notification.userInfo as NSDictionary? {
            self.paymentMethod = loc["paymentMethod"] as! String
            self.mobilePay = (loc["mobilePay"] as! MobilePay)
        }
        loadData()
    }
    func loadData()
    {
        //let dur = Double(self.mobilePay.duration)
        //self.lblTime.text = "\(String(format: "%.2f", dur!)) min"
        let des = Double(self.mobilePay.distance)
        let desKM = des! / 1000.0
        self.lblDistance.text = "\(String(format: "%.2f", desKM)) km"
        if self.mobilePay.estmateFare == ""
        {
            self.mobilePay.estmateFare = self.mobilePay.estimatedFare
        }
        else
        {
            self.mobilePay.estimatedFare = self.mobilePay.estmateFare
        }
        
        self.totalAmount = self.isWalkIn ? Double(self.mobilePay.estimatedFare)! : Double(self.mobilePay.totalFare)!
        
        self.lblFare.text = "€\(String(format: "%.2f",self.totalAmount))".replacingOccurrences(of: ".", with: ",")
        self.selectionPaymentView()
        if paymentMethod == Constants.Common.paypal
        {
            self.vwCreditCard.isHidden = true
            self.btnPayTopConstrain.constant = 20
        }
        else if paymentMethod == Constants.Common.creditCard
        {
            self.getCardList()
        }
    }
    @IBAction func btnPay(_ sender: Any) {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MobilePayConfirmationViewController") as! MobilePayConfirmationViewController
        vw.modalPresentationStyle = .overCurrentContext
        vw.delegate = self
        vw.totalFare = "\(Double(self.mobilePay.totalFare)! + Double(self.tip)!)"
        vw.tip = self.tip
        vw.goodsCost = "\(Double(self.mobilePay.totalFare)! - 15.0)"
        //vw.isWalkIn = self.isWalkIn
        
        self.present(vw, animated: true, completion: nil)
    }
//    @IBAction func btnNo(_ sender: Any) {
//        self.vwConfirmation.isHidden = true
//    }
//    @IBAction func btnYes(_ sender: Any) {
//        self.vwConfirmation.isHidden = true
//
//    }
    
    func paypalPayment()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let payment = PayPalPayment()
        payment.amount =  NSDecimalNumber.init(string: String(self.totalAmount))
        payment.shortDescription = "Cangoo"
        payment.currencyCode = "EUR"
        let payPalConfig = PayPalConfiguration()
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            self.present(paymentViewController!, animated: true, completion: nil)
        }
    }
    @IBAction func btnPaypal(_ sender: Any) {
        self.paymentMethod = Constants.Common.paypal
        self.selectionPaymentView()
        self.loadData()
    }
    @IBAction func btnCreditCard(_ sender: Any) {
        
        self.paymentMethod = Constants.Common.creditCard
         self.selectionPaymentView()
        self.loadData()
    }
    private func selectionPaymentView()
    {
        if self.paymentMethod == Constants.Common.creditCard
        {
            self.vwPaypalSelection.removeBrdrClr()
            self.vwCreditCardSelection.addBordWhAppClr()
        }
        else
        {
            self.vwPaypalSelection.addBordWhAppClr()
            self.vwCreditCardSelection.removeBrdrClr()
        }
    }
}
extension FarePaymentViewController:PayPalPaymentDelegate
{
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        if let completePayment = completedPayment.confirmation as? [String: Any], let response = completePayment["response"] as? [String: String], let id = response["id"] {
           if self.isWalkIn
                {
                    self.PaymentByWalkIN(cusID: "", transID: id)
            }
            else
            {
                self.paymentWithPaypal(tranID: id)
            }
            paymentViewController.dismiss(animated: true, completion: nil)
        }
    }
    func paymentWithPaypal(tranID:String)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        payByPayPal(tripID: self.mobilePay.tripID, pID: SharedManager.sharedInstance.userData.pID, paymentTip: self.tip, paypalTransactionID: tranID, paymentAmount: "\(self.totalAmount)", isOverride: self.mobilePay!.isOverride, promoDiscountAmount: "\(self.promoDiscount)", walletUsedAmount: "\(self.walletAmountUsed)", voucherUsedAmount: self.mobilePay.voucherUsedAmount, fleetID: self.mobilePay.fleetID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            self.openRatingViewOnDashBoard()
                            self.present(ActionHandler.sharedInstance.showAlertDismisViewController(message: NSLocalizedString("Successfully paid", comment: ""), vw: self), animated: true, completion: nil)
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
}
extension FarePaymentViewController:CCSelectionDelegate
{
    func payWithCreditCard()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        payByCreditCard(currency: "eur", customerID: self.customerID, amount: "\(self.totalAmount)", paymentTip: self.tip, passengerID: SharedManager.sharedInstance.userData.pID, tripID: self.mobilePay.tripID, isOverride: self.mobilePay!.isOverride, promoDiscountAmount: "\(self.promoDiscount)", walletUsedAmount: "\(self.walletAmountUsed)", voucherUsedAmount: self.mobilePay.voucherUsedAmount, fleetID: self.mobilePay.fleetID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            self.openRatingViewOnDashBoard()
                            self.present(ActionHandler.sharedInstance.showAlertDismisViewController(message: NSLocalizedString("Successfully paid", comment: ""), vw: self), animated: true, completion: nil)
                        }
                        else {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    func PaymentByWalkIN(cusID:String, transID:String)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
     RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let mp = self.mobilePay
        getAddressFromGeocoding(latitude: Double(mp!.pickUplatitude)!, longitude: Double(mp!.pickUplongitude)!) { (err, picAddress) in
            getAddressFromGeocoding(latitude: Double(mp!.pickUplatitude)!, longitude: Double(mp!.pickUplongitude)!) { (error, dropAddress) in
            
                walkInPayment(pickUplatitude: (mp?.pickUplatitude)!, pickUplongitude: (mp?.pickUplongitude)!, dropOfflatitude: (mp?.dropOfflatitude)!, dropOfflongitude: (mp?.dropOfflongitude)!, driverID: (mp?.driverID)!, pID:SharedManager.sharedInstance.userData.pID, paymentAmount: "\(self.totalAmount)", resellerID: (mp?.ressellerID)!, currency: "eur", customerID: cusID, paymentTip: "\(self.tip)", isOverride: (mp?.isOverride)!, vehicleID: self.mobilePay.vehicleID, selectedPaymentMethod: self.paymentMethod, paypalTransactionID: transID, pickUpAddress: picAddress, dropOffAddress: dropAddress,distance:self.mobilePay.distance, fleetID: self.mobilePay.fleetID) { (response) in
                guard response.result.isSuccess else {
                    // self.showAlert(message: response.result.error! as! String)
                    RappleActivityIndicatorView.stopAnimation()
                    return
                }
                if let _JSON = response.result.value
                {
                    RappleActivityIndicatorView.stopAnimation()
                    let jsonData = _JSON.data(using: String.Encoding.utf8)
                    do{
                        if let data = jsonData,
                            let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                        {
                            if (json["Message"] as? String) != nil
                            {
                                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                return
                            }
                            let error = json["error"] as! Bool
                            if !error
                            {
                                self.openRatingViewOnDashBoard()
                                self.present(ActionHandler.sharedInstance.showAlertDismisViewController(message: NSLocalizedString("Successfully paid", comment: ""), vw: self), animated: true, completion: nil)
                            }
                            else {
                                if let msg = json["message"] as? String
                                {
                                    if msg.lowercased() == "paymentgetwayerror"
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("", comment: "")), animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }catch{}
                }
            }
            }
        }
    }
    func openRatingViewOnDashBoard() -> Void {
        NotificationCenter.default.post(name: NSNotification.Name("ratingObserver"), object: nil)
    }
    func CCSelected(arrCC: [CreditCard], defaultCard:String) {
        self.arrCreditCard = arrCC
        self.defaultCardID = defaultCard
        self.loadDefaultCard()
    }
    
    func getCardList()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        getCreditCard(passengerID: SharedManager.sharedInstance.userData.pID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                self.loadCard(dt: dt)
                            }
                        }
                        else
                        {
                            if let msg = json["message"] as? String
                            {
                                if msg.lowercased() == "paymentgetwayerror"
                                {
                                     self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                }
                                else
                                {
                                     self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }catch {}
            }
        }
    }
    private func loadCard(dt:[String:Any])
    {
        if let crdDetail = dt["creditCardDetails"] as? [String:Any]
        {
            if let lst = crdDetail["cardsList"] as? [[String:Any]]
            {
                if lst.count > 0
                {
                    self.vwCreditCard.isHidden = false
                    self.btnPayTopConstrain.constant = 85
                    self.arrCreditCard = []
                    for cd in lst
                    {
                        self.arrCreditCard.append(CreditCard.fromJSON(cd))
                    }
                    if let defualtCard = crdDetail["defaultSourceId"] as? String
                    {
                        self.customerID = crdDetail["customerId"] as! String
                        self.defaultCardID = defualtCard//crdDetail["defaultSourceId"] as! String
                        self.loadDefaultCard()
                    }
                }
                else
                {
                    self.noCredit()
                }
            }
            else
            {
                self.noCredit()
            }
        }
        else
        {
            self.noCredit()
        }
    }
    private func noCredit()
    {
        self.paymentMethod = Constants.Common.paypal
        self.selectionPaymentView()
        self.loadData()
        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("No credit card added in your wallet", comment: "")), animated: true, completion: nil)
    }
    private func loadDefaultCard()
    {
        if self.arrCreditCard.count > 0
        {
            for cd in self.arrCreditCard
            {
                if self.defaultCardID == cd.cardId
                {
                    //here show default card
                    self.vwCardHolderName.text = cd.cardHolderName
                    self.lblCardNo.text = "************"+cd.last4Digits
                    break
                }
            }
        }
    }
}
extension FarePaymentViewController:MobilePayDelegate
{
    func payment(payable:String) {
        self.totalAmount = Double(payable)!
        if self.paymentMethod == Constants.Common.paypal
        {
            if self.totalAmount == 0
            {
                self.paymentWithPaypal(tranID: "Zero Payment")
            }
            else
            {
                self.paypalPayment()
            }
        }
        else if self.paymentMethod == Constants.Common.creditCard
        {
            if isWalkIn
            {
                self.PaymentByWalkIN(cusID: self.customerID, transID: "")
            }
            else
            {
                self.payWithCreditCard()
            }
            
        }
    }
}
