//
//  BookingViewController.swift
//  CangooDeliver
//
//  Created by Abubakar on 20/03/2020.
//  Copyright © 2020 LS. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Alamofire

class BookingViewController: UIViewController {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblGoods: UILabel!
    @IBOutlet weak var lblMedicine: UILabel!
    @IBOutlet weak var vwGoods: UIView!
    @IBOutlet weak var imgGrocery: UIImageView!
    @IBOutlet weak var vwMedicine: UIView!
    @IBOutlet weak var imgMedicine: UIImageView!
    @IBOutlet weak var vwPaypal: UIView!
    @IBOutlet weak var vwCard: UIView!
    @IBOutlet weak var vwCash: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var txtOrderDetail: UITextView!
    @IBOutlet weak var lblEstimateFare: UILabel!
    @IBOutlet weak var vwAddress: UIView!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var lblNote: UILabel!
    
    
    
    var deliveryAddress = ""
    var deliveryLatitude = ""
    var deliveryLogitude = ""
    var selectedType = ""
    var selectedPaymentMethod = ""
    var placeText = "Write order detail here."
    var attachment:UIImage!
     var alamoFireManager : SessionManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblGoods.text = NSLocalizedString("Grocery", comment: "")
        self.lblNote.text = NSLocalizedString("Orders can only be placed in household quantities and subject to availability", comment: "")
        self.lblMedicine.text = NSLocalizedString("Medicine", comment: "")
        self.btnCancel.setTitle(NSLocalizedString("ABORT", comment: ""), for: .normal)
        self.btnCancel.addTarget(self, action: #selector(self.btnCncl), for: .touchUpInside)
        self.btnBookNow.setTitle(NSLocalizedString("ORDER NOW", comment: ""), for: .normal)
        self.btnBookNow.addTarget(self, action: #selector(self.btnBkNw), for: .touchUpInside)
        self.btnAttachment.addTarget(self, action: #selector(self.getAttachment), for: .touchUpInside)
        self.lblAddress.text = self.deliveryAddress
        self.txtOrderDetail.delegate = self
        self.txtOrderDetail.text = NSLocalizedString(self.placeText, comment: "")
        self.txtOrderDetail.textColor = UIColor.lightGray
        self.txtOrderDetail.dropShadow(radius: 5)
        self.vwAddress.dropShadow(radius: 5)
        self.lblGoods.headerView()
        self.lblMedicine.headerView()
        self.lblEstimateFare.text = NSLocalizedString("FARE: €15 + GOODS CHARGES", comment: "")
        let ud = SharedManager.sharedInstance.userData
        self.selectedPaymentMethod = (ud?.selectedPaymentMethod)!
        self.vwGoods.cornerRadius(radius: 8)
        self.vwMedicine.cornerRadius(radius: 8)
        self.vwPaypal.cornerRadius(radius: 8)
        self.vwCard.cornerRadius(radius: 8)
        self.vwCash.cornerRadius(radius: 8)
        if self.selectedPaymentMethod != ""
        {
            self.selectPaymentMethod(paymentMethod: (ud?.selectedPaymentMethod)!)
        }
        else
        {
            self.selectPaymentMethod(paymentMethod: "Cash")
        }
    }
    
    @IBAction func btnGoods(_ sender: Any) {
        self.selectedType = Constants.tripType.grocery
        self.vwGoods.addBottomBorderWithColor(color: Constants.Common.appYellowColor, width: 3)
        self.lblGoods.backgroundColor = Constants.Common.appYellowColor
        self.vwMedicine.addBottomBorderWithColor(color: UIColor.white, width: 3)
        self.lblMedicine.backgroundColor = Constants.Common.unSelectedColor
    }
    @IBAction func btnMedicine(_ sender: Any) {
        self.selectedType = Constants.tripType.medicine
        self.vwMedicine.addBottomBorderWithColor(color: Constants.Common.appYellowColor, width: 3)
        self.lblMedicine.backgroundColor = Constants.Common.appYellowColor
        self.vwGoods.addBottomBorderWithColor(color: UIColor.white, width: 3)
        self.lblGoods.backgroundColor = Constants.Common.unSelectedColor
    }
    @IBAction func btnTaxi(_ sender: Any) {
        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("This function is not yet available", comment: "")), animated: true, completion: nil)
    }
    
    @IBAction func btnPaypal(_ sender: Any) {
        self.selectPaymentMethod(paymentMethod: "Paypal")
    }
    
    @IBAction func btnCard(_ sender: Any) {
        self.selectPaymentMethod(paymentMethod: "CreditCard")
    }
    
    @IBAction func btnCash(_ sender: Any) {
        self.selectPaymentMethod(paymentMethod: "Cash")
    }
    
    private func selectPaymentMethod(paymentMethod:String)
    {
        if paymentMethod.lowercased() == "cash"
        {
            self.selectedPaymentMethod = "Cash"
            self.vwPaypal.addBottomBorderWithColor(color: UIColor.white, width: 3)
            self.vwCard.addBottomBorderWithColor(color: UIColor.white, width: 3)
            self.vwCash.addBottomBorderWithColor(color: Constants.Common.appYellowColor, width: 3)
        }
        else if paymentMethod.lowercased() == "paypal"
        {
            self.selectedPaymentMethod = "PayPal"
            self.vwPaypal.addBottomBorderWithColor(color: Constants.Common.appYellowColor, width: 3)
            self.vwCard.addBottomBorderWithColor(color: UIColor.white, width: 3)
            self.vwCash.addBottomBorderWithColor(color: UIColor.white, width: 3)
        }
        else if paymentMethod.lowercased() == "creditcard"
        {
            if SharedManager.sharedInstance.userData.isCedirCardAdded == "true"
            {
                self.selectedPaymentMethod = "CreditCard"
                           self.vwPaypal.addBottomBorderWithColor(color: UIColor.white, width: 3)
                           self.vwCard.addBottomBorderWithColor(color: Constants.Common.appYellowColor, width: 3)
                           self.vwCash.addBottomBorderWithColor(color: UIColor.white, width: 3)
            }
            else
            {
                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("First add creditcard or select other payment method", comment: "")), animated: true, completion: nil)
            }
           
        }
    }
    @objc private func btnCncl()
    {
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(vw, animated: false)
    }
    @objc private func btnBkNw()
    {
       if self.selectedType == ""
        {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please select an order type", comment: "")), animated: true, completion: nil)
            
            return
        }
        self.bookTrip()
    }
    @objc private func getAttachment()
    {
        if self.attachment == nil
        {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        else
        {
            let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "ShowImageViewController") as! ShowImageViewController
            vw.attachemt = self.attachment
            vw.delegateImg = self
            self.present(vw, animated: true, completion: nil)
        }
    }
}
extension BookingViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate ,AttachImageDelete//camera image
{
    func delImage() {
        self.attachment = nil
        btnAttachment.setImage(UIImage.init(named: "ic_attachment"), for: .normal)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        self.attachment = image
//        
//        guard let editimage = info[.editedImage] as? UIImage else {
//            print("No image found")
//            return
//        }
        
        self.btnAttachment.setImage(UIImage.init(named: "ic_attachment_checked"), for: .normal)
         picker.dismiss(animated: true)
    }
}
extension BookingViewController:UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == NSLocalizedString(self.placeText, comment: "")
        {
            self.txtOrderDetail.text = ""
            self.txtOrderDetail.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""
        {
            self.txtOrderDetail.text = NSLocalizedString(self.placeText, comment: "")
            self.txtOrderDetail.textColor = UIColor.lightGray
        }
    }
}
extension BookingViewController //bookNow
{
    func bookTrip()
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        let ud = SharedManager.sharedInstance.userData
        let facili = self.selectedType
        if self.attachment != nil
        {
            let parameter = ["pID":SharedManager.sharedInstance.userData.pID, "dropOfflatitude":self.deliveryLatitude,"dropOfflongitude":self.deliveryLogitude, "selectedPaymentMethod":self.selectedPaymentMethod, "dropOffLocation":self.deliveryAddress, "requiredFacilities": facili, "description":self.txtOrderDetail.text!]
            
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 100
            configuration.timeoutIntervalForResource = 100
            
            alamoFireManager = Alamofire.SessionManager(configuration: configuration)
            alamoFireManager!.upload(multipartFormData: { multipartFormData in
                
                if self.attachment != nil
                {
                    let mugImgData = self.attachment.jpegData(compressionQuality: 0.2)!
                    multipartFormData.append(mugImgData, withName: "image.jpg",fileName: "image.jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in parameter {
                    multipartFormData.append(((value).data(using: String.Encoding.utf8)!), withName: key)
                }
            },
                                     usingThreshold: UInt64.init(), to: Constants.Server.serverURL+"rideRequest", method: .post, headers: ["Authorization":"Bearer "+Constants.AuthToken.authToken, "ResellerID": Constants.Server.resellerID,"ApplicationID":Constants.Server.applicationID])
            { (result) in
                print(result)
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        //  RappleActivityIndicatorView.stopAnimation()
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseString { response in
                        RappleActivityIndicatorView.stopAnimation()
                        do {
                            print(response)
                            if response.data != nil
                            {
                                let json = try JSONSerialization.jsonObject(with: response.data!) as! [String: Any]
                                let error = json["error"] as! Bool
                                if !error {
                                    if let dt = json["data"] as? [String:Any]
                                    {
                                        let timeOut = dt["requestTime"] as! Double
                                        let tripID = dt["tripID"] as! String
                                        //let isLaterBooking = dt["isLaterBooking"] as! Bool
                                        Constants.Common.isBookingAccepted = false
                                        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestScreenViewController") as! RequestScreenViewController
                                        vw.waiTingTime = "\(timeOut)"
                                        vw.tripID = tripID
                                        vw.currentLat = self.deliveryLatitude
                                        vw.currentLong = self.deliveryLogitude
                                        //vw.isLaterBooking = isLaterBooking
                                        self.navigationController?.pushViewController(vw, animated: true)
                                    }
                                }
                                else {
                                    if let msg = json["message"] as? String
                                    {
                                        if msg.lowercased() == "useroutofrange"
                                        {
                                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Service not available in this area", comment: "")), animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                        }catch{}
                    }
                    break
                case .failure(let encodingError):
                    RappleActivityIndicatorView.stopAnimation()
                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Somthing wrong! Please try again later", comment: "")), animated: true, completion: nil)
                    print(encodingError)
                }
            }
        }
        else
        {
            rideRequestTrip(dropOfflatitude: self.deliveryLatitude, dropOfflongitude: self.deliveryLogitude, pID: (ud?.pID)!, selectedPaymentMethod: self.selectedPaymentMethod, dropOffLocation: self.deliveryAddress, requiredFacilities: facili, description: self.txtOrderDetail.text) { (response) in
                guard response.result.isSuccess else {
                    // self.showAlert(message: response.result.error! as! String)
                    RappleActivityIndicatorView.stopAnimation()
                    return
                }
                if let _JSON = response.result.value
                {
                    RappleActivityIndicatorView.stopAnimation()
                    let jsonData = _JSON.data(using: String.Encoding.utf8)
                    do{
                        if let data = jsonData,
                            let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                        {
                            if (json["Message"] as? String) != nil
                            {
                                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                return
                            }
                            let error = json["error"] as! Bool
                            if !error
                            {
                                if let dt = json["data"] as? [String:Any]
                                {
                                    let timeOut = dt["requestTime"] as! Double
                                    let tripID = dt["tripID"] as! String
                                    //let isLaterBooking = dt["isLaterBooking"] as! Bool
                                    Constants.Common.isBookingAccepted = false
                                    let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestScreenViewController") as! RequestScreenViewController
                                    vw.waiTingTime = "\(timeOut)"
                                    vw.tripID = tripID
                                    vw.currentLat = self.deliveryLatitude
                                    vw.currentLong = self.deliveryLogitude
                                    //vw.isLaterBooking = isLaterBooking
                                    self.navigationController?.pushViewController(vw, animated: true)
                                }
                            }
                            else {
                                if let msg = json["message"] as? String
                                {
                                    if msg.lowercased() == "useroutofrange"
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Service not available in this area", comment: "")), animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }catch{}
                }
            }
        }
    }
}
