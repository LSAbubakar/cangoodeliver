//
//  ChangePasswordViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 15/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var btnChangePasswordOutlet: UIButton!
    @IBOutlet weak var lblConfirmPasswordCap: UILabel!
    @IBOutlet weak var lblNewPasswordCap: UILabel!
    @IBOutlet weak var lblCurrentPasswordCap: UILabel!
    @IBOutlet weak var lblChangePasswordCap: UILabel!
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //localization
        self.lblChangePasswordCap.text = NSLocalizedString("CHANGE PASSWORD", comment: "")
        self.lblCurrentPasswordCap.text = NSLocalizedString("CURRENT PASSWORD", comment: "")
        self.lblNewPasswordCap.text = NSLocalizedString("NEW PASSWORD", comment: "")
        self.lblConfirmPasswordCap.text = NSLocalizedString("CONFIRM PASSWORD", comment: "")
        self.btnChangePasswordOutlet.setTitle(NSLocalizedString("CHANGE", comment: ""), for: .normal)
        
        //textfield
        self.txtConfirmPassword.textFieldRoundCorner()
        self.txtNewPassword.textFieldRoundCorner()
        self.txtCurrentPassword.textFieldRoundCorner()
        
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnChange(_ sender: Any) {
        if validate()
        {
            if !Connectivity.isConnectedToInternet() {
                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
                return
            }
            let userData = SharedManager.sharedInstance.userData
            RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
            changePassword(phoneNumber: (userData?.phoneNumber)!, password: self.txtNewPassword.text!, oldPassword: self.txtCurrentPassword.text!, pid: (userData?.pID)!) { (response) in
                guard response.result.isSuccess else {
                    // self.showAlert(message: response.result.error! as! String)
                    RappleActivityIndicatorView.stopAnimation()
                    return
                }
                if let _JSON = response.result.value
                {
                    RappleActivityIndicatorView.stopAnimation()
                    let jsonData = _JSON.data(using: String.Encoding.utf8)
                    do{
                        if let data = jsonData,
                            let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                        {
                            if (json["Message"] as? String) != nil
                            {
                                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                                return
                            }
                            let error = json["error"] as! Bool
                            if !error
                            {
                                self.present(ActionHandler.sharedInstance.showAlertPopViewController(message: NSLocalizedString("Your password has been updated successfully", comment: ""), vw: self), animated: true, completion: nil)
                            }
                            else {
                                if let msg = json["message"] as? String
                                {
                                    if msg.lowercased() == "failedtoresetpassword"
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Your current password incorrect", comment: "")), animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }catch{}
                }
            }
        }
    }
    func validate()->Bool
    {
        var result = true
        if self.txtCurrentPassword.text == ""
        {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter current password", comment: "")), animated: true, completion: nil)
        }
        else if self.txtNewPassword.text == ""
        {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter new password", comment: "")), animated: true, completion: nil)
        }
        else if self.txtConfirmPassword.text == ""
        {
            result = false
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Please enter confirm password", comment: "")), animated: true, completion: nil)
        }
        else if self.txtNewPassword.text != "" && self.txtConfirmPassword.text != ""
        {
            if self.txtNewPassword.text != self.txtConfirmPassword.text
            {
                result = false
                self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Password mistach", comment: "")), animated: true, completion: nil)
            }
        }
        return result
    }
}
