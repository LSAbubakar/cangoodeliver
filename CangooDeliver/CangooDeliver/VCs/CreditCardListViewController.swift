//
//  CreditCardListViewController.swift
//  CanTaxi-User
//
//  Created by Abubakar on 21/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit
import RappleProgressHUD
import LSCreditCardForm

protocol CreditCardListDelegate {
    func defaultCard(crd:CreditCard)
    func backFrmCC()
}

class CreditCardListViewController: ParentViewController {

    @IBOutlet weak var lblCreditCardCap: UILabel!
    @IBOutlet weak var tblCard: UITableView!
    //type
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var lblPersonal: UILabel!
    @IBOutlet weak var imgPersonal: UIImageView!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var imgCompany: UIImageView!
    @IBOutlet weak var lblFamilyFriend: UILabel!
    @IBOutlet weak var imgFF: UIImageView!
    @IBOutlet weak var vwMainType: UIView!
    @IBOutlet weak var vwDone: UIView!
    //@IBOutlet weak var vwTypeTopConstrain: NSLayoutConstraint!
    
    var arrCreditCard:[CreditCard] = []
    var defaultCardID = ""
    var customerID = ""
    var isPersonal = false
    var isCompany = false
    var isFF = false
    var defaultCard:CreditCard!
    var delegate:CreditCardListDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwMainType.isHidden = true
        self.tblCard.tableFooterView = UIView()
        self.tblCard.delegate = self
        self.tblCard.dataSource = self
    
        self.vwHeader.clipsToBounds = true
        self.vwHeader.layer.cornerRadius = 8
        self.vwHeader.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        //localization
        self.lblPersonal.text = NSLocalizedString("Personal", comment: "")
        self.lblCompany.text = NSLocalizedString("Company", comment: "")
        self.lblFamilyFriend.text = NSLocalizedString("Friends/Family", comment: "")
        self.lblHeader.text = NSLocalizedString("ADD CREDIT CARD", comment: "")
        self.lblCreditCardCap.text = NSLocalizedString("Credit Card", comment: "")
        self.getCardList()
    }
    func cardType() -> Void {
        //card type
        if self.isPersonal
        {
            self.imgPersonal.image = UIImage.init(named: "check-mark")
        }
        else
        {
            self.imgPersonal.image = UIImage.init(named: "")
        }
        if self.isCompany
        {
            self.imgCompany.image = UIImage.init(named: "check-mark")
        }
        else
        {
            self.imgCompany.image = UIImage.init(named: "")
        }
        if self.isFF
        {
            self.imgFF.image = UIImage.init(named: "check-mark")
        }
        else
        {
            self.imgFF.image = UIImage.init(named: "")
        }
    }
    @IBAction func btnAddCreditCard(_ sender: Any) {
        self.openTypePopup()
    }
    private func openTypePopup()
    {
         if self.isPersonal
         {
             self.openAlert(type: "personal")
         }
         else {
             self.openAddCard(cardType: Constants.CreditCardType.cardPersonal)
         }
//        self.view.bringSubviewToFront(self.vwMainType)
//        self.vwMainType.isHidden = false
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.delegate.backFrmCC()
        //self.delegate.defaultCard(crd: self.defaultCard)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnPersona(_ sender: Any) {
        if self.isPersonal
        {
            self.openAlert(type: "personal")
        }
        else {
            self.openAddCard(cardType: Constants.CreditCardType.cardPersonal)
        }
    }
    @IBAction func btnCompany(_ sender: Any) {
        if self.isCompany
        {
            self.openAlert(type: "company")
        }
        else {
            self.openAddCard(cardType: Constants.CreditCardType.cardCompany)
        }
    }
    @IBAction func btnFF(_ sender: Any) {
        if self.isFF
        {
            self.openAlert(type: "friends/family")
        }
        else {
            self.openAddCard(cardType: Constants.CreditCardType.cardFriend)
        }
    }
    @IBAction func btnCloseCardType(_ sender: Any) {
        self.vwMainType.isHidden = true
    }
    @IBAction func btnDone(_ sender: Any) {
        self.delegate.defaultCard(crd: self.defaultCard)
        self.dismiss(animated: true, completion: nil)
    }
}
extension CreditCardListViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCreditCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblCard.dequeueReusableCell(withIdentifier: "CreditCardTableViewCell", for: indexPath) as! CreditCardTableViewCell
        let crd = self.arrCreditCard[indexPath.row]
        cell.vwMain.dropShadow(scale: true, radius: 5)
        cell.vwMain.layer.cornerRadius = 8
        cell.vwMain.clipsToBounds = true
        cell.lblCardType.text = NSLocalizedString("", comment: "")
       
//        if self.defaultCardID == crd.cardId
//        {
//            cell.btnSetDefault.setImage(UIImage.init(named: "check_card"), for: .normal)
//        }
//        else
//        {
//            cell.btnSetDefault.setImage(UIImage.init(named: "uncheck_card"), for: .normal)
//        }
        let ls = LSCreditCardView()
        cell.addSubview(ls)
        ls.leftAnchor.constraint(equalTo: cell.vwCard.leftAnchor).isActive = true
       ls.topAnchor.constraint(equalTo: cell.vwCard.topAnchor).isActive = true
       cell.vwCard.bottomAnchor.constraint(equalTo: ls.bottomAnchor).isActive = true
        let crdDetail = LSCreditCard()
        crdDetail.cardHolderName = crd.cardHolderName
        crdDetail.expiration = crd.expiryMonth+"/"+crd.expiryYear
        crdDetail.number = "************"+crd.last4Digits
        //crdDetail.cardType = .americanExpress
        ls.updateValues(creditCard: crdDetail)
        
        cell.btnSetDefault.addTarget(self, action: #selector(self.changeDefault(btn:)), for: .touchUpInside)
        cell.btnSetDefault.tag  = indexPath.row + 100
        cell.btnDeleteCard.addTarget(self, action: #selector(self.delCard(btn:)), for: .touchUpInside)
        cell.btnDeleteCard.tag  = indexPath.row + 200
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        return cell
    }
    @objc func changeDefault(btn:UIButton)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let crd = self.arrCreditCard[btn.tag - 100]
        if self.arrCreditCard.count >= 1
        {
            self.updateCard(token: crd.cardId, customeID: self.customerID)
        }
        
    }
    @objc func delCard(btn:UIButton)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
        vw.message = "Are sure you want to delete credit card?"
        vw.isNotify = false
        vw.delContentID = "\(btn.tag - 200)"
        vw.delegateConfirm = self
        vw.modalPresentationStyle = .overCurrentContext
        self.present(vw, animated: true, completion: nil)
    }
}
extension CreditCardListViewController:ConfirmDelegate
{
    func yes(delContentID: String) {
        let crd = self.arrCreditCard[Int(delContentID)!]
        self.delCredCard(token: crd.cardId, customerID: self.customerID, cardType:crd.cardDescription)
    }
}
extension CreditCardListViewController
{
    func getCardList()
    {
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        getCreditCard(passengerID: SharedManager.sharedInstance.userData.pID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                self.loadCard(dt: dt)
                            }
                        }
                        else
                        {
                            if let msg = json["message"] as? String
                            {
                                if msg.lowercased() == "paymentgetwayerror"
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("", comment: "")), animated: true, completion: nil)
                                }
                                else
                                {
                                    self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }catch {}
            }
        }
    }
    func delCredCard(token:String, customerID:String, cardType:String)
    {
        if !Connectivity.isConnectedToInternet() {
            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Failed to connect internet", comment: "")), animated: true, completion: nil)
            return
        }
         RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        delCreditCard(customerID:customerID, cardToken: token) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                if cardType == Constants.CreditCardType.cardCompany
                                {
                                    self.isCompany = false
                                }
                                if cardType == Constants.CreditCardType.cardPersonal
                                {
                                    self.isPersonal = false
                                }
                                if cardType == Constants.CreditCardType.cardFriend
                                {
                                    self.isFF = false
                                }
                                self.loadCard(dt: dt)
                            }
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch {}
            }
        }
    }
    func updateCard(token:String, customeID:String)
    {
        RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        updateDefaultCreditCard(customerID:customeID , cardToken: token) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                self.loadCard(dt: dt)
                            }
                        }
                        else
                        {
                            
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch {}
            }

        }
    }
    func AddCard(token:String, customerID:String)
    {
         RappleActivityIndicatorView.startAnimating(attributes: Constants.rappleAttribute.attributes)
        addEditCard(passengerID: SharedManager.sharedInstance.userData.pID, cardToken: token, passangerEmailAddress: SharedManager.sharedInstance.userData.email, customerID: customerID) { (response) in
            guard response.result.isSuccess else {
                // self.showAlert(message: response.result.error! as! String)
                RappleActivityIndicatorView.stopAnimation()
                return
            }
            if let _JSON = response.result.value
            {
                RappleActivityIndicatorView.stopAnimation()
                let jsonData = _JSON.data(using: String.Encoding.utf8)
                do{
                    if let data = jsonData,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                        if (json["Message"] as? String) != nil
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Authentication failure", comment: "")), animated: true, completion: nil)
                            return
                        }
                        let error = json["error"] as! Bool
                        if !error
                        {
                            if let dt = json["data"] as? [String:Any]
                            {
                                self.loadCard(dt: dt)
                            }
                        }
                        else
                        {
                            self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("Something wrong! Please try again later", comment: "")), animated: true, completion: nil)
                        }
                    }
                }catch{}
            }
        }
    }
    func loadCard(dt:[String:Any])
    {
        if let crdDetail = dt["creditCardDetails"] as? [String:Any]
        {
            if let lst = crdDetail["cardsList"] as? [[String:Any]]
            {
                self.arrCreditCard = []
                if let defualtCard = crdDetail["defaultSourceId"] as? String
                {
                    self.defaultCardID = defualtCard//crdDetail["defaultSourceId"] as! String
                }
                if lst.count > 0
                {
                    SharedManager.sharedInstance.userData.isCedirCardAdded = "true"
                    ActionHandler.sharedInstance.delUpdateUserDefaultsForUserData(uData: SharedManager.sharedInstance.userData)
                }
                for cd in lst
                {
                    let crd = CreditCard.fromJSON(cd)
                    self.arrCreditCard.append(crd)
                    if crd.cardDescription == Constants.CreditCardType.cardCompany
                    {
                        self.isCompany = true
                    }
                     if crd.cardDescription == Constants.CreditCardType.cardPersonal
                    {
                        self.isPersonal = true
                    }
                    if crd.cardDescription == Constants.CreditCardType.cardFriend
                    {
                        self.isFF = true
                    }
                    if self.defaultCardID == crd.cardId
                    {
                        self.defaultCard = crd
                    }
                }
                if let customerId = crdDetail["customerId"] as? String
                {
                    self.customerID = customerId
                    self.cardType()
                    self.tblCard.reloadData()
                }
                if lst.count > 0
                {
                    self.vwDone.isHidden = false
                }
                else
                {
                        SharedManager.sharedInstance.userData.isCedirCardAdded = "false"
                        ActionHandler.sharedInstance.delUpdateUserDefaultsForUserData(uData: SharedManager.sharedInstance.userData)
                    
                    self.vwDone.isHidden = true
                }
            }
            else
            {
               self.openTypePopup()
            }
        }
        else
        {
            self.openTypePopup()
        }
    }
}
extension CreditCardListViewController
{
    
    func openAlert(type:String)
    {
        self.present(ActionHandler.sharedInstance.showAlert(message: NSLocalizedString("You are allowed to add only one credit card at a time. Delete old one to add new card", comment: "")), animated: true, completion: nil)
    }
    func openAddCard(cardType:String) {
        self.vwMainType.isHidden = true
        let vw = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCreditCardViewController") as! AddCreditCardViewController
        vw.delegateCardToke = self
        vw.customerID = self.customerID
        vw.cardType = cardType
        self.present(vw, animated: true, completion: nil)
    }
}
extension CreditCardListViewController:AddCreditCardDelegate
{
    func CcAddedSucc(stripToken: String, customerID:String) {
        
        self.AddCard(token: stripToken, customerID:customerID )
    }
}
