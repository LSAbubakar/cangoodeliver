//
//  CreditCardTableViewCell.swift
//  CanTaxi-User
//
//  Created by Abubakar on 21/04/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import UIKit

class CreditCardTableViewCell: UITableViewCell {

    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var btnSetDefault: UIButton!
    @IBOutlet weak var btnDeleteCard: UIButton!
    @IBOutlet weak var vwCard: UIView!
    @IBOutlet weak var lblCardType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
