//
//  PastTripTableViewCell.swift
//  CanTaxi-User
//
//  Created by Abubakar on 20/12/2018.
//  Copyright © 2018 Abubakar. All rights reserved.
//

import UIKit

class PastTripTableViewCell: UITableViewCell {

    @IBOutlet weak var vwHead: UIView!
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDropOffaddress: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblTip: UILabel!
    @IBOutlet weak var lblTipCap: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var imgBGP: UIImageView!
    @IBOutlet weak var imgPayment: UIImageView!
    @IBOutlet weak var lblGCCap: UILabel!
    @IBOutlet weak var lblGoodsCost: UILabel!
    @IBOutlet weak var lblRFCap: UILabel!
    @IBOutlet weak var lblRideFare: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.lblGCCap.text = NSLocalizedString("Goods Cost:", comment: "")
        self.lblRFCap.text = NSLocalizedString("Ride Fare:", comment: "")
        self.lblTipCap.text = NSLocalizedString("Tip:", comment: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
