//
//  GoogleApi.swift
//  CanTaxi
//
//  Created by Abubakar on 07/02/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import Alamofire
public func getAddressFromGeocoding(latitude:Double, longitude:Double, completion:@escaping(_ error:Bool,_ address:String)-> ())
{
    let direction = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude   )&key=\(Constants.Server.googleAPIKey)"
    var address = ""
    
    sendRequest(url: direction, parameters: [:]) { (response) in
        print(response)
        guard response.result.isSuccess else {
            completion(true,"")
            return
        }
        
        if let _JSON = response.result.value
        {
            let jsonData = _JSON.data(using: String.Encoding.utf8)
            do{
                if let data = jsonData,
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                    if let results = json["results"] as? [NSDictionary] {
                        if results.count > 0 {
                            if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                                address = (results[0]["formatted_address"] as? String)!
//                                for component in addressComponents {
//                                    if let temp = component.object(forKey: "types") as? [String] {
//                                        print(temp)
////                                        if (temp[0] == "postal_code") {
////                                            postalCode = (component["long_name"] as? String)!
////                                        }
////                                        if (temp[0] == "locality") {
////                                            city = (component["long_name"] as? String)!
////                                        }
////                                        if (temp[0] == "administrative_area_level_1") {
////                                            state = (component["long_name"] as? String)!
////                                        }
////                                        if (temp[0] == "country") {
////                                            country = (component["long_name"] as? String)!
////                                        }
//                                    }
//                                }
                                completion(false,"\(address)")
                            }
                        }
                    }
                }
            }catch {
                completion(true,"")
            }
        }
    }
}


public func gDirection(pickLatitude:String,pickLongitude:String,dropLatitude:String,dropLongitude:String, completion:@escaping (_ err:Bool,_ pickupAddress:String,_ pickDistance:String, _ points:String, _ pickUpDuration:String,_ dropOffAddress:String) -> () )
{
    var totalDistance = ""
    var pickupAddress = ""
    var dropOffAddress = ""
    var pointsPolyLine = ""
    var pickDuration = ""
    let direction = "https://maps.googleapis.com/maps/api/directions/json?origin=\(pickLatitude),\(pickLongitude)&destination=\(dropLatitude),\(dropLongitude)&mode=driving&key=\(Constants.Server.googleAPIKey)"
    sendRequest(url: direction, parameters: [:]) { (response) in
        print(response)
        guard response.result.isSuccess else {
            completion(true,"","","","", "")
            return
        }
        
        if let _JSON = response.result.value
        {
            let jsonData = _JSON.data(using: String.Encoding.utf8)
            do{
                if let data = jsonData,
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                    if let routes = json["routes"] as? [[String:Any]],
                        let route = routes.first,
                        let legs = route["legs"] as? [[String:Any]],
                        let leg = legs.first,
                        let distance = leg["distance"] as? [String:Any],
                        let distanceText = distance["text"] as? String {
                        
                        totalDistance = distanceText
                        if let address = leg["end_address"] as? String
                        {
                           dropOffAddress = address
                        }
                        if let startaddress = leg["start_address"] as? String
                        {
                            pickupAddress = startaddress
                        }
                        if  let duration = leg["duration"] as? [String:Any],
                            let durationText = duration["text"] as? String
                        {
                            pickDuration = durationText
                        }
                        if let polyline = route["overview_polyline"] as? [String:Any],
                            let points = polyline["points"] as? String
                        {
                            print(points)
                            pointsPolyLine = points
                        }
                        
                        completion(false,pickupAddress,totalDistance,pointsPolyLine,pickDuration,dropOffAddress)
                    }
                }
                else {
                    completion(true,"","","","","")
                }
            }
            catch{
                completion(true,"","","","","")
            }
        }
    }
}

public func sendRequest(url: String,parameters: Parameters, completion: @escaping (DataResponse<String>) -> Void) -> Void {
    Alamofire.request(url, method: .get,parameters: parameters).responseString { response in
        completion(response)
    }
}
