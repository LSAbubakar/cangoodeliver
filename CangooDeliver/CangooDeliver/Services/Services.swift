//
//  Services.swift
//  CanTaxi
//
//  Created by Abubakar on 16/01/2019.
//  Copyright © 2019 Abubakar. All rights reserved.
//

import Foundation
import Alamofire
//User
func loginPassenger(phoneNumber:String,password:String,deviceToken:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "phoneNumber":phoneNumber,
        "password":password,
        "deviceToken":deviceToken
    ]
    sendPostRequest(url: "loginPassenger", parameters: parameters, isHeader: false, completion: completion)
}
func registerPassenger(firstName:String, lastName:String, phoneNumber:String, email:String, password:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "firstName":firstName,
        "lastName":lastName,
        "phoneNumber":phoneNumber,
        "email":email,
        "password":password
    ]
    sendPostRequest(url: "registerPassenger", parameters: parameters, isHeader: false, completion: completion)
}
func forgetPassword(phoneNumber:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "phoneNumber":phoneNumber
    ]
    sendPostRequest(url: "ResetPassword", parameters: parameters, isHeader: false, completion: completion)
}
func changePassword(phoneNumber:String,password:String, oldPassword:String, pid:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "phoneNumber":phoneNumber,
        "password":password,
        "oldPassword":oldPassword,
        "pid":pid
    ]
    sendPostRequest(url: "chagePassword", parameters: parameters, isHeader: true, completion: completion)
}
func phoneVerification(verificationCode:String,pID:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "verificationCode":verificationCode,
        "pID":pID
    ]
    sendPostRequest(url: "phoneVerification", parameters: parameters, isHeader: true, completion: completion)
}
//trip detail
func getTripHistory(pID:String,offset:String,limit:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "offset":offset,
        "limit":limit
    ]
    sendGetRequest(url: "tripHistory", parameters: parameters, isHeader: true, completion: completion)
}
func getUpcomingTrip(pID:String,offset:String,limit:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "offset":offset,
        "limit":limit
    ]
    sendGetRequest(url: "tripUpcoming", parameters: parameters, isHeader: true, completion: completion)
}
//Wallet
func getWallet(pID:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID
    ]
    sendGetRequest(url: "getWalletDetail", parameters: parameters, isHeader: true, completion: completion)
}
func walletPreffer(pID:String,isWallet:Bool,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "isWallet":isWallet ? "true" : "false"
    ]
    sendPostRequest(url: "walletAdjustment", parameters: parameters, isHeader: true, completion: completion)
}
func updatePaymentMethod(pID:String,selectedPaymentMethod:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "selectedPaymentMethod":selectedPaymentMethod
    ]
    sendPostRequest(url: "updatePaymentMethod", parameters: parameters, isHeader: true, completion: completion)
}
//applyInviteCode
func applyInviteCode(passengerID:String,inviteCode:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "passengerID":passengerID,
        "inviteCode":inviteCode
    ]
    sendPostRequest(url: "applyInviteCode", parameters: parameters, isHeader: true, completion: completion)
}
//estimate fare
func fareEstimate(pickUplatitude:String,pickUplongitude:String, dropOfflatitude:String,dropOfflongitude:String, resellerID:String,resellerArea:String,seatingCapacity:String, pid:String,estimatedDistance:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pickUplatitude":pickUplatitude,
        "pickUplongitude":pickUplongitude,
        "dropOfflatitude":dropOfflatitude,
        "dropOfflongitude":dropOfflongitude,
        "resellerID":resellerID,
        "resellerArea":resellerArea,
        "seatingCapacity":seatingCapacity,
        "pID":pid,
        "estimatedDistance":estimatedDistance
    ]
    sendPostRequest(url: "estimatedFare", parameters: parameters, isHeader: true, completion: completion)
}
//ride request
//dropOfflatitude, dropOfflongitude, dropOffLocation, pID, selectedPaymentMethod, description, requiredFacilities
func rideRequestTrip(dropOfflatitude:String,dropOfflongitude:String, pID:String, selectedPaymentMethod:String, dropOffLocation:String, requiredFacilities:String,description:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "dropOfflatitude":dropOfflatitude,
        "dropOfflongitude":dropOfflongitude,
        "pID":pID,
        "selectedPaymentMethod":selectedPaymentMethod,
        "dropOffLocation":dropOffLocation,
        "requiredFacilities": requiredFacilities,
        "description":description
    ]
    sendPostRequest(url: "rideRequest", parameters: parameters, isHeader: true, completion: completion)
}
//time out of ride request
func rideRequestTimeOut(tripID:String,pID:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "tripID":tripID,
        "pID":pID
    ]
    sendPostRequest(url: "rideTimeOut", parameters: parameters, isHeader: true, completion: completion)
}
//cancel Ride

func tripCancel(tripID:String,cancelID:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "tripID":tripID,
        "cancelID":cancelID,
        "distance":"0"
    ]
    sendPostRequest(url: "cancelRide", parameters: parameters, isHeader: true, completion: completion)
}
//prmo code
func addDelPromo(passengerID:String,promoCode:String,addPromo:Bool,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "passengerID":passengerID,
        "promoCode":promoCode,
        "addPromo":addPromo ? "true" : "false"
    ]
    sendPostRequest(url: "addEditPromoCode", parameters: parameters, isHeader: true, completion: completion)
}
//credit card
func getCreditCard(passengerID:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "passengerID":passengerID
    ]
    sendPostRequest(url: "getUserCreditCards", parameters: parameters, isHeader: true, completion: completion)
}

func delCreditCard(customerID:String,cardToken:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "customerID":customerID,
        "cardToken":cardToken
    ]
    sendPostRequest(url: "deleteCreditCard", parameters: parameters, isHeader: true, completion: completion)
}
func addEditCard(passengerID:String,cardToken:String,passangerEmailAddress:String,customerID:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "passengerID":passengerID,
        "cardToken":cardToken,
        "passangerEmailAddress":passangerEmailAddress,
        "customerID":customerID
    ]
    sendPostRequest(url: "addEditCreditCard", parameters: parameters, isHeader: true, completion: completion)
}
func updateDefaultCreditCard(customerID:String,cardToken:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "customerID":customerID,
        "cardToken":cardToken
    ]
    sendPostRequest(url: "updateDefaultCreditCard", parameters: parameters, isHeader: true, completion: completion)
}
//paypalPayment
func payByPayPal(tripID:String,pID:String,paymentTip:String,paypalTransactionID:String, paymentAmount:String,isOverride:String,promoDiscountAmount:String,walletUsedAmount:String, voucherUsedAmount:String, fleetID:String,  completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "tripID":tripID,
        "pID":pID,
        "paymentTip":paymentTip,
        "paypalTransactionID":paypalTransactionID,
        "paymentAmount":paymentAmount,
        "isOverride":isOverride,
        "promoDiscountAmount":promoDiscountAmount,
        "voucherUsedAmount":voucherUsedAmount,
        "walletUsedAmount":walletUsedAmount,
        "fleetID":fleetID
    ]
    sendPostRequest(url: "paypalPayment", parameters: parameters, isHeader: true, completion: completion)
}
//creditcard payment
func payByCreditCard(currency:String,customerID:String,amount:String,paymentTip:String, passengerID:String,tripID:String,isOverride:String, promoDiscountAmount:String, walletUsedAmount:String, voucherUsedAmount:String,fleetID:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "currency":currency,
        "customerID":customerID,
        "amount":amount,
        "paymentTip":paymentTip,
        "passengerID":passengerID,
        "tripID":tripID,
        "isOverride":isOverride,
        "promoDiscountAmount":promoDiscountAmount,
        "walletUsedAmount":walletUsedAmount,
        "voucherUsedAmount":voucherUsedAmount,
        "fleetID":fleetID
    ]
    sendPostRequest(url: "creditCardPayment", parameters: parameters, isHeader: true, completion: completion)
}
//walkIn Payment
func walkInPayment(pickUplatitude:String,pickUplongitude:String,dropOfflatitude:String,dropOfflongitude:String,   driverID:String,pID:String,paymentAmount:String, resellerID:String, currency:String, customerID:String, paymentTip:String,isOverride:String,vehicleID:String,selectedPaymentMethod:String, paypalTransactionID:String, pickUpAddress:String, dropOffAddress:String, distance:String, fleetID:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pickUplatitude":pickUplatitude,
        "pickUplongitude":pickUplongitude,
        "dropOfflatitude":dropOfflatitude,
        "dropOfflongitude":dropOfflongitude,
        "driverID":driverID,
        "pID":pID,
        "paymentAmount":paymentAmount,
        "resellerID":resellerID,
        "currency":currency,
        "customerID":customerID,
        "paymentTip":paymentTip,
        "isOverride":isOverride,
        "vehicleID":vehicleID,
        "selectedPaymentMethod":selectedPaymentMethod,
        "paypalTransactionID":paypalTransactionID,
        "pickUpLocation":pickUpAddress,
        "dropOffLocation":dropOffAddress,
        "distance":distance,
        "fleetID":fleetID,
    ]
    sendPostRequest(url: "WalkInPassengerPayment", parameters: parameters, isHeader: true, completion: completion)
}
//favUnFav
func captainFavUnfav(pID:String,tripID:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "tripID":tripID
    ]
    sendPostRequest(url: "favUnFav", parameters: parameters, isHeader: true, completion: completion)
}
//submitTripFeedback
func feedbackByPassenger(tripID:String,driverRating:String,vehicleRating:String, completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "tripID":tripID,
        "driverRating":driverRating,
        "vehicleRating":vehicleRating
    ]
    sendPostRequest(url: "submitTripFeedback", parameters: parameters, isHeader: true, completion: completion)
}
//updateName
func updateName(pID:String,firstName:String,lastName:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "firstName":firstName,
        "lastName":lastName
    ]
    sendPostRequest(url: "updateName", parameters: parameters, isHeader: true, completion: completion)
}
//resendVerificationCode
func resendCode(pID:String,phoneNumber:String,completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [
        "pID":pID,
        "phoneNumber":phoneNumber
    ]
    sendPostRequest(url: "resendVerificationCode", parameters: parameters, isHeader: true, completion: completion)
}
//get facilities
func getTripFacilities(completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [:]
    sendGetRequest(url: "getFacilities", parameters: parameters, isHeader: true, completion: completion)
}
public func checkToken(completion: @escaping (DataResponse<String>) -> Void)
{
    let parameters: Parameters = [:]
    sendGetRequest(url: "tokenValidation",parameters: parameters, isHeader: true, completion: completion)
}
func sendGetRequest(url: String,parameters: Parameters, isHeader:Bool, completion: @escaping (DataResponse<String>) -> Void) -> Void {
    let headers = [
        "Authorization": "Bearer \(Constants.AuthToken.authToken)",
        "Content-Type": "application/x-www-form-urlencoded",
        "ResellerID": Constants.Server.resellerID,
        "ApplicationID": Constants.Server.applicationID
    ]
    
   // if isHeader {
       
        Alamofire.request("\(Constants.Server.serverURL)/" + url, method: .get,parameters: parameters, encoding: URLEncoding.default, headers: headers).responseString { response in
            completion(response)
        }
//    }
//    else {
//        Alamofire.request("\(Constants.Server.serverURL)/" + url, method: .get,parameters: parameters).responseString { response in
//            completion(response)
//        }
//    }
}

func sendPostRequest(url: String, parameters: Parameters, isHeader:Bool, completion: @escaping (DataResponse<String>) -> Void) -> Void {
    let headers = [
        "Authorization": "Bearer \(Constants.AuthToken.authToken)",
        "Content-Type": "application/x-www-form-urlencoded",
        "ResellerID": Constants.Server.resellerID,
        "ApplicationID": Constants.Server.applicationID
    ]
    
//    if isHeader
//    {
    
        Alamofire.request("\(Constants.Server.serverURL)/" + url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseString { response in
            completion(response)
        }
//    }
//    else {
//        Alamofire.request("\(Constants.Server.serverURL)/" + url, method: .post, parameters:parameters).responseString { response in
//            completion(response)
//        }
//    }
}

